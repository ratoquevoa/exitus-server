-- Database: exitus

-- DROP DATABASE exitus;

CREATE DATABASE exitus
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'C'
       LC_CTYPE = 'C'
       CONNECTION LIMIT = -1;

\connect exitus

CREATE TABLE aluno (
    id bigint NOT NULL,
    data_atualizacao timestamp without time zone NOT NULL,
    nome character varying(45) NOT NULL
);


CREATE TABLE ano_semestre (
    id bigint NOT NULL,
    data_atualizacao timestamp without time zone NOT NULL,
    descricao character varying(10) NOT NULL
);



CREATE TABLE campus (
    id bigint NOT NULL,
    data_atualizacao timestamp without time zone NOT NULL,
    nome character varying(45) NOT NULL
);


CREATE TABLE curso (
    id bigint NOT NULL,
    data_atualizacao timestamp without time zone NOT NULL,
    nome character varying(45) NOT NULL
);


CREATE TABLE disciplina (
    id bigint NOT NULL,
    data_atualizacao timestamp without time zone NOT NULL,
    nome character varying(100) NOT NULL
);


CREATE TABLE frequencia (
    id bigint NOT NULL,
    data_atualizacao timestamp without time zone NOT NULL,
    aula integer NOT NULL,
    data_frequencia timestamp without time zone NOT NULL,
    status character varying(3) NOT NULL,
    oferta_disciplina_id bigint NOT NULL
);


CREATE TABLE frequencia_aluno (
    id bigint NOT NULL,
    data_atualizacao timestamp without time zone NOT NULL,
    situacao boolean NOT NULL,
    frequencia_id bigint NOT NULL,
    oferta_disciplina_aluno_id bigint NOT NULL
);


CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE log_acesso (
    id integer NOT NULL,
    data timestamp without time zone NOT NULL,
    token character varying(255) NOT NULL,
    valido boolean,
    professor_id bigint NOT NULL
);


CREATE TABLE matriz (
    id bigint NOT NULL,
    data_atualizacao timestamp without time zone NOT NULL,
    descricao character varying(10) NOT NULL
);

CREATE TABLE oferta_curso (
    id bigint NOT NULL,
    data_atualizacao timestamp without time zone NOT NULL,
    ano_semestre_id bigint NOT NULL,
    campus_id bigint NOT NULL,
    curso_id bigint NOT NULL,
    matriz_id bigint NOT NULL
);


CREATE TABLE oferta_disciplina (
    id bigint NOT NULL,
    data_atualizacao timestamp without time zone NOT NULL,
    disciplina_id bigint NOT NULL,
    oferta_curso_id bigint NOT NULL
);


CREATE TABLE oferta_disciplina_aluno (
    id bigint NOT NULL,
    data_atualizacao timestamp without time zone NOT NULL,
    aluno_id bigint NOT NULL,
    oferta_disciplina_id bigint NOT NULL
);


CREATE TABLE oferta_disciplina_professor (
    id bigint NOT NULL,
    data_atualizacao timestamp without time zone NOT NULL,
    oferta_disciplina_id bigint NOT NULL,
    professor_id bigint NOT NULL
);


CREATE TABLE professor (
    id bigint NOT NULL,
    data_atualizacao timestamp without time zone NOT NULL,
    nome character varying(100) NOT NULL,
    senha character varying(100) NOT NULL,
    usuario character varying(100) NOT NULL
);


CREATE TABLE tarefa_agendada (
    tabela character varying(31) NOT NULL,
    id integer NOT NULL,
    data_finalizacao timestamp without time zone,
    parametros character varying(255) NOT NULL,
    status character varying(3) NOT NULL
);

CREATE TABLE tarefa_agendada_depende (
    tarefa_pai integer NOT NULL,
    tarefa_filha integer NOT NULL
);

ALTER TABLE ONLY aluno
    ADD CONSTRAINT aluno_pkey PRIMARY KEY (id);

ALTER TABLE ONLY ano_semestre
    ADD CONSTRAINT ano_semestre_pkey PRIMARY KEY (id);

ALTER TABLE ONLY campus
    ADD CONSTRAINT campus_pkey PRIMARY KEY (id);

ALTER TABLE ONLY curso
    ADD CONSTRAINT curso_pkey PRIMARY KEY (id);

ALTER TABLE ONLY disciplina
    ADD CONSTRAINT disciplina_pkey PRIMARY KEY (id);

ALTER TABLE ONLY frequencia_aluno
    ADD CONSTRAINT frequencia_aluno_pkey PRIMARY KEY (id);

ALTER TABLE ONLY frequencia
    ADD CONSTRAINT frequencia_pkey PRIMARY KEY (id);

ALTER TABLE ONLY log_acesso
    ADD CONSTRAINT log_acesso_pkey PRIMARY KEY (id);

ALTER TABLE ONLY matriz
    ADD CONSTRAINT matriz_pkey PRIMARY KEY (id);

ALTER TABLE ONLY oferta_curso
    ADD CONSTRAINT oferta_curso_pkey PRIMARY KEY (id);

ALTER TABLE ONLY oferta_disciplina_aluno
    ADD CONSTRAINT oferta_disciplina_aluno_pkey PRIMARY KEY (id);

ALTER TABLE ONLY oferta_disciplina
    ADD CONSTRAINT oferta_disciplina_pkey PRIMARY KEY (id);

ALTER TABLE ONLY oferta_disciplina_professor
    ADD CONSTRAINT oferta_disciplina_professor_pkey PRIMARY KEY (id);

ALTER TABLE ONLY professor
    ADD CONSTRAINT professor_pkey PRIMARY KEY (id);

ALTER TABLE ONLY tarefa_agendada
    ADD CONSTRAINT tarefa_agendada_pkey PRIMARY KEY (id);

ALTER TABLE ONLY frequencia
    ADD CONSTRAINT fk1u3e75vpgsvmv2dv9c4gx7iw3 FOREIGN KEY (oferta_disciplina_id) REFERENCES oferta_disciplina(id);

ALTER TABLE ONLY oferta_disciplina_aluno
    ADD CONSTRAINT fk39xjj0yqb0f6xct6dmvjsvq93 FOREIGN KEY (oferta_disciplina_id) REFERENCES oferta_disciplina(id);

ALTER TABLE ONLY oferta_curso
    ADD CONSTRAINT fk3bj650ssvdclhbti8ckfkom6y FOREIGN KEY (curso_id) REFERENCES curso(id);

ALTER TABLE ONLY frequencia_aluno
    ADD CONSTRAINT fk8pr3b0g8m55s11hjkkvq9rhob FOREIGN KEY (frequencia_id) REFERENCES frequencia(id);

ALTER TABLE ONLY frequencia_aluno
    ADD CONSTRAINT fka3n1l1ojrjxq5kjvxt6t9n138 FOREIGN KEY (oferta_disciplina_aluno_id) REFERENCES oferta_disciplina_aluno(id);
	
ALTER TABLE ONLY oferta_disciplina_professor
    ADD CONSTRAINT fkaqfgdcklyk23rdpq43h24mfah FOREIGN KEY (professor_id) REFERENCES professor(id);

ALTER TABLE ONLY oferta_curso
    ADD CONSTRAINT fkdau6lb1eym7t1s93p2nfr3py4 FOREIGN KEY (matriz_id) REFERENCES matriz(id);

ALTER TABLE ONLY oferta_curso
    ADD CONSTRAINT fkg7v6log7v9d93vs4hnxbj44ie FOREIGN KEY (ano_semestre_id) REFERENCES ano_semestre(id);

ALTER TABLE ONLY tarefa_agendada_depende
    ADD CONSTRAINT fkhiakdmklylb5j299hb5446gtt FOREIGN KEY (tarefa_pai) REFERENCES tarefa_agendada(id);

ALTER TABLE ONLY log_acesso
    ADD CONSTRAINT fki1ydhjt6ycepuivx1gtb9rx4t FOREIGN KEY (professor_id) REFERENCES professor(id);

ALTER TABLE ONLY oferta_disciplina
    ADD CONSTRAINT fkinao552q9dsu8g31treayceno FOREIGN KEY (oferta_curso_id) REFERENCES oferta_curso(id);

ALTER TABLE ONLY oferta_disciplina_professor
    ADD CONSTRAINT fkkfes2uauvs87ix2jc5pln3h9c FOREIGN KEY (oferta_disciplina_id) REFERENCES oferta_disciplina(id);

ALTER TABLE ONLY tarefa_agendada_depende
    ADD CONSTRAINT fklxd889d31g3sqab83ld2bio36 FOREIGN KEY (tarefa_filha) REFERENCES tarefa_agendada(id);

ALTER TABLE ONLY oferta_curso
    ADD CONSTRAINT fkqofidbqd8woega5b0p93gwo27 FOREIGN KEY (campus_id) REFERENCES campus(id);

ALTER TABLE ONLY oferta_disciplina_aluno
    ADD CONSTRAINT fkrfbj0d44jl4qo7colkqskrhee FOREIGN KEY (aluno_id) REFERENCES aluno(id);

ALTER TABLE ONLY oferta_disciplina
    ADD CONSTRAINT fktdn0w0uqgeoscsnvpauh7g3wv FOREIGN KEY (disciplina_id) REFERENCES disciplina(id);

