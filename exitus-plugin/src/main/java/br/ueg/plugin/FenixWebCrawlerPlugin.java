package br.ueg.plugin;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Predicate;

import br.ueg.exitus.modelo.Aluno;
import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.modelo.Campus;
import br.ueg.exitus.modelo.Curso;
import br.ueg.exitus.modelo.Disciplina;
import br.ueg.exitus.modelo.Frequencia;
import br.ueg.exitus.modelo.FrequenciaAluno;
import br.ueg.exitus.modelo.Matriz;
import br.ueg.exitus.modelo.OfertaCurso;
import br.ueg.exitus.modelo.OfertaDisciplina;
import br.ueg.exitus.modelo.OfertaDisciplinaAluno;
import br.ueg.exitus.modelo.OfertaDisciplinaProfessor;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.AbstractWebCrawler;
import br.ueg.exitus.utils.DriverPool;
import br.ueg.exitus.utils.ObjectUtils;
import br.ueg.exitus.utils.Retorno;
import br.ueg.exitus.utils.Severidade;
import br.ueg.plugin.util.PhantomJSDriverPool;

public class FenixWebCrawlerPlugin extends AbstractWebCrawler<PhantomJSDriver, PhantomJSDriverService> {

	private final static int TIME_IN_SECONDS = 60;
	private final static String FREQUENCIA_INICIO_URL = "https://www.adms.ueg.br/gestao_academica/modulos/frequencia/inicio.xhtml";
	private final static String FREQUENCIA_URL = "https://www.adms.ueg.br/gestao_academica/modulos/frequencia/frequencia.xhtml";
	private final static String ADMS_URL = "https://www.adms.ueg.br/auth/acesso/index";
	private final static String FENIX_URL = "https://www.adms.ueg.br/auth/u/gestao_academica";
	private final static String SAIR_URL = "https://www.adms.ueg.br/auth/usuario/logout/";
	
	@Override
	public Retorno<Professor> getProfessor(String usuario, String senha) {
		PhantomJSDriver driver = getWebDriver();
		driver.get(FENIX_URL);
		WebElement spanNome = driver.findElementByXPath("//*[@id=\"frm-navbar\"]/div/ul[2]/li[1]/span/span");
		Professor p = new Professor();
		p.setNome(spanNome.getAttribute("title"));
		p.setSenha(senha);
		p.setUsuario(usuario);

		return new Retorno<>(true, "", Severidade.SUCESSO, p);
	}


	@Override
	public Retorno<Object> autenticar(PhantomJSDriver driver, String usuario, String senha) {
		driver.get(ADMS_URL);
		driver.findElementById("cpf").sendKeys(usuario);
		driver.findElementById("senha").sendKeys(senha);
		driver.findElementByName("envio").click();
		if(driver.getCurrentUrl().equals("https://www.adms.ueg.br/auth/acesso/login")){
			WebElement elemento = driver.findElementByClassName("error-message");
			if(!ObjectUtils.isNullOrEmpty(elemento)){
				return new Retorno<>(false, elemento.findElement(By.tagName("p")).getText(), Severidade.ALERTA);
			}
		}
		driver.get(FENIX_URL);
		WebElement spanPerfil = driver.findElementByXPath("//*[@id=\"frm-navbar\"]/div/ul[2]/li[2]/span");
		Retorno<Object> ret = null;
		if(spanPerfil.getAttribute("innerHTML").contains("Discente")){
			ret = new Retorno<>(false, "Somente professores podem acessar", Severidade.ALERTA);
		}else{
			ret = new Retorno<>(true, "", Severidade.SUCESSO);
		}
		return ret;
	}

	@Override
	public void sair() {
		PhantomJSDriver driver = getWebDriver();
		driver.get(SAIR_URL);

	}

	@Override
	public String getIdentificadorUsuario() {
		return "CPF";
	}

	@Override
	public Retorno<AnoSemestre> getAnosSemestres() {
		Retorno<AnoSemestre> ret = new Retorno<>(true);
		PhantomJSDriver driver = getWebDriver();
		try{
			driver.navigate().to(FREQUENCIA_INICIO_URL);
			ArrayList<AnoSemestre> anosSemestres = new ArrayList<>();
			List<WebElement> elements = driver.findElement(getBySelectAnoSemestre())
					.findElements(By.xpath("./*"));
			//			System.out.println(elements.size());
			for(WebElement elemento : elements){
				if(elemento.getAttribute("innerHTML").equals(" .:Selecione um período:. ")) continue;
				AnoSemestre anoSemestre = new AnoSemestre();
				anoSemestre.setDescricao(elemento.getAttribute("innerHTML"));
				anosSemestres.add(anoSemestre);
			}
			ret.setLista(anosSemestres);
		}catch(Exception e){
			capturarScreenshot(driver);
			e.printStackTrace();
			ret.setOk(false);
			ret.setMensagem(e.getMessage());
		}

		return ret;
	}

	@Override
	protected DriverPool<PhantomJSDriver, PhantomJSDriverService> getDriverPool() {
		return PhantomJSDriverPool.getInstancia();
	}

	@Override
	public Retorno<Campus> getCampus() {
		Retorno<Campus> ret = new Retorno<>(true);
		PhantomJSDriver driver = getWebDriver();
		try{
			driver.navigate().to(FREQUENCIA_INICIO_URL);
			ArrayList<Campus> listaCampus = new ArrayList<>();
			List<WebElement> elements = driver.findElement(getBySelectCampus())
					.findElements(By.xpath("./*"));
			for(WebElement elemento : elements){
				if(elemento.getAttribute("innerHTML").equals("..:: Selecione ::..")) continue;
				Campus campus = new Campus();
				campus.setNome(elemento.getAttribute("innerHTML"));
				listaCampus.add(campus);
			}
			ret.setLista(listaCampus);
		}catch(Exception e){
			capturarScreenshot(driver);
			ret.setOk(false);
			ret.setMensagem(e.getMessage());
		}

		return ret;
	}

	@Override
	public Retorno<Curso> getCursos(AnoSemestre anoSemestre) {
		Retorno<Curso> ret = new Retorno<>(true);
		PhantomJSDriver driver = getWebDriver();
		try {
			ArrayList<Curso> cursos = new ArrayList<>();
			Retorno<Campus> retCampus = getCampus();
			if(!retCampus.isOk()){
				throw new Exception(retCampus.getMensagem());
			}
			driver.navigate().to(FREQUENCIA_INICIO_URL);
			for(Campus campus : retCampus.getLista()){
				List<String> nomeCursos = getCursoOuMatriz(driver, 0, campus, anoSemestre);
				for(String descricaoMatriz : nomeCursos){
					if(verificarSeExisteCursoLista(cursos, descricaoMatriz)) continue;
					Curso curso = new Curso();
					curso.setNome(descricaoMatriz);
					cursos.add(curso);
				}
			}
			ret.setLista(cursos);
		} catch (Exception e) {
			capturarScreenshot(driver);
			e.printStackTrace();
			ret.setOk(false);
			ret.setMensagem(e.getMessage());
		}

		return ret;
	}

	@Override
	public Retorno<Matriz> getMatrizes(AnoSemestre anoSemestre) {
		Retorno<Matriz> ret = new Retorno<>(true);
		PhantomJSDriver driver = getWebDriver();
		try {
			ArrayList<Matriz> matrizes = new ArrayList<>();
			Retorno<Campus> retCampus = getCampus();
			if(!retCampus.isOk()){
				throw new Exception(ret.getMensagem());
			}
			driver.navigate().to(FREQUENCIA_INICIO_URL);
			for(Campus campus : retCampus.getLista()){
				List<String> descricaoMatrizes = getCursoOuMatriz(driver, 1, campus, anoSemestre);
				for(String descricaoMatriz : descricaoMatrizes){
					if(verificarSeExisteMatrizLista(matrizes, descricaoMatriz) != null) continue;
					Matriz matriz = new Matriz();
					matriz.setDescricao(descricaoMatriz);
					matrizes.add(matriz);
				}
			}
			ret.setLista(matrizes);
		} catch (Exception e) {
			capturarScreenshot(driver);
			e.printStackTrace();
			ret.setOk(false);
			ret.setMensagem(e.getMessage());
		}

		return ret;

	}

	public Matriz verificarSeExisteMatrizLista(List<Matriz> lista, String matriz1){
		for(Matriz matriz2 : lista){
			if(matriz2.getDescricao().equals(matriz1)){
				return matriz2;
			}
		}
		return null;
	}

	public boolean verificarSeExisteCursoLista(List<Curso> lista, String curso1){
		for(Curso curso2 : lista){
			if(curso2.getNome().equals(curso1)){
				return true;
			}
		}
		return false;
	}

	public List<String> getCursoOuMatriz(PhantomJSDriver driver, int posicao, Campus campus, AnoSemestre anoSemestre) throws Exception{
		List<String> ret = new ArrayList<>();

		//Selecionar ano semestre
		selecionarAnoSemestre(driver, anoSemestre);

		//Selecionar campus
		selecionarCampus(driver, campus);

		//Curso
		WebElement selectMatrizCurricular = waitUntilElementBecomesObsolete(driver, getBySelectMatrizCurricular());
		List<WebElement> optionsMatrizCurricular =	selectMatrizCurricular.findElements(By.xpath("./*"));
		for(WebElement element : optionsMatrizCurricular){
			if(element.getAttribute("innerHTML").equals("..:: Selecione ::..")) continue;
			String value = element.getAttribute("innerHTML").split("–")[posicao].trim();
			ret.add(value);
		}

		return ret;
	}

	@Override
	public Retorno<OfertaCurso> getOfertasCurso(AnoSemestre anoSemestre){
		Retorno<OfertaCurso> ret = new Retorno<>(true);
		ArrayList<OfertaCurso> lista = new ArrayList<>();
		ret.setLista(lista);
		PhantomJSDriver driver = getWebDriver();
		try {
			driver.navigate().to(FREQUENCIA_INICIO_URL);
			selecionarAnoSemestre(driver, anoSemestre);
			List<WebElement> optionsCampus = driver.findElement(getBySelectCampus())
					.findElements(By.xpath("./*"));
			for(WebElement optionCampus : optionsCampus){
				if(optionCampus.getAttribute("innerHTML").equals("..:: Selecione ::..")) continue;
				Campus campus = new Campus();
				campus.setNome(optionCampus.getAttribute("innerHTML"));
				selecionarCampus(driver, campus);
				WebElement selectMatrizCurricular = waitUntilElementBecomesObsolete(driver, getBySelectMatrizCurricular());
				List<WebElement> optionsMatrizCurricular =	selectMatrizCurricular.findElements(By.xpath("./*"));
				for(WebElement optionMatrizCurricular : optionsMatrizCurricular){
					if(optionMatrizCurricular.getAttribute("innerHTML").equals("..:: Selecione ::..")) continue;
					String cursoEmatriz = optionMatrizCurricular.getAttribute("innerHTML");
					Curso curso = new Curso();
					curso.setNome(cursoEmatriz.split("–")[0].trim());
					Matriz matriz = new Matriz();
					matriz.setDescricao(cursoEmatriz.split("–")[1].trim());
					lista.add(new OfertaCurso(curso, matriz, anoSemestre, campus));
				}
			}
		} catch (Exception e) {
			capturarScreenshot(driver);
			e.printStackTrace();
			ret.setOk(false);
			ret.setMensagem(e.getMessage());
		}
		return ret;
	}

	@Override
	public Retorno<OfertaDisciplina> getOfertasDisciplina(List<OfertaCurso> ofertasCurso) {
		Retorno<OfertaDisciplina> ret = new Retorno<>(true);
		ArrayList<OfertaDisciplina> lista = new ArrayList<>();
		ret.setLista(lista);
		PhantomJSDriver driver = getWebDriver();
		try {
			for(OfertaCurso ofertaCurso : ofertasCurso){
				driver.navigate().to(FREQUENCIA_INICIO_URL);
				//Selecionar ano semestre
				selecionarAnoSemestre(driver, ofertaCurso.getAnoSemestre());

				//Selecionar campus
				selecionarCampus(driver, ofertaCurso.getCampus());

				//Curso
				selecionarMatrizCurricular(driver, ofertaCurso.getCurso());

				ArrayList<Disciplina> disciplinas = new ArrayList<>();
				waitUntilElementAppears(driver, By.id("frm:fkDisciplinasOferecidas:btnPopupGradesDisciplinas")).click();
				WebElement tabelaDisciplinas = waitUntilElementAppears(driver, By.id("frm:fkDisciplinasOferecidas:panelTabela_data"));
				List<WebElement> linhas = tabelaDisciplinas.findElements(By.xpath("./*"));
				for(WebElement linha : linhas){
					if(linha.findElement(By.xpath("./td[6]")).getText().equals("AMBIENTE100% À DISTÂNCIA")) continue;
					Disciplina disciplina = new Disciplina();
					disciplina.setId(Long.valueOf(linha.findElement(By.xpath("./td[1]")).getText().substring(1)));
					disciplina.setNome(linha.findElement(By.xpath("./td[2]")).getText().substring(10));
					disciplinas.add(disciplina);
					OfertaDisciplina ofertaDisciplina = new OfertaDisciplina(disciplina, ofertaCurso);
					try{
						ofertaDisciplina.setAulasPrevistas(Integer.valueOf(
								linha.findElement(By.xpath("./td[9]")).getText().substring(15)));
						ofertaDisciplina.setAulasLancadas(Integer.valueOf(
								linha.findElement(By.xpath("./td[10]")).getText().substring(14)));
					}catch(NumberFormatException e){
						e.printStackTrace();
					}
					lista.add(ofertaDisciplina);
				}
			}
		} catch (Exception e) {
			capturarScreenshot(driver);
			e.printStackTrace();
			ret.setOk(false);
			ret.setMensagem(e.getMessage());
		}
		return ret;
	}

	@Override
	public Retorno<OfertaDisciplinaProfessor> getOfertasDisciplinaProfessor(List<OfertaDisciplina> ofertasDisciplina) {
		Retorno<OfertaDisciplinaProfessor> ret = new Retorno<>(true);
		ArrayList<OfertaDisciplinaProfessor> lista = new ArrayList<>();
		ret.setLista(lista);
		PhantomJSDriver driver = getWebDriver();
		try {

			driver.navigate().to(FREQUENCIA_INICIO_URL);
			//Selecionar ano semestre
			selecionarAnoSemestre(driver, ofertasDisciplina.get(0).getOfertaCurso().getAnoSemestre());

			//Selecionar campus
			selecionarCampus(driver,  ofertasDisciplina.get(0).getOfertaCurso().getCampus());

			//Curso
			selecionarMatrizCurricular(driver,  ofertasDisciplina.get(0).getOfertaCurso().getCurso());

			ArrayList<Disciplina> disciplinas = new ArrayList<>();
			waitUntilElementAppears(driver, By.id("frm:fkDisciplinasOferecidas:btnPopupGradesDisciplinas")).click();
			WebElement tabelaDisciplinas = waitUntilElementAppears(driver, By.id("frm:fkDisciplinasOferecidas:panelTabela_data"));
			List<WebElement> linhas = tabelaDisciplinas.findElements(By.xpath("./*"));
			for(WebElement linha : linhas){
				if(linha.findElement(By.xpath("./td[6]")).getText().equals("AMBIENTE100% À DISTÂNCIA")) continue;
				Disciplina disciplina = new Disciplina();
				disciplina.setId(Long.valueOf(linha.findElement(By.xpath("./td[1]")).getText().substring(1)));
				disciplina.setNome(linha.findElement(By.xpath("./td[2]")).getText().substring(10));
				OfertaDisciplina ofertaDisciplina = null;
				for(OfertaDisciplina oDisciplina : ofertasDisciplina){
					if(disciplina.getNome().equals(oDisciplina.getDisciplina().getNome())){
						ofertaDisciplina = oDisciplina;
					}
				}
				disciplinas.add(disciplina);
				WebElement spanNome = driver.findElementByXPath("//*[@id=\"frm-navbar\"]/div/ul[2]/li[1]/span/span");
				Professor professor = new Professor();
				professor.setNome(spanNome.getAttribute("title"));
				professor.setSenha(getPooledDriver().getSenha());
				professor.setUsuario(getPooledDriver().getUsuario());
				OfertaDisciplinaProfessor ofertaDisciplinaProfessor = new OfertaDisciplinaProfessor(professor, ofertaDisciplina);
				lista.add(ofertaDisciplinaProfessor);
			}
		} catch (Exception e) {
			capturarScreenshot(driver);
			e.printStackTrace();
			ret.setOk(false);
			ret.setMensagem(e.getMessage());
		}
		return ret;
	}

	@Override
	public Retorno<OfertaDisciplinaAluno> getOfertasDisciplinaAluno(OfertaDisciplina ofertaDisciplina) {
		Retorno<OfertaDisciplinaAluno> ret = new Retorno<>(true);
		PhantomJSDriver driver = getWebDriver();
		ArrayList<OfertaDisciplinaAluno> lista = new ArrayList<>();
		ret.setLista(lista);
		try {
			driver.navigate().to(FREQUENCIA_INICIO_URL);

			selecionarAnoSemestre(driver, ofertaDisciplina.getOfertaCurso().getAnoSemestre());

			selecionarCampus(driver, ofertaDisciplina.getOfertaCurso().getCampus());

			selecionarMatrizCurricular(driver, ofertaDisciplina.getOfertaCurso().getCurso());

			selecionarDisciplina(driver, ofertaDisciplina.getDisciplina());

			selecionarDiaFrequencia(driver, new Date(), 1);

			WebElement tabelaAlunos = waitUntilElementAppears(driver, By.id("frmAlunos:alunos_data"));
			List<WebElement> linhasAluno = tabelaAlunos.findElements(By.xpath("./*"));
			for(WebElement linha : linhasAluno){
				Aluno aluno = new Aluno();
				aluno.setId(Long.valueOf(linha.findElement(By.xpath("./td[2]")).getText()));
				aluno.setNome(linha.findElement(By.xpath("./td[3]")).getText());
				lista.add(new OfertaDisciplinaAluno(aluno, ofertaDisciplina));
			}
		}catch (Exception e) {
			capturarScreenshot(driver);
			e.printStackTrace();
			ret.setOk(false);
			ret.setMensagem(e.getMessage());
		}

		return ret;
	}


	@Override
	public Retorno<Disciplina> getDisciplinas(List<OfertaCurso> ofertasCurso) {
		Retorno<Disciplina> ret = new Retorno<>(true);
		PhantomJSDriver driver = getWebDriver();
		ArrayList<Disciplina> disciplinas = new ArrayList<>();
		ret.setLista(disciplinas);
		try {
			for(OfertaCurso ofertaCurso : ofertasCurso){
				driver.navigate().to(FREQUENCIA_INICIO_URL);
				//Selecionar ano semestre
				selecionarAnoSemestre(driver, ofertaCurso.getAnoSemestre());

				//Selecionar campus
				selecionarCampus(driver, ofertaCurso.getCampus());

				//Curso
				if(selecionarMatrizCurricular(driver, ofertaCurso.getCurso()).isEmpty()) continue;

				waitUntilElementAppears(driver, By.id("frm:fkDisciplinasOferecidas:btnPopupGradesDisciplinas")).click();
				WebElement tabelaDisciplinas = waitUntilElementAppears(driver, By.id("frm:fkDisciplinasOferecidas:panelTabela_data"));
				List<WebElement> linhas = tabelaDisciplinas.findElements(By.xpath("./*"));
				for(WebElement linha : linhas){
					if(linha.findElement(By.xpath("./td[6]")).getText().equals("AMBIENTE100% À DISTÂNCIA")) continue;
					Disciplina disciplina = new Disciplina();
					disciplina.setId(Long.valueOf(linha.findElement(By.xpath("./td[1]")).getText().substring(1)));
					disciplina.setNome(linha.findElement(By.xpath("./td[2]")).getText().substring(10));
					disciplinas.add(disciplina);
				}
			}
		} catch (Exception e) {
			capturarScreenshot(driver);
			e.printStackTrace();
			ret.setOk(false);
			ret.setMensagem(e.getMessage());
		}

		return ret;
	}

	public Retorno<Aluno> getAlunos(OfertaDisciplina ofertaDisciplina){
		Retorno<Aluno> ret = new Retorno<>(true);
		PhantomJSDriver driver = getWebDriver();
		try {
			driver.navigate().to(FREQUENCIA_INICIO_URL);

			selecionarAnoSemestre(driver, ofertaDisciplina.getOfertaCurso().getAnoSemestre());

			selecionarCampus(driver, ofertaDisciplina.getOfertaCurso().getCampus());

			selecionarMatrizCurricular(driver, ofertaDisciplina.getOfertaCurso().getCurso());

			selecionarDisciplina(driver, ofertaDisciplina.getDisciplina());

			ArrayList<Aluno> alunos = new ArrayList<>();

			selecionarDiaFrequencia(driver, new Date(), 1);

			WebElement tabelaAlunos = waitUntilPageLoad(driver, By.id("frmAlunos:alunos_data"), FREQUENCIA_URL);
			List<WebElement> linhasAluno = tabelaAlunos.findElements(By.xpath("./*"));
			for(WebElement linha : linhasAluno){
				Aluno aluno = new Aluno();
				aluno.setId(Long.valueOf(linha.findElement(By.xpath("./td[2]")).getText()));
				aluno.setNome(linha.findElement(By.xpath("./td[3]")).getText());
				alunos.add(aluno);
			}
			ret.setLista(alunos);
		}catch (Exception e) {
			capturarScreenshot(driver);
			e.printStackTrace();
			ret.setOk(false);
			ret.setMensagem(e.getMessage());
		}

		return ret;
	}

	@Override
	public Retorno<Frequencia> getFrequencia(Date diaFrequencia,
			OfertaDisciplina ofertaDisciplina) {
		Retorno<Frequencia> ret = new Retorno<>(true);
		PhantomJSDriver driver = getWebDriver();
		try {
			driver.navigate().to(FREQUENCIA_INICIO_URL);

			selecionarAnoSemestre(driver, ofertaDisciplina.getOfertaCurso().getAnoSemestre());

			selecionarCampus(driver, ofertaDisciplina.getOfertaCurso().getCampus());

			selecionarMatrizCurricular(driver, ofertaDisciplina.getOfertaCurso().getCurso());

			selecionarDisciplina(driver, ofertaDisciplina.getDisciplina());

			selecionarDiaFrequencia(driver, diaFrequencia, 0);

			ArrayList<Frequencia> frequencias = new ArrayList<>();
			ret.setLista(frequencias);
			WebElement tabelaAlunos = null;
			try{
				tabelaAlunos = waitUntilPageLoad(driver, By.id("frmAlunos:alunos_data"), FREQUENCIA_URL);
			}catch(TimeoutException e){
				ret.setMensagem(getMensagemSistema(driver));
				return ret;
			}
			List<WebElement> linhasAluno = tabelaAlunos.findElements(By.xpath("./*"));
			for(WebElement linha : linhasAluno){
				for(OfertaDisciplinaAluno oDisciplinaAluno : ofertaDisciplina.getOfertaDisciplinaAlunos()){
					if(Long.valueOf(linha.findElement(By.xpath("./td[2]")).getText()).equals(oDisciplinaAluno.getAluno().getId())){
						List<WebElement> colunasAluno = linha.findElements(By.xpath("./*"));
						for(int i = 3; i < colunasAluno.size()-4; i++){
							try{
								WebElement inputCheckBox = colunasAluno.get(i).findElement(By.xpath("./div/div[1]/input"));
								Frequencia frequencia = null;
								if((i -3) >= frequencias.size()){
									frequencia = new Frequencia(diaFrequencia, i-2, oDisciplinaAluno.getOfertaDisciplina());
									frequencias.add(frequencia);
								}else{
									frequencia = frequencias.get(i -3);
								}
								FrequenciaAluno frequenciaAluno = new FrequenciaAluno();
								frequenciaAluno.setOfertaDisciplinaAluno(oDisciplinaAluno);
								String value = inputCheckBox.getAttribute("checked");
								frequenciaAluno.setSituacao((value != null && value.equals("true")));
								frequenciaAluno.setFrequencia(frequencia);
								frequencia.getFrequenciaAlunos().add(frequenciaAluno);
							}catch(NoSuchElementException e){}
						}
					}
				}
			}
		}catch (Exception e) {
			capturarScreenshot(driver);
			e.printStackTrace();
			ret.setOk(false);
			ret.setMensagem(e.getMessage());
		}

		return ret;
	}

	private String getMensagemSistema(PhantomJSDriver driver) {
		return driver.findElement(By.xpath("//*[@id=\"messages\"]/div/ul/li/span")).getAttribute("innerHTML");
	}

	@Override
	public Retorno<Frequencia> salvarFrequencia(List<Frequencia> frequencias, Integer quantidadeAulas) {
		Retorno<Frequencia> ret = new Retorno<>(true);
		PhantomJSDriver driver = getWebDriver();
		try {
			driver.navigate().to(FREQUENCIA_INICIO_URL);
			OfertaDisciplina ofertaDisciplina = frequencias.get(0).getOfertaDisciplina();
			selecionarAnoSemestre(driver, ofertaDisciplina.getOfertaCurso().getAnoSemestre());

			selecionarCampus(driver, ofertaDisciplina.getOfertaCurso().getCampus());

			selecionarMatrizCurricular(driver, ofertaDisciplina.getOfertaCurso().getCurso());

			selecionarDisciplina(driver, ofertaDisciplina.getDisciplina());

			selecionarDiaFrequencia(driver, frequencias.get(0).getDataFrequencia(), quantidadeAulas);

			WebElement tabelaAlunos = null;
			try{
				tabelaAlunos = waitUntilPageLoad(driver, By.id("frmAlunos:alunos_data"), FREQUENCIA_URL);
			}catch(TimeoutException e){
				WebElement mensagem = driver.findElement(By.xpath("//*[@id=\"messages\"]/div/ul/li/span"));
				ret.setMensagem(mensagem.getAttribute("innerHTML"));
				ret.setOk(false);
				return ret;
			}
			List<WebElement> linhasAluno = tabelaAlunos.findElements(By.xpath("./*"));
			for(Frequencia frequencia : frequencias){
				for(WebElement linha : linhasAluno){
					for(FrequenciaAluno frequenciAluno : frequencia.getFrequenciaAlunos()){
						OfertaDisciplinaAluno ofertaDisciplinaAluno = frequenciAluno.getOfertaDisciplinaAluno();
						if(Long.valueOf(linha.findElement(By.xpath("./td[2]")).getText()).equals(ofertaDisciplinaAluno.getAluno().getId())){
							List<WebElement> colunasAluno = linha.findElements(By.xpath("./*"));
							WebElement inputCheckbox = colunasAluno.get(frequencia.getAula() + 2).findElement(By.xpath("./div/div[1]/input"));
							WebElement checkbox = colunasAluno.get(frequencia.getAula() + 2).findElement(By.xpath("./div/div[2]"));
							String value = inputCheckbox.getAttribute("checked");
							Boolean situacao = value != null && value.equals("true");
							if(situacao.equals(frequenciAluno.getSituacao())) continue;
							checkbox.click();
						}
					}
				}
			}
			//Salva a frequencia
			waitUntilElementBeClickable(driver, By.id("frmAlunos:j_idt145")).click();
			waitUntilPageLoad(driver, null, FREQUENCIA_INICIO_URL);
			ret.setMensagem(getMensagemSistema(driver));
		}catch (Exception e) {
			capturarScreenshot(driver);
			e.printStackTrace();
			ret.setOk(false);
			ret.setMensagem(e.getMessage());
		}

		return ret;
	}


	@Override
	public Retorno<Integer> getquantidadeAulaLancada(Date diaFrequencia, OfertaDisciplina ofertaDisciplina) {
		Retorno<Integer> ret = new Retorno<>(true);
		PhantomJSDriver driver = getWebDriver();
		try {
			driver.navigate().to(FREQUENCIA_INICIO_URL);
			selecionarAnoSemestre(driver, ofertaDisciplina.getOfertaCurso().getAnoSemestre());

			selecionarCampus(driver, ofertaDisciplina.getOfertaCurso().getCampus());

			selecionarMatrizCurricular(driver, ofertaDisciplina.getOfertaCurso().getCurso());

			selecionarDisciplina(driver, ofertaDisciplina.getDisciplina());

			selecionarDiaFrequencia(driver, diaFrequencia, 0);

			WebElement tabelaAlunoHeader = null;
			try{
				tabelaAlunoHeader = waitUntilPageLoad(driver, By.xpath("//*[@id=\"frmAlunos:alunos_head\"]/tr"), FREQUENCIA_URL);
			}catch(TimeoutException e){
				WebElement mensagem = driver.findElement(By.xpath("//*[@id=\"messages\"]/div/ul/li/span"));
				ret.setMensagem(mensagem.getAttribute("innerHTML"));
				ret.setOk(true);
				ret.setValor(0);
				return ret;
			}
			List<WebElement> colunasAlunoHeader = tabelaAlunoHeader.findElements(By.xpath("./*"));
			ret.setValor(colunasAlunoHeader.size()-7);
		}catch (Exception e) {
			capturarScreenshot(driver);
			e.printStackTrace();
			ret.setOk(false);
			ret.setMensagem(e.getMessage());
		}

		return ret;
	}

	private void selecionarCampus(WebDriver driver, Campus campus){
		waitUntilElementBeClickable(driver, getByCampus()).click();
		List<WebElement> optionsCampus = waitUntilElementBeVisible(driver, getByOptionsCampus())
				.findElements(By.xpath("./*"));
		for(WebElement optionCampus : optionsCampus){
			if(optionCampus.getAttribute("data-label").equals(campus.getNome())){
				optionCampus.click();
			}
		}
	}

	private void selecionarAnoSemestre(WebDriver driver, AnoSemestre anoSemestre){
		waitUntilElementBeClickable(driver, getByAnoSemestre()).click();
		List<WebElement> optionsAnoSemestre = waitUntilElementBeClickable(driver, getByOptionsAnoSemestre())
				.findElements(By.xpath("./*"));
		for(WebElement optionAnoSemestre : optionsAnoSemestre){
			if(optionAnoSemestre.getAttribute("data-label").equals(anoSemestre.getDescricao())){
				optionAnoSemestre.click();
			}
		}
	}

	private String selecionarMatrizCurricular(WebDriver driver, Curso curso){
		waitUntilElementBecomesObsolete(driver, getBySelectMatrizCurricular());
		waitUntilElementBeClickable(driver, getByMatrizCurricular()).click();
		List<WebElement> optionsMatrizCurricular = waitUntilElementBeVisible(driver, getByOptionsMatrizCurricular())
				.findElements(By.xpath("./*"));
		for(WebElement optionMatrizCurricular : optionsMatrizCurricular){
			if(optionMatrizCurricular.getAttribute("data-label").contains(curso.getNome())){
				optionMatrizCurricular.click();
				return optionMatrizCurricular.getAttribute("data-label").split("–")[1].trim();
			}
		}
		return "";
	}

	private void selecionarDisciplina(WebDriver driver, Disciplina disciplina){
		waitUntilElementAppears(driver, By.id("frm:fkDisciplinasOferecidas:btnPopupGradesDisciplinas")).click();
		WebElement tabelaDisciplinas = waitUntilElementAppears(driver, By.id("frm:fkDisciplinasOferecidas:panelTabela_data"));
		List<WebElement> linhasDisciplina = tabelaDisciplinas.findElements(By.xpath("./*"));
		for(WebElement linha : linhasDisciplina){
			if(linha.findElement(By.xpath("./td[1]")).getText().substring(1).equals(disciplina.getId().toString())){
				linha.findElement(By.id("frm:fkDisciplinasOferecidas:panelTabela:"+ linha.getAttribute("data-ri")
				+":cmdEnviarDocente")).click();
			}
		}
	}

	private void selecionarDiaFrequencia(WebDriver driver, Date date, Integer quantidadeAulas){
		WebElement diaDaAula = waitUntilElementAppears(driver, By.id("frm:dtInicioAula_input"));
		System.out.println(new SimpleDateFormat("dd/MM/yyyy").format(date));
		diaDaAula.sendKeys("value", new SimpleDateFormat("dd/MM/yyyy").format(date));
		driver.findElement(By.id("frm:ocorrencias_input")).sendKeys(quantidadeAulas.toString());
		waitUntilElementBeClickable(driver, By.id("frm:j_idt140")).click();
	}

	private By getByCampus(){
		return By.xpath("//*[@id=\"frm:depId_label\"]");
	}

	private By getByOptionsCampus(){
		return By.xpath("//*[@id=\"frm:depId_panel\"]/div[2]/table/tbody");
	}

	private By getBySelectCampus(){
		return By.id("frm:depId_input");
	}

	private By getByAnoSemestre(){
		return By.xpath("//*[@id=\"frm:perOid_label\"]");
	}

	private By getByOptionsAnoSemestre(){
		return By.xpath("//*[@id=\"frm:perOid_panel\"]/div[2]/table/tbody");
	}

	private By getBySelectAnoSemestre(){
		return By.id("frm:perOid_input");
	}

	private By getByMatrizCurricular(){
		return By.xpath("//*[@id=\"frm:oferOid_label\"]");
	}

	private By getByOptionsMatrizCurricular(){
		return By.xpath("//*[@id=\"frm:oferOid_panel\"]/div[2]/table/tbody");
	}

	private By getBySelectMatrizCurricular(){
		return By.id("frm:oferOid_input");
	}

	private WebElement waitUntilElementBecomesObsolete(WebDriver driver, By by){
		return waitUntil(driver, TIME_IN_SECONDS, by, ExpectedConditions.stalenessOf(driver.findElement(by)));
	}

	private WebElement waitUntilElementAppears(WebDriver driver, By by){
		return waitUntil(driver, TIME_IN_SECONDS, by, ExpectedConditions.presenceOfElementLocated(by));
	}

	private WebElement waitUntilPageLoad(RemoteWebDriver driver, By by, String url){
		WebElement element = waitUntil(driver, TIME_IN_SECONDS, by, ExpectedConditions.urlContains(url));
		new WebDriverWait(driver, TIME_IN_SECONDS).until(new Predicate<WebDriver>() {

			@Override
			public boolean apply(WebDriver driver) {
				return ((RemoteWebDriver)driver).executeScript("return document.readyState").equals("complete");
			}
		});
		return element;
	}

	private WebElement waitUntilElementBeClickable(WebDriver driver, By by){
		return waitUntil(driver, TIME_IN_SECONDS, by, ExpectedConditions.elementToBeClickable(by));
	}

	private WebElement waitUntilElementBeVisible(WebDriver driver, By by){
		return waitUntil(driver, TIME_IN_SECONDS, by, ExpectedConditions.visibilityOfElementLocated(by));
	}

	private WebElement waitUntil(WebDriver driver, long seconds, By by, ExpectedCondition<?> condition){
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		wait.until(condition);
		if(by == null){
			return null;
		}
		return driver.findElement(by);
	}

	private void capturarScreenshot(RemoteWebDriver driver){
		File file = driver.getScreenshotAs(OutputType.FILE);
		File copia =  new File("C:\\Users\\jonas\\Pictures\\Capturas de tela phantom\\" + file.getName());
		try {
			FileUtils.copyFile(file, copia);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
