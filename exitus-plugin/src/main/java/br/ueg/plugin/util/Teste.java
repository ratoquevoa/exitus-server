package br.ueg.plugin.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import br.ueg.exitus.controle.OfertaCursoControle;
import br.ueg.exitus.modelo.Aluno;
import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.modelo.Campus;
import br.ueg.exitus.modelo.Curso;
import br.ueg.exitus.modelo.Disciplina;
import br.ueg.exitus.modelo.Frequencia;
import br.ueg.exitus.modelo.FrequenciaAluno;
import br.ueg.exitus.modelo.Matriz;
import br.ueg.exitus.modelo.OfertaCurso;
import br.ueg.exitus.modelo.OfertaDisciplina;
import br.ueg.exitus.modelo.OfertaDisciplinaAluno;
import br.ueg.exitus.modelo.OfertaDisciplinaProfessor;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.IPlugin;
import br.ueg.exitus.plugin.PluginCapability;
import br.ueg.exitus.plugin.PluginFactory;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.utils.Retorno;

public class Teste {

	public static void main(String[] args) {
		long inicio = System.currentTimeMillis();
		//		System.out.println(new SimpleDateFormat("ddMMyyyy").format(new Date()));
		//		testarPool();
		//		testarConcurrentModification();
//		try {
//			testarPlugin();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		PluginCapability capabilities = new PluginCapability();
//		capabilities.setCabapility(PluginCapability.USER, "98030175191");
//		capabilities.setCabapility(PluginCapability.PASSWORD, "AlunoSI20182");
//		capabilities.setCabapility(PluginCapability.PLUGIN_PATH, "br.ueg.plugin.FenixWebCrawlerPlugin");
//		IPlugin plugin;
//		try {
//			plugin = PluginFactory.getInstancia().getPlugin(capabilities);
//					buscarOfertasDisciplinas(plugin, buscarOfertaCursos(plugin).getLista()).getLista().get(2);
//			
//			
//		} catch (PluginInitException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
		
//		List<String> optionsMatrizCurricular = new ArrayList<>();
//		List<String> ret = new ArrayList<>();
//		optionsMatrizCurricular.add("1");
//		optionsMatrizCurricular.add("2");
//		optionsMatrizCurricular.add("1");
//		optionsMatrizCurricular.add("3");
//		System.out.println(new FenixWebCrawlerPlugin().verificarSeExisteNaLista(optionsMatrizCurricular, "3"));
		long fim = System.currentTimeMillis();
		System.out.println("\nTempo de execu��o: " + ((fim-inicio)/1000D) + " segundos");
	}

	private static Retorno<OfertaDisciplina> buscarOfertasDisciplinas(IPlugin plugin, List<OfertaCurso> lista) {
		Retorno<OfertaDisciplina> ret = plugin.getOfertasDisciplina(lista);
		if(!ret.isOk()){
			System.out.println(ret.getMensagem());
			return ret;
		}
		for(OfertaDisciplina ofertaDisciplina : ret.getLista()){
			System.out.println(ofertaDisciplina.getDisciplina().getNome());
			System.out.println(ofertaDisciplina.getOfertaCurso().getNomeCursoMatriz());
		}
		return ret;
	}

	private static Retorno<OfertaDisciplinaAluno> buscarOfertasDisciplinasAluno(IPlugin plugin, OfertaDisciplinaProfessor ofertaDisciplinaProfessor) {
		Retorno<OfertaDisciplinaAluno> ret = null; //plugin.getOfertasDisciplinaAluno(ofertaDisciplinaProfessor);
		if(!ret.isOk()){
			System.out.println(ret.getMensagem());
			return ret;
		}
		for(OfertaDisciplinaAluno ofertaDisciplinaAluno : ret.getLista()){
			System.out.println(ofertaDisciplinaAluno.getAluno().getNome());
			System.out.println(ofertaDisciplinaAluno.getOfertaDisciplina().getDisciplina().getNome());
		}
		return ret;
	}

	private static Retorno<OfertaDisciplinaProfessor> buscarOfertasDisciplinasProfessor(IPlugin plugin, List<OfertaCurso> ofertaCursos) {
//		Retorno<OfertaDisciplinaProfessor> ret = plugin.getOfertasDisciplinaProfessor(ofertaCursos);
//		if(!ret.isOk()){
//			System.out.println(ret.getMensagem());
//			return ret;
//		}
//		for(OfertaDisciplinaProfessor OfertaDisciplinaProfessor : ret.getLista()){
//			System.out.println(OfertaDisciplinaProfessor.getOfertaDisciplina().getDisciplina().getNome());
//			System.out.println(OfertaDisciplinaProfessor.getProfessor().getNome());
//		}
//		return ret;
		return null;
	}

	private static Retorno<OfertaCurso> buscarOfertaCursos(IPlugin plugin) {
		AnoSemestre anoSemestre1 = new AnoSemestre();
		anoSemestre1.setDescricao("2018/2");
		Retorno<OfertaCurso> ret = plugin.getOfertasCurso(anoSemestre1);
		if(!ret.isOk()){
			System.out.println(ret.getMensagem());
			return ret;
		}
		for(OfertaCurso ofertaCurso : ret.getLista()){
			System.out.println(ofertaCurso.getAnoSemestre().getDescricao());
			System.out.println(ofertaCurso.getNomeCursoMatriz());
			System.out.println(ofertaCurso.getCampus().getNome());
		}
		return ret;
	}

	private static void testarPlugin() throws ClassNotFoundException, InstantiationException, IllegalAccessException{
		IPlugin plugin = null;
		try {
			PluginCapability capabilities = new PluginCapability();
			capabilities.setCapability(PluginCapability.USER, "98030175191");
			capabilities.setCapability(PluginCapability.PASSWORD, "AlunoSI20182");
			capabilities.setCapability(PluginCapability.PLUGIN_PATH, "br.ueg.plugin.FenixWebCrawlerPlugin");
			plugin = PluginFactory.getInstancia().getPlugin(capabilities);
		} catch (PluginInitException e) {
			e.printStackTrace();
			return;
		}
		Retorno<Professor> retProf = buscarProfessor(plugin);
		if(!retProf.isOk()){
			System.out.println(retProf.getMensagem());
			return;
		}
		Professor professor = retProf.getValor();
		try {
			PluginCapability capabilities = new PluginCapability();
			capabilities.setCapability(PluginCapability.USER, "98030175191");
			capabilities.setCapability(PluginCapability.PASSWORD, "AlunoSI20182");
			capabilities.setCapability(PluginCapability.PLUGIN_PATH, "br.ueg.plugin.FenixWebCrawlerPlugin");
			plugin = PluginFactory.getInstancia().getPlugin(capabilities);
		} catch (PluginInitException e) {
			e.printStackTrace();
			return;
		}
		Retorno<AnoSemestre> retAnoSemestre = buscarAnosSemestres(plugin);
		if(!retAnoSemestre.isOk()){
			System.out.println(retAnoSemestre.getMensagem());
			return;
		}
		Retorno<Campus> retCampus = buscarCampus(plugin);
		if(!retCampus.isOk()){
			System.out.println(retCampus.getMensagem());
			return;
		}
		AnoSemestre anoSemestreEscolhido = retAnoSemestre.getLista().get(1);
		List<OfertaCurso> ofertaCursos = new ArrayList<>();
		for(Campus campus : retCampus.getLista()){
			Retorno<Curso> retCurso = buscarCursos(plugin, anoSemestreEscolhido);
			if(!retCurso.isOk()){
				System.out.println(retCurso.getMensagem());
				return;
			}
			Retorno<Matriz> retMatriz = buscarMatrizes(plugin, anoSemestreEscolhido);
			if(!retMatriz.isOk()){
				System.out.println(retMatriz.getMensagem());
				return;
			}
			for(int i = 0; i < retCurso.getLista().size(); i++){
				Curso curso = retCurso.getLista().get(i);
				Matriz matriz = retMatriz.getLista().get(i);
				OfertaCurso ofertaCurso = new OfertaCurso(curso, matriz, anoSemestreEscolhido, campus);
				ofertaCursos.add(ofertaCurso);
			}
		}
		List<OfertaDisciplinaProfessor> ofertaDisciplinasProfessor = new ArrayList<>();
		for(OfertaCurso ofertaCurso : ofertaCursos){
			Retorno<Disciplina> retDisciplina = buscarDisciplinas(plugin, ofertaCurso);
			if(!retDisciplina.isOk()){
				System.out.println(retDisciplina.getMensagem());
				return;
			}
			for(Disciplina disciplina : retDisciplina.getLista()){
				OfertaDisciplina ofertaDisciplina = new OfertaDisciplina(disciplina, ofertaCurso);
				ofertaDisciplinasProfessor.add(new OfertaDisciplinaProfessor(professor, ofertaDisciplina));
			}
		}
		List<OfertaDisciplinaAluno> ofertaDisciplinaAlunos = new ArrayList<>();
		for(OfertaDisciplinaProfessor ofertaDisciplinaProfessor : ofertaDisciplinasProfessor){
			Retorno<Aluno> retAluno = buscarAlunos(plugin, ofertaDisciplinaProfessor.getOfertaDisciplina());
			if(!retAluno.isOk()){
				System.out.println(retAluno.getMensagem());
				return;
			}
			for(Aluno aluno : retAluno.getLista()){
				ofertaDisciplinaAlunos.add(new OfertaDisciplinaAluno(aluno, ofertaDisciplinaProfessor.getOfertaDisciplina()));
			}
		}
		OfertaDisciplinaProfessor disciplinaProfessor = ofertaDisciplinasProfessor.get(ofertaDisciplinasProfessor.size()-1);
		System.out.println(disciplinaProfessor.getOfertaDisciplina().getDisciplina().getNome());
		List<OfertaDisciplinaAluno> disciplinaAluno = new ArrayList<>();
		for(OfertaDisciplinaAluno ofertaDisciplinaAluno : ofertaDisciplinaAlunos){
			if(ofertaDisciplinaAluno.getOfertaDisciplina().equals(disciplinaProfessor.getOfertaDisciplina())){
				disciplinaAluno.add(ofertaDisciplinaAluno);
				System.out.println(ofertaDisciplinaAluno.getAluno().getId() +" - "+ofertaDisciplinaAluno.getAluno().getNome());
			}
		}
		disciplinaProfessor.getOfertaDisciplina().setOfertaDisciplinaAlunos(disciplinaAluno);
		//		System.out.println(disciplinaAluno.size());
		//		Calendar calendar = Calendar.getInstance();
		//		calendar.set(2018, 10, 7);
		//		List<Frequencia> frequencias = new ArrayList<>();
		//		Frequencia frequencia1 = new Frequencia(calendar.getTime(), 1, disciplinaProfessor.getOfertaDisciplina());
		//		Frequencia frequencia2 = new Frequencia(calendar.getTime(), 2, disciplinaProfessor.getOfertaDisciplina());
		//		for(OfertaDisciplinaAluno alunoProg : disciplinaAluno){
		//			frequencia1.getFrequenciaAlunos().add(new FrequenciaAluno(false, alunoProg, frequencia1));
		//			frequencia2.getFrequenciaAlunos().add(new FrequenciaAluno(true, alunoProg, frequencia2));
		//		}
		//		frequencias.add(frequencia1);
		//		frequencias.add(frequencia2);
		Retorno<Frequencia> retFrequenciaRec = buscarFrequencias(plugin, disciplinaProfessor);
		//		Retorno<Integer> retQtdAulas = getQuantidadeDeAulas(plugin, calendar.getTime(), programacao4);
		//		System.out.println(retQtdAulas.getValor());
		//		salvarFrequencia(plugin, frequencias);
	}

	private static Retorno<Integer> getQuantidadeDeAulas(IPlugin plugin, Date diaFrequencia, OfertaDisciplinaProfessor ofertaDisciplinaProfessor){
		return null; //plugin.getquantidadeAulaLancada(diaFrequencia, ofertaDisciplinaProfessor);
	}

	private static Retorno<Frequencia> salvarFrequencia(IPlugin plugin, List<Frequencia> frequencias) {
		Retorno<Frequencia> ret = null; //plugin.salvarFrequencia(frequencias);
		System.out.println(ret.getMensagem());
		return ret;
	}

	private static Retorno<Frequencia> buscarFrequencias(IPlugin plugin, OfertaDisciplinaProfessor ofertaDisciplinaProfessor) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(2018, 10, 9);
		Retorno<Frequencia> ret = null; //plugin.getFrequencia(calendar.getTime(), ofertaDisciplinaProfessor);
		if(!ret.isOk()){
			System.out.println(ret.getMensagem());
			return ret;
		}
		System.out.println("Campus: " + ret.getLista().get(0).getOfertaDisciplina().getOfertaCurso().getCampus().getNome());
		System.out.println("Curso: " + ret.getLista().get(0).getOfertaDisciplina().getOfertaCurso().getNomeCursoMatriz());
		System.out.println("Disciplina: " + ret.getLista().get(0).getOfertaDisciplina().getDisciplina().getNome());
		for(Frequencia frequencia : ret.getLista()){
			System.out.println();
			System.out.println("Data da frequ�ncia: " + frequencia.getDataFrequencia());
			System.out.println("Aula n� " + frequencia.getAula());
			for(FrequenciaAluno frequenciaAluno : frequencia.getFrequenciaAlunos()){
				System.out.println(frequenciaAluno.getOfertaDisciplinaAluno().getAluno().getNome()
						+ " : " + (frequenciaAluno.getSituacao() ? "P" : "F"));
			}
		}
		return ret;
	}

	//	private static Retorno<Frequencia> buscarFrequencias(IPlugin plugin, OfertaDisciplinaProfessor ofertaDisciplinaProfessor) {
	//		Campus campus = new Campus();
	//		campus.setNome("An�polis CET");
	//		AnoSemestre anoSemestre = new AnoSemestre();
	//		anoSemestre.setDescricao("2018/2");
	//		Matriz matriz = new Matriz();
	//		matriz.setDescricao("2015/1");
	//		Curso curso = new Curso();
	//		curso.setNome("SISTEMAS DE INFORMA��O");
	//		OfertaCurso ofertaCurso = new OfertaCurso(curso, matriz, anoSemestre, campus);
	//		Disciplina disciplina = new Disciplina();
	//		disciplina.setId(1081745L);
	//		disciplina.setNome("PROGRAMA��O IV");
	//		OfertaDisciplina ofertaDisciplina = new OfertaDisciplina();
	//		ofertaDisciplina.setDisciplina(disciplina);
	//		ofertaDisciplina.setOfertaCurso(ofertaCurso);
	//		Aluno aluno1 = new Aluno();
	//		aluno1.setId(12016002455L);
	//		aluno1.setNome("GABRIEL SOARES SENA");
	//		Aluno aluno2 = new Aluno();
	//		aluno2.setId(12016003331L);
	//		aluno2.setNome("HIGOR JUNIO SILVA");
	//		Aluno aluno3 = new Aluno();
	//		aluno3.setId(12016002217L);
	//		aluno3.setNome("ROMULO SOUZA NASCIMENTO OLIVEIRA");
	//		Aluno aluno4 = new Aluno();
	//		aluno4.setId(12014002849L);
	//		aluno4.setNome("JEAN CARLO ABRENHOSA BORDORI");
	//		OfertaDisciplinaAluno ofertaDisciplinaAluno1 = new OfertaDisciplinaAluno(aluno1, ofertaDisciplina);
	//		OfertaDisciplinaAluno ofertaDisciplinaAluno2 = new OfertaDisciplinaAluno(aluno2, ofertaDisciplina);
	//		OfertaDisciplinaAluno ofertaDisciplinaAluno3 = new OfertaDisciplinaAluno(aluno3, ofertaDisciplina);
	//		OfertaDisciplinaAluno ofertaDisciplinaAluno4 = new OfertaDisciplinaAluno(aluno4, ofertaDisciplina);
	//		Calendar calendar = Calendar.getInstance();
	//		calendar.set(2018, 10, 7);
	//		Retorno<Frequencia> ret =
	//				plugin.buscarFrequencia(calendar.getTime(), ofertaDisciplinaAluno1, ofertaDisciplinaAluno2, ofertaDisciplinaAluno3, ofertaDisciplinaAluno4);
	//		if(!ret.isOk()){
	//			System.out.println(ret.getMensagem());
	//			return;
	//		}
	//		System.out.println(ret.getLista().size());
	//		for(Frequencia frequencia : ret.getLista()){
	//			System.out.println();
	//			System.out.println("Data da frequ�ncia: " + frequencia.getDataFrequencia());
	//			System.out.println("Aula n� " + frequencia.getAula());
	//			for(FrequenciaAluno frequenciaAluno : frequencia.getFrequenciaAlunos()){
	//				System.out.println(frequenciaAluno.getOfertaDisciplinaAluno().getAluno().getNome()
	//						+ " : " + (frequenciaAluno.getSituacao() ? "P" : "F"));
	//			}
	//		}
	//		return buscarFrequencias(plugin, ofertaDisciplinaProfessor);
	//	}

	private static Retorno<Aluno> buscarAlunos(IPlugin plugin, OfertaDisciplina ofertaDisciplina) {
		//		Campus campus = new Campus();
		//		campus.setNome("An�polis CET");
		//		AnoSemestre anoSemestre = new AnoSemestre();
		//		anoSemestre.setDescricao("2018/2");
		//		Matriz matriz = new Matriz();
		//		matriz.setDescricao("2015/1");
		//		Curso curso = new Curso();
		//		curso.setNome("SISTEMAS DE INFORMA��O");
		//		OfertaCurso ofertaCurso = new OfertaCurso(curso, matriz, anoSemestre, campus);
		//		Disciplina disciplina = new Disciplina();
		//		disciplina.setId(1081745L);
		//		disciplina.setNome("PROGRAMA��O IV");
		//		OfertaDisciplina ofertaDisciplina = new OfertaDisciplina();
		//		ofertaDisciplina.setDisciplina(disciplina);
		//		ofertaDisciplina.setOfertaCurso(ofertaCurso);
		//		Retorno<Aluno> ret = plugin.buscarAlunos(ofertaDisciplina);
		//		if(!ret.isOk()){ 
		//			System.out.println(ret.getMensagem());
		//			return;
		//		}
		//		System.out.println(ret.getLista().size());
		//		for(Aluno aluno : ret.getLista()){
		//			System.out.println(aluno.getNome());
		//		}
		return plugin.getAlunos(ofertaDisciplina);
	}

	private static Retorno<Disciplina> buscarDisciplinas(IPlugin plugin, OfertaCurso ofertaCurso) {
		//		Campus campus = new Campus();
		//		campus.setNome("An�polis CET");
		//		AnoSemestre anoSemestre = new AnoSemestre();
		//		anoSemestre.setDescricao("2018/2");
		//		Matriz matriz = new Matriz();
		//		matriz.setDescricao("2015/1");
		//		Curso curso = new Curso();
		//		curso.setNome("SISTEMAS DE INFORMA��O");
		//		OfertaCurso ofertaCurso = new OfertaCurso(curso, matriz, anoSemestre, campus);
		//		Retorno<Disciplina> ret = plugin.buscarDisciplinas(ofertaCurso);
		//		if(!ret.isOk()){ 
		//			System.out.println(ret.getMensagem());
		//			return;
		//		}
		//		for(Disciplina disciplina : ret.getLista()){
		//			System.out.println(disciplina.getNome());
		//		}
//		return plugin.getDisciplinas(ofertaCurso);
		return null;
	}

	private static Retorno<Matriz> buscarMatrizes(IPlugin plugin, AnoSemestre anoSemestre) {
		AnoSemestre anoSemestre1 = new AnoSemestre();
		anoSemestre1.setDescricao("2018/2");
		Retorno<Matriz> ret = plugin.getMatrizes(anoSemestre1);
		if(!ret.isOk()){
			System.out.println(ret.getMensagem());
			return ret;
		}
		for(Matriz matriz : ret.getLista()){
			System.out.println(matriz.getDescricao());
		}
		return ret;
//		return plugin.buscarMatrizes(anoSemestre);
	}

	private static Retorno<Curso> buscarCursos(IPlugin plugin, AnoSemestre anoSemestre) {
		AnoSemestre anoSemestre1 = new AnoSemestre();
		anoSemestre1.setDescricao("2018/2");
		Retorno<Curso> ret = plugin.getCursos(anoSemestre1);
		if(!ret.isOk()){
			System.out.println(ret.getMensagem());
			return ret;
		}
		for(Curso curso : ret.getLista()){
			System.out.println(curso.getNome());
		}
		return ret;
		//		return plugin.buscarCursos(anoSemestre);
	}

	private static Retorno<Professor> buscarProfessor(IPlugin plugin) {
		return plugin.getProfessor("98030175191", "AlunoSI20182");
	}

	private static Retorno<AnoSemestre> buscarAnosSemestres(IPlugin plugin) {
		//		Retorno<AnoSemestre> ret = plugin.buscarAnosSemestres();
		//		if(ret.isOk()){
		//			for(AnoSemestre anoSemestre : ret.getLista()){
		//				System.out.println(anoSemestre.getDescricao());
		//			}
		//		}
		//		return ret;

		return plugin.getAnosSemestres();
	}

	private static Retorno<Campus> buscarCampus(IPlugin plugin) {
		//		Retorno<Campus> ret = plugin.buscarCampus();
		//		if(!ret.isOk()){
		//			System.out.println(ret.getMensagem());
		//			return;
		//		}
		//		for(Campus campus : ret.getLista()){
		//			System.out.println(campus.getNome());
		//		}
		return plugin.getCampus();
	}

	private static void testarConcurrentModification() {
		ArrayList<Integer> arrayList = new ArrayList<>();
		arrayList.add(1);
		arrayList.add(1);
		arrayList.add(1);
		arrayList.add(1);
		arrayList.add(1);
		arrayList.add(1);
		ArrayList<Integer> copia = new ArrayList<>(arrayList);
		System.out.println(arrayList.size());
		Iterator<Integer> it = copia.iterator();
		while(it.hasNext()){
			arrayList.remove(it.next());
		}

		System.out.println(arrayList.size());
	}

	public static void testarPool(){
		//		System.out.println("\n"+PhantomJSDriverPool.getInstancia().obterDriver("maria", "123").getUsuario()); //Nova aloca��o
		//		System.out.println("\n"+PhantomJSDriverPool.getInstancia().obterDriver("joao", "123").getUsuario());	//Nova aloca��o
		//		System.out.println("\n"+PhantomJSDriverPool.getInstancia().obterDriver("cleber", "123").getUsuario()); //Nova aloca��o
		//		System.out.println("\n"+PhantomJSDriverPool.getInstancia().obterDriver("joao", "123").getUsuario()); //Tempo renovado
		//		System.out.println("\n"+PhantomJSDriverPool.getInstancia().obterDriver("maria", "123").getUsuario()); //Tempo renovado
		//		System.out.println("\n"+PhantomJSDriverPool.getInstancia().obterDriver("jose", "123").getUsuario()); //Nova aloca��o
		//		System.out.println("\n"+PhantomJSDriverPool.getInstancia().obterDriver("maria", "123").getUsuario()); //Tempo renovado
		//		System.out.println("\n"+PhantomJSDriverPool.getInstancia().obterDriver("cleber", "123").getUsuario()); //Tempo renovado
		//		System.out.println("\n"+PhantomJSDriverPool.getInstancia().obterDriver("ana", "123").getUsuario()); //Nova aloca��o
		//		System.out.println("\n"+PhantomJSDriverPool.getInstancia().obterDriver("jonas", "123").getUsuario()); //Desaloca��o do driver mais antigo
	}

}
