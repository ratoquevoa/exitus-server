package br.ueg.plugin.util;

import java.util.ArrayList;
import java.util.Calendar;

import br.ueg.exitus.modelo.Aluno;
import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.modelo.Campus;
import br.ueg.exitus.modelo.Curso;
import br.ueg.exitus.modelo.Disciplina;
import br.ueg.exitus.modelo.Frequencia;
import br.ueg.exitus.modelo.FrequenciaAluno;
import br.ueg.exitus.modelo.Matriz;
import br.ueg.exitus.modelo.OfertaCurso;
import br.ueg.exitus.modelo.OfertaDisciplina;
import br.ueg.exitus.modelo.OfertaDisciplinaAluno;
import br.ueg.exitus.plugin.IPlugin;
import br.ueg.exitus.plugin.PluginCapability;
import br.ueg.exitus.plugin.PluginFactory;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.utils.Retorno;

public class TesteApresentacao {

	public static void main(String[] args) {
		Campus campus = new Campus();
		campus.setNome("An�polis CET");
		AnoSemestre anoSemestre = new AnoSemestre();
		anoSemestre.setDescricao("2018/2");
		Curso curso = new Curso();
		curso.setNome("SISTEMAS DE INFORMA��O");
		Matriz matriz = new Matriz();
		matriz.setDescricao("2015/1");
		OfertaCurso ofertaCurso = new OfertaCurso(curso, matriz, anoSemestre, campus);
		PluginCapability capabilities = new PluginCapability();
		capabilities.setCapability(PluginCapability.USER, "98030175191");
		capabilities.setCapability(PluginCapability.PASSWORD, "AlunoSI20182");
		capabilities.setCapability(PluginCapability.PLUGIN_PATH, "br.ueg.plugin.FenixWebCrawlerPlugin");
		IPlugin plugin;
		try {
			plugin = PluginFactory.getInstancia().getPlugin(capabilities);
			ArrayList<OfertaCurso> ofertaCursos = new ArrayList<>();
			ofertaCursos.add(ofertaCurso);
			//Busca no sistema acad�mico as disciplinas
			Retorno<Disciplina> retDisciplina = plugin.getDisciplinas(ofertaCursos);
			OfertaDisciplina ofertaDisciplina = new OfertaDisciplina(retDisciplina.getLista().get(2), ofertaCurso);
			//Busca no sistema acad�mico os alunos
			Retorno<Aluno> retAluno = plugin.getAlunos(ofertaDisciplina);
			Calendar calendar = Calendar.getInstance();
			calendar.set(2018, 10, 19);
			Frequencia frequencia1 = new Frequencia(calendar.getTime(), 1, ofertaDisciplina);
			Frequencia frequencia2 = new Frequencia(calendar.getTime(), 2, ofertaDisciplina);
			for(Aluno aluno : retAluno.getLista()){
				OfertaDisciplinaAluno ofertaDisciplinaAluno = new OfertaDisciplinaAluno(aluno, ofertaDisciplina);
				frequencia1.getFrequenciaAlunos().add(new FrequenciaAluno(false, ofertaDisciplinaAluno, frequencia1));
				frequencia2.getFrequenciaAlunos().add(new FrequenciaAluno(true, ofertaDisciplinaAluno, frequencia2));
			}
			ArrayList<Frequencia> frequencias = new ArrayList<>();
			frequencias.add(frequencia1);
			frequencias.add(frequencia2);
			Retorno<Frequencia> retFreq = null; //plugin.salvarFrequencia(frequencias);
			System.out.println(retFreq.getMensagem());
			
			
		} catch (PluginInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
