package br.ueg.plugin.util;

import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import br.ueg.exitus.utils.DriverPool;

public class PhantomJSDriverPool extends DriverPool<PhantomJSDriver, PhantomJSDriverService> {
	private static PhantomJSDriverPool instancia;
	private DesiredCapabilities caps;
	
	private PhantomJSDriverPool() {
		super();
		caps = new DesiredCapabilities();
		caps.setJavascriptEnabled(true);
		caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
				"C:/Users/jonas/Downloads/phantomjs-2.1.1-windows/bin/phantomjs.exe");
		System.out.println("Capacidade m�xima do pool: " + numeroMax);
	}
	
	public static PhantomJSDriverPool getInstancia(){
		if(instancia == null){
			instancia = new PhantomJSDriverPool();
		}
		return instancia;
	}

	@Override
	public PhantomJSDriver criarDriver(PhantomJSDriverService driverService) {
		return new PhantomJSDriver(driverService, caps);
	}

	@Override
	protected PhantomJSDriverService criarDriverService() {
		return PhantomJSDriverService.createDefaultService(caps);
	}

}
