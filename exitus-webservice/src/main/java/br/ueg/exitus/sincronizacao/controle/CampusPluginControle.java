package br.ueg.exitus.sincronizacao.controle;

import br.ueg.exitus.modelo.Campus;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginFactory;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.utils.Retorno;

public class CampusPluginControle extends PluginControle{

	public CampusPluginControle(Professor p) throws PluginInitException {
		super(p);
	}

	public Retorno<Campus> buscarCampus(){
		Retorno<Campus> ret = plugin.getCampus();
		PluginFactory.getInstancia().returnPlugin(plugin);
		return ret;
	}
}
