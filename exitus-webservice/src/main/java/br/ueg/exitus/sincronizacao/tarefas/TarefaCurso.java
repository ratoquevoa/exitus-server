package br.ueg.exitus.sincronizacao.tarefas;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import br.ueg.exitus.controle.AnoSemestreControle;
import br.ueg.exitus.controle.Controle;
import br.ueg.exitus.controle.CursoControle;
import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.modelo.Curso;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.sincronizacao.controle.CursoPluginControle;
import br.ueg.exitus.sincronizacao.controle.PluginControle;
import br.ueg.exitus.sincronizador.utils.SyncResult;

@Entity
@DiscriminatorValue("curso")
public class TarefaCurso extends Tarefa<Curso> {
	
	public TarefaCurso(String params) {
		super(params);
	}
	
	public TarefaCurso() {
		super("{}");
	}

	@Override
	protected PluginControle getNewPluginControle(Professor p) throws PluginInitException {
		AnoSemestreControle anoSemestreControle = new AnoSemestreControle();
		AnoSemestre anoSemestre = anoSemestreControle.getModeloByChaveSecundaria(getParametro("ano_semestre")).getValor();
		return new CursoPluginControle(p, anoSemestre);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Controle getControleBaseLocal() {
		return new CursoControle();
	}

	@Override
	public boolean possuiControleDeAlteracaoNaBaseRemota() {
		return false;
	}

	@Override
	public SyncResult<Curso> getEntidadesBaseRemota(Date dataAtualizacao) {
		return getSyncResult(((CursoPluginControle) pluginControle).buscarCursos());
	}

}
