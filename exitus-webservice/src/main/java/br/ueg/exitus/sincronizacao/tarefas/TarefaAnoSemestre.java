package br.ueg.exitus.sincronizacao.tarefas;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import br.ueg.exitus.controle.AnoSemestreControle;
import br.ueg.exitus.controle.Controle;
import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.sincronizacao.controle.AnoSemestrePluginControle;
import br.ueg.exitus.sincronizacao.controle.PluginControle;
import br.ueg.exitus.sincronizador.utils.SyncResult;

@Entity
@DiscriminatorValue("anosemestre")
public class TarefaAnoSemestre extends Tarefa<AnoSemestre>{
	
	public TarefaAnoSemestre(String params) {
		super(params);
	}
	
	public TarefaAnoSemestre() {
		super("{}");
	}

	@Override
	protected PluginControle getNewPluginControle(Professor p) throws PluginInitException {
		return new AnoSemestrePluginControle(p);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Controle getControleBaseLocal() {
		return new AnoSemestreControle();
	}

	@Override
	public boolean possuiControleDeAlteracaoNaBaseRemota() {
		return false;
	}

	@Override
	public SyncResult<AnoSemestre> getEntidadesBaseRemota(Date dataAtualizacao) {
		return getSyncResult(((AnoSemestrePluginControle) pluginControle).getAnosSemestres());
	}
}
