package br.ueg.exitus.sincronizacao.controle;

import java.util.List;

import br.ueg.exitus.modelo.OfertaCurso;
import br.ueg.exitus.modelo.OfertaDisciplina;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginFactory;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.utils.Retorno;

public class OfertaDisciplinaPluginControle extends PluginControle {

	public OfertaDisciplinaPluginControle(Professor p) throws PluginInitException {
		super(p);
	}
	
	public Retorno<OfertaDisciplina> getOfertasDisciplina(List<OfertaCurso> ofertasCurso){
		Retorno<OfertaDisciplina> ret = plugin.getOfertasDisciplina(ofertasCurso);
		PluginFactory.getInstancia().returnPlugin(plugin);
		return ret;
	}

}
