package br.ueg.exitus.sincronizacao.tarefas;

import java.util.Date;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import br.ueg.exitus.controle.AnoSemestreControle;
import br.ueg.exitus.controle.Controle;
import br.ueg.exitus.controle.OfertaDisciplinaControle;
import br.ueg.exitus.controle.OfertaDisciplinaProfessorControle;
import br.ueg.exitus.controle.ProfessorControle;
import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.modelo.OfertaDisciplinaProfessor;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.sincronizacao.controle.OfertaDisciplinaProfessorPluginControle;
import br.ueg.exitus.sincronizacao.controle.PluginControle;
import br.ueg.exitus.sincronizador.utils.SyncResult;

@Entity
@DiscriminatorValue("ofertadisciplinaprofessor")
public class TarefaOfertaDisciplinaProfessor extends Tarefa<OfertaDisciplinaProfessor> {

	public TarefaOfertaDisciplinaProfessor(String params) {
		super(params);
	}
	
	public TarefaOfertaDisciplinaProfessor() {
		super("{}");
	}

	@Override
	public SyncResult<OfertaDisciplinaProfessor> salvarBaseLocal(List<OfertaDisciplinaProfessor> entidades) {
		for(OfertaDisciplinaProfessor entidade : entidades){
			SyncResult<OfertaDisciplinaProfessor> ret = salvarBaseLocal(entidade);
			if(!ret.isOk()) return ret;
		}
		return new SyncResult<>(true);
	}

	@Override
	public SyncResult<OfertaDisciplinaProfessor> salvarBaseLocal(OfertaDisciplinaProfessor entidade) {
		entidade.setProfessor(new ProfessorControle().getModeloByChaveSecundaria(getParametro("usuario")).getValor());
		return super.salvarBaseLocal(entidade);
	}
	
	

	@Override
	public void init(String params) {
		super.init(params);
		TarefaOfertaDisciplina tarefaOfertaDisciplina = new TarefaOfertaDisciplina(params);
		tarefaOfertaDisciplina.addTarefaDependente(this);
		
		addTarefaPai(tarefaOfertaDisciplina);
	}

	@Override
	protected PluginControle getNewPluginControle(Professor p) throws PluginInitException {
		return new OfertaDisciplinaProfessorPluginControle(p);
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Controle getControleBaseLocal() {
		return new OfertaDisciplinaProfessorControle();
	}

	@Override
	public boolean possuiControleDeAlteracaoNaBaseRemota() {
		return false;
	}

	@Override
	public SyncResult<OfertaDisciplinaProfessor> getEntidadesBaseRemota(Date dataAtualizacao) {
		OfertaDisciplinaControle ofertaDisciplinaControle = new OfertaDisciplinaControle();
		AnoSemestreControle anoSemestreControle = new AnoSemestreControle();
		AnoSemestre anoSemestre = anoSemestreControle.getModeloByChaveSecundaria(getParametro("ano_semestre")).getValor();
		return getSyncResult(((OfertaDisciplinaProfessorPluginControle)pluginControle)
				.getOfertasDisciplinaProfessor(ofertaDisciplinaControle.getByAnoSemestre(anoSemestre).getLista()));
	}

}
