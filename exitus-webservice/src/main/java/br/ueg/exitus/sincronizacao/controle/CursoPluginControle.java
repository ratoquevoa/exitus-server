package br.ueg.exitus.sincronizacao.controle;

import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.modelo.Curso;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginFactory;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.utils.Retorno;

public class CursoPluginControle extends PluginControle{
	
	private AnoSemestre anoSemestre;

	public CursoPluginControle (Professor p, AnoSemestre anoSemestre) throws PluginInitException {
		super(p);
		this.anoSemestre = anoSemestre;
	}

	public Retorno<Curso> buscarCursos(){
		Retorno<Curso> ret =  plugin.getCursos(anoSemestre);
		PluginFactory.getInstancia().returnPlugin(plugin);
		return ret;
	}

}
