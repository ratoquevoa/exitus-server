package br.ueg.exitus.sincronizacao.tarefas;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import br.ueg.exitus.controle.AlunoControle;
import br.ueg.exitus.controle.Controle;
import br.ueg.exitus.controle.OfertaDisciplinaControle;
import br.ueg.exitus.modelo.Aluno;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.sincronizacao.controle.AlunoPluginControle;
import br.ueg.exitus.sincronizacao.controle.PluginControle;
import br.ueg.exitus.sincronizador.utils.SyncResult;

@Entity
@DiscriminatorValue("aluno")
public class TarefaAluno extends Tarefa<Aluno> {

	public TarefaAluno(String params) {
		super(params);
	}
	
	public TarefaAluno() {
		super("{}");
	}

	@Override
	public boolean possuiControleDeAlteracaoNaBaseRemota() {
		return true;
	}

	@Override
	public SyncResult<Aluno> getEntidadesBaseRemota(Date dataAtualizacao) {
		OfertaDisciplinaControle oControle = new OfertaDisciplinaControle();
		AlunoPluginControle alunoPluginControle = getPluginControle();
		Long id = Long.valueOf(getParametro("oferta_disciplina"));
		return getSyncResult(alunoPluginControle.buscarAlunos(oControle.getModeloByID(id).getValor()));
	}

	@Override
	protected PluginControle getNewPluginControle(Professor p) throws PluginInitException {
		return new AlunoPluginControle(p);
	}

	@Override
	protected Controle getControleBaseLocal() {
		return new AlunoControle();
	}

}
