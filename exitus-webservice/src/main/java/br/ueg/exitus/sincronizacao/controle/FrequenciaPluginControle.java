package br.ueg.exitus.sincronizacao.controle;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.ueg.exitus.controle.OfertaDisciplinaAlunoControle;
import br.ueg.exitus.modelo.Frequencia;
import br.ueg.exitus.modelo.OfertaDisciplina;
import br.ueg.exitus.modelo.OfertaDisciplinaAluno;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginCapability;
import br.ueg.exitus.plugin.PluginFactory;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.utils.Retorno;

public class FrequenciaPluginControle extends PluginControle{

	public FrequenciaPluginControle(Professor p) throws PluginInitException {
		super(p);
	}
	
	public FrequenciaPluginControle(PluginCapability capabilities) throws PluginInitException {
		super(capabilities);
	}
	
	public Retorno<Frequencia> salvarFrequencia(List<Frequencia> frequencia, Integer quantidadeAulas){
		return plugin.salvarFrequencia(frequencia, quantidadeAulas);
	}
	
	public Retorno<Frequencia> getFrequencia(Date diaFrequencia, OfertaDisciplina ofertaDisciplina){
		Retorno<OfertaDisciplinaAluno> retOfertaDiscAluno = carregarOfertasDisciplinaAluno(ofertaDisciplina);
		if(!retOfertaDiscAluno.isOk()){
			return new Retorno<>(false, retOfertaDiscAluno.getMensagem(), retOfertaDiscAluno.getSeveridade());
		}
		ofertaDisciplina.setOfertaDisciplinaAlunos(retOfertaDiscAluno.getLista());
		return plugin.getFrequencia(diaFrequencia, ofertaDisciplina);
	}
	
	public Retorno<Integer> getQuantidadeAulas(Date diaFrequencia, OfertaDisciplina ofertaDisciplina){
		return plugin.getquantidadeAulaLancada(diaFrequencia, ofertaDisciplina);
	}
	
	private Retorno<OfertaDisciplinaAluno> carregarOfertasDisciplinaAluno(OfertaDisciplina ofertaDisciplina){
		OfertaDisciplinaAlunoControle ofertaDisciplinaAlunoControle = new OfertaDisciplinaAlunoControle();
		Map<String, Object> map = new HashMap<>();
		map.put("oferta_disciplina_id", ofertaDisciplina.getId());
		return ofertaDisciplinaAlunoControle.getModelosByFieldValues(map);
	}
	
	public void devolverPlugin(){
		PluginFactory.getInstancia().returnPlugin(plugin);
	}

}
