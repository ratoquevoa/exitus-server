package br.ueg.exitus.sincronizacao.controle;

import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.IPlugin;
import br.ueg.exitus.plugin.PluginCapability;
import br.ueg.exitus.plugin.PluginFactory;
import br.ueg.exitus.plugin.PluginInitException;

public abstract class PluginControle {

	protected IPlugin plugin;

	public PluginControle (Professor p) throws PluginInitException {
		PluginCapability capabilities = new PluginCapability();
		capabilities.setCapability(PluginCapability.USER, p.getUsuario());
		capabilities.setCapability(PluginCapability.PASSWORD, p.getSenha());
		capabilities.setCapability(PluginCapability.PLUGIN_PATH, "br.ueg.plugin.FenixWebCrawlerPlugin");
		plugin = PluginFactory.getInstancia().getPlugin(capabilities);
	}
	
	public PluginControle (PluginCapability capabilities) throws PluginInitException {
		plugin = PluginFactory.getInstancia().getPlugin(capabilities);
	}

}
