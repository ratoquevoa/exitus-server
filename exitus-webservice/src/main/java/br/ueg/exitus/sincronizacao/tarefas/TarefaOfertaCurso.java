package br.ueg.exitus.sincronizacao.tarefas;

import java.util.Date;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import br.ueg.exitus.controle.AnoSemestreControle;
import br.ueg.exitus.controle.CampusControle;
import br.ueg.exitus.controle.Controle;
import br.ueg.exitus.controle.CursoControle;
import br.ueg.exitus.controle.MatrizControle;
import br.ueg.exitus.controle.OfertaCursoControle;
import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.modelo.Campus;
import br.ueg.exitus.modelo.Curso;
import br.ueg.exitus.modelo.Matriz;
import br.ueg.exitus.modelo.OfertaCurso;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.sincronizacao.controle.OfertaCursoPluginControle;
import br.ueg.exitus.sincronizacao.controle.PluginControle;
import br.ueg.exitus.sincronizador.utils.SyncResult;

@Entity
@DiscriminatorValue("ofertacurso")
public class TarefaOfertaCurso extends Tarefa<OfertaCurso> {
	
	public TarefaOfertaCurso(String params) {
		super(params);
	}
	
	public TarefaOfertaCurso() {
		super("{}");
	}

	@Override
	public SyncResult<OfertaCurso> salvarBaseLocal(List<OfertaCurso> entidades) {
		for(OfertaCurso entidade : entidades){
			SyncResult<OfertaCurso> ret = salvarBaseLocal(entidade);
			if(!ret.isOk()) return ret;
		}
		return new SyncResult<>(true);
	}

	@Override
	public SyncResult<OfertaCurso> salvarBaseLocal(OfertaCurso entidade) {
		AnoSemestre anoSemestre = entidade.getAnoSemestre();
		Curso curso = entidade.getCurso();
		Matriz matriz = entidade.getMatriz();
		Campus campus = entidade.getCampus();
		
		AnoSemestreControle anoSemestreControle = new AnoSemestreControle();
		entidade.setAnoSemestre(anoSemestreControle.getModeloByChaveSecundaria(anoSemestre.getDescricao()).getValor());
		
		CampusControle campusControle = new CampusControle();
		entidade.setCampus(campusControle.getModeloByChaveSecundaria(campus.getNome()).getValor());
		
		MatrizControle matrizControle = new MatrizControle();
		entidade.setMatriz(matrizControle.getModeloByChaveSecundaria(matriz.getDescricao()).getValor());
		
		CursoControle cursoControle = new CursoControle();
		entidade.setCurso(cursoControle.getModeloByChaveSecundaria(curso.getNome()).getValor());
		return super.salvarBaseLocal(entidade);
	}
	
	

	@Override
	public void init(String params) {
		super.init(params);
		TarefaAnoSemestre tarefaAnoSemestre = new TarefaAnoSemestre(params);
		TarefaCampus tarefaCampus = new TarefaCampus(params);
		TarefaCurso tarefaCurso = new TarefaCurso(params);
		TarefaMatriz tarefaMatriz = new TarefaMatriz(params);
		
		tarefaAnoSemestre.addTarefaDependente(tarefaCurso);
		tarefaAnoSemestre.addTarefaDependente(tarefaMatriz);
		tarefaCurso.addTarefaPai(tarefaAnoSemestre);
		tarefaMatriz.addTarefaPai(tarefaAnoSemestre);
		
		tarefaCampus.addTarefaDependente(this);
		tarefaCurso.addTarefaDependente(this);
		tarefaMatriz.addTarefaDependente(this);

		addTarefaPai(tarefaCampus);
		addTarefaPai(tarefaCurso);
		addTarefaPai(tarefaMatriz);
	}

	@Override
	protected PluginControle getNewPluginControle(Professor p) throws PluginInitException {
		return new OfertaCursoPluginControle(p);
	}

	@Override
	protected Controle getControleBaseLocal() {
		return new OfertaCursoControle();
	}

	@Override
	public boolean possuiControleDeAlteracaoNaBaseRemota() {
		return false;
	}

	@Override
	public SyncResult<OfertaCurso> getEntidadesBaseRemota(Date dataAtualizacao) {
		AnoSemestreControle anoSemestreControle = new AnoSemestreControle();
		return getSyncResult(((OfertaCursoPluginControle)pluginControle).getOfertaCursos(
				anoSemestreControle.getModeloByChaveSecundaria(getParametro("ano_semestre")).getValor()));
	}

//	@Override
//	public void executar() {
//		List<ITarefaAgendada<?>> tarefasPai = getTarefasPai();
//		AnoSemestre anoSemestre = new AnoSemestreControle().getModeloByChaveSecundaria(getParametro("ano_semestre")).getValor();
//		ITarefaAgendada<Campus> tarefaCampus = (ITarefaAgendada<Campus>) tarefasPai.get(0);
//		ITarefaAgendada<Curso> tarefaCurso = (ITarefaAgendada<Curso>) tarefasPai.get(1);
//		ITarefaAgendada<Matriz> tarefaMatriz = (ITarefaAgendada<Matriz>) tarefasPai.get(2);
//		ArrayList<OfertaCurso> ofertasCurso = new ArrayList<>();
//		for(Curso curso : tarefaCurso.getResult()){
//			for(Matriz matriz : tarefaMatriz.getResult()){
//				for(Campus campus : tarefaCampus.getResult()){
//					ofertasCurso.add(new OfertaCurso(curso, matriz, anoSemestre, campus));
//				}
//			}
//		}
//		setResult(ofertasCurso);
//		for(OfertaCurso ofertaCurso : ofertasCurso){
//			new OfertaCursoControle().inserir(ofertaCurso);
//		}
//		setStatus(Status.FINALIZADA);
//	}

}
