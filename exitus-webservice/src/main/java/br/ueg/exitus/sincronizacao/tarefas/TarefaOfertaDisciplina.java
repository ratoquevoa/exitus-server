package br.ueg.exitus.sincronizacao.tarefas;

import java.util.Date;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import br.ueg.exitus.controle.AnoSemestreControle;
import br.ueg.exitus.controle.Controle;
import br.ueg.exitus.controle.DisciplinaControle;
import br.ueg.exitus.controle.OfertaCursoControle;
import br.ueg.exitus.controle.OfertaDisciplinaControle;
import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.modelo.Disciplina;
import br.ueg.exitus.modelo.OfertaDisciplina;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.sincronizacao.controle.OfertaDisciplinaPluginControle;
import br.ueg.exitus.sincronizacao.controle.PluginControle;
import br.ueg.exitus.sincronizador.utils.SyncResult;

@Entity
@DiscriminatorValue("ofertadisciplina")
public class TarefaOfertaDisciplina extends Tarefa<OfertaDisciplina> {

	public TarefaOfertaDisciplina(String params) {
		super(params);
	}
	
	public TarefaOfertaDisciplina() {
		super("{}");
	}

	@Override
	public SyncResult<OfertaDisciplina> salvarBaseLocal(List<OfertaDisciplina> entidades) {
		System.out.println("TAREFA OFERTA DISCIPLINA :" + entidades.size());
		for(OfertaDisciplina entidade : entidades){
			SyncResult<OfertaDisciplina> ret = salvarBaseLocal(entidade);
			if(!ret.isOk()) return ret;
		}
		return new SyncResult<>(true);
	}

	@Override
	public SyncResult<OfertaDisciplina> salvarBaseLocal(OfertaDisciplina entidade) {
		Disciplina disciplina = entidade.getDisciplina();
		
		DisciplinaControle disciplinaControle = new DisciplinaControle();
		entidade.setDisciplina(disciplinaControle.getModeloByID(disciplina.getId()).getValor());
		
		return super.salvarBaseLocal(entidade);
	}
	
	

	@Override
	public void init(String params) {
		super.init(params);
		TarefaDisciplina tarefaDisciplina = new TarefaDisciplina(params);
		tarefaDisciplina.addTarefaDependente(this);
		
		addTarefaPai(tarefaDisciplina);
	}

	@Override
	protected PluginControle getNewPluginControle(Professor p) throws PluginInitException {
		return new OfertaDisciplinaPluginControle(p);
	}

	@Override
	protected Controle getControleBaseLocal() {
		return new OfertaDisciplinaControle();
	}

	@Override
	public boolean possuiControleDeAlteracaoNaBaseRemota() {
		return false;
	}

	@Override
	public SyncResult<OfertaDisciplina> getEntidadesBaseRemota(Date dataAtualizacao) {
		OfertaCursoControle ofertaCursoControle = new OfertaCursoControle();
		AnoSemestreControle anoSemestreControle = new AnoSemestreControle();
		AnoSemestre anoSemestre = anoSemestreControle.getModeloByChaveSecundaria(getParametro("ano_semestre")).getValor();
		return getSyncResult(((OfertaDisciplinaPluginControle)pluginControle)
				.getOfertasDisciplina(ofertaCursoControle.getByAnoSemestre(anoSemestre).getLista()));
	}

}
