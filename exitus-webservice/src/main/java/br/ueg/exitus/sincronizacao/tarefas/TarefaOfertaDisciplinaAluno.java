package br.ueg.exitus.sincronizacao.tarefas;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import br.ueg.exitus.controle.Controle;
import br.ueg.exitus.controle.OfertaDisciplinaAlunoControle;
import br.ueg.exitus.controle.OfertaDisciplinaControle;
import br.ueg.exitus.modelo.OfertaDisciplinaAluno;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.sincronizacao.controle.OfertaDisciplinaAlunoPluginControle;
import br.ueg.exitus.sincronizacao.controle.PluginControle;
import br.ueg.exitus.sincronizador.utils.SyncResult;

@Entity
@DiscriminatorValue("ofertadisciplinaaluno")
public class TarefaOfertaDisciplinaAluno extends Tarefa<OfertaDisciplinaAluno> {

	@Override
	public void init(String params) {
		super.init(params);
		TarefaAluno tarefaAluno = new TarefaAluno(params);
		tarefaAluno.addTarefaDependente(this);
		
		addTarefaPai(tarefaAluno);
	}

	public TarefaOfertaDisciplinaAluno(String params) {
		super(params);
	}
	
	public TarefaOfertaDisciplinaAluno() {
		super("{}");
	}
	
	@Override
	public boolean possuiControleDeAlteracaoNaBaseRemota() {
		return false;
	}

	@Override
	public SyncResult<OfertaDisciplinaAluno> getEntidadesBaseRemota(Date dataAtualizacao) {
		OfertaDisciplinaControle oControle = new OfertaDisciplinaControle();
		OfertaDisciplinaAlunoPluginControle disciplinaAlunoPluginControle = getPluginControle();
		Long id = Long.valueOf(getParametro("oferta_disciplina"));
		return getSyncResult(disciplinaAlunoPluginControle.getOfertasDisciplinaAluno(oControle.getModeloByID(id).getValor()));
	}

	@Override
	protected PluginControle getNewPluginControle(Professor p) throws PluginInitException {
		return new OfertaDisciplinaAlunoPluginControle(p);
	}

	@Override
	protected Controle getControleBaseLocal() {
		return new OfertaDisciplinaAlunoControle();
	}

}
