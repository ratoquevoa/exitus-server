package br.ueg.exitus.sincronizacao;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import br.ueg.exitus.sincronizador.MonitoradorAbstract;
import br.ueg.exitus.sincronizador.tarefa.ITarefaAgendada;

public class Monitorador extends MonitoradorAbstract {
	private ScheduledFuture<?> scheduledFuture;
	private List<ITarefaAgendada<?>> listaExecucao;

	@Override
	public void iniciarMonitoramento(List<ITarefaAgendada<?>> listaExecucao) {
		this.listaExecucao = listaExecucao;
		schedule();
	}

	@Override
	public boolean isAlive() {
		return scheduledFuture != null && !scheduledFuture.isCancelled();
	}

	@Override
	protected void esperar() {
		scheduledFuture.cancel(true);
		System.out.println("Finalizando monitoramento " + new Date());
	}

	@Override
	public void notificar() {
		schedule();
	}

	private void schedule() {
		ScheduledExecutorService scheduler =
				Executors.newSingleThreadScheduledExecutor();
		scheduledFuture = scheduler.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				System.out.println("Tamanho da lista de execu��o: " + listaExecucao.size());
				System.out.println("Executando monitoramento " + new Date());
				executar(listaExecucao);
				System.out.println("Pausando monitoramento " + new Date());
			}
		}, 1, 7, TimeUnit.SECONDS);
	}
	
	@Override
	public boolean isWaiting() {
		return false;
	}


}
