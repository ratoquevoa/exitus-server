package br.ueg.exitus.sincronizacao.tarefas;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import br.ueg.exitus.controle.Controle;
import br.ueg.exitus.controle.FrequenciaAlunoControle;
import br.ueg.exitus.controle.FrequenciaControle;
import br.ueg.exitus.controle.OfertaDisciplinaProfessorControle;
import br.ueg.exitus.modelo.Frequencia;
import br.ueg.exitus.modelo.FrequenciaAluno;
import br.ueg.exitus.modelo.OfertaDisciplina;
import br.ueg.exitus.modelo.OfertaDisciplinaProfessor;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.sincronizacao.controle.FrequenciaPluginControle;
import br.ueg.exitus.sincronizacao.controle.PluginControle;
import br.ueg.exitus.sincronizador.tarefa.Status;
import br.ueg.exitus.sincronizador.utils.SyncResult;
import br.ueg.exitus.utils.ObjectUtils;
import br.ueg.exitus.utils.Retorno;

@Entity
@DiscriminatorValue("frequencia")
public class TarefaFrequencia extends Tarefa<Frequencia> {

	public TarefaFrequencia(String params) {
		super(params);
	}

	public TarefaFrequencia() {
		super("{}");
	}

	@Override
	public void executar() {
		if(!podeIniciar()){
			setStatus(Status.PENDENTE);
			return;
		}
		FrequenciaPluginControle frequenciaPluginControle = getPluginControle();
		setStatus(Status.EXECUTANDO);
		List<Frequencia> frequencias;
		FrequenciaControle frequenciaControle = new FrequenciaControle();
		Retorno<Frequencia> retBuscaFreqLocal = frequenciaControle.getModeloByChaveSecundaria(getParametro("data_frequencia"));
		if(!retBuscaFreqLocal.isOk() || ObjectUtils.isNullOrEmpty(retBuscaFreqLocal.getLista())){
			cancelar(frequenciaPluginControle);
			return;
		}
		frequencias = retBuscaFreqLocal.getLista();
		for(Frequencia frequencia : frequencias){
			FrequenciaAlunoControle frequenciaAlunoControle = new FrequenciaAlunoControle();
			Map<String, Object> map = new HashMap<>();
			map.put("frequencia_id", frequencia.getId());
			Retorno<FrequenciaAluno> retFreqAluno = frequenciaAlunoControle.getModelosByFieldValues(map);
			if(!retFreqAluno.isOk() || ObjectUtils.isNullOrEmpty(retFreqAluno.getLista())){
				cancelar(frequenciaPluginControle);
				return;
			}
			frequencia.setFrequenciaAlunos(retFreqAluno.getLista());
		}
		Frequencia frequencia = frequencias.get(0);
		Retorno<Integer> retQtd = frequenciaPluginControle.getQuantidadeAulas(frequencia.getDataFrequencia(), 
				frequencia.getOfertaDisciplina());
		if(!retQtd.isOk()){
			cancelar(frequenciaPluginControle);
			return;
		}
		if(retQtd.getValor() == 0 || frequencias.size() == retQtd.getValor()){
			//atualizarStatusFrequencia(frequencias, Status.PARCIAL);
			Retorno<Frequencia> retFreq = 
					inserirOuAtualizarFrequencia(frequenciaPluginControle, frequencias, Math.abs((frequencias.size() - retQtd.getValor())));
			if(!retFreq.isOk()){
				cancelar(frequenciaPluginControle);
				return;
			}
			Retorno<Frequencia> retAtualizarStatusFrequencia = atualizarStatusFrequencia(frequencias, Status.SINCRONIZADA);
			if(!retAtualizarStatusFrequencia.isOk()){
				cancelar(frequenciaPluginControle);
				return;
			}
		}else{
			Retorno<Frequencia> retFreqBuscaPlugin = frequenciaPluginControle.getFrequencia(frequencia.getDataFrequencia(), 
					frequencia.getOfertaDisciplina());
			if(!retFreqBuscaPlugin.isOk()){
				cancelar(frequenciaPluginControle);
				return;
			}
			//substituir a frequencia
			Retorno<Frequencia> retSubstFreq = frequenciaControle.substituirFrequencia(frequencias, retFreqBuscaPlugin.getLista());
			if(!retSubstFreq.isOk()){
				cancelar(frequenciaPluginControle);
				return;
			}
			
			Retorno<Frequencia> retAtualizarStatusFrequencia = atualizarStatusFrequencia(retSubstFreq.getLista(), Status.CONFLITO);
			if(!retAtualizarStatusFrequencia.isOk()){
				cancelar(frequenciaPluginControle);
				return;
			}
		}
		setStatus(Status.FINALIZADA);
		frequenciaPluginControle.devolverPlugin();
	}
	
	private Retorno<Frequencia> inserirOuAtualizarFrequencia(FrequenciaPluginControle frequenciaPluginControle, List<Frequencia> frequencias, Integer qtd){
		Frequencia frequencia = frequencias.get(0);
		Retorno<Frequencia> retFreq = frequenciaPluginControle.salvarFrequencia(frequencias, qtd);
		if(!retFreq.isOk()){
			cancelar(frequenciaPluginControle);
			return new Retorno<>(false);
		}
		OfertaDisciplina ofertaDisciplina = frequencia.getOfertaDisciplina();
		Retorno<Frequencia> retFreqBusca = frequenciaPluginControle.getFrequencia(frequencia.getDataFrequencia(), 
				ofertaDisciplina);
		for(int i = 0; i < frequencias.size(); i++){
			Frequencia frequenciaLocal = frequencias.get(i);
			Frequencia frequenciaRemota = retFreqBusca.getLista().get(i);
			if(frequenciaLocal.getAula() == frequenciaRemota.getAula()){
				for(int j = 0; j < frequenciaLocal.getFrequenciaAlunos().size(); j++){
					FrequenciaAluno frequenciaAlunoLocal = frequenciaLocal.getFrequenciaAlunos().get(j);
					FrequenciaAluno frequenciaAlunoRemota = frequenciaRemota.getFrequenciaAlunos().get(j);
					if(frequenciaAlunoLocal.getOfertaDisciplinaAluno().getAluno().getNome()
							.equals(frequenciaAlunoRemota.getOfertaDisciplinaAluno().getAluno().getNome())){
						if(!frequenciaAlunoLocal.getSituacao().equals(frequenciaAlunoRemota.getSituacao())){
							cancelar(frequenciaPluginControle);
							return new Retorno<>(false);
						}
					}
				}
			}
		}
		return new Retorno<>(true);
	}

	private void cancelar(FrequenciaPluginControle frequenciaPluginControle) {
		setStatus(Status.PENDENTE);
		frequenciaPluginControle.devolverPlugin();
	}

	private Retorno<Frequencia> atualizarStatusFrequencia(List<Frequencia> frequencias2, String status) {
		FrequenciaControle frequenciaControle = new FrequenciaControle();
		for(Frequencia frequencia : frequencias2){
			frequencia.setStatus(status);
			Retorno<Frequencia> ret = frequenciaControle.alterar(frequencia);
			if(!ret.isOk()) return ret;
		}
		return new Retorno<>(true);
	}

	@Override
	public boolean possuiControleDeAlteracaoNaBaseRemota() {
		return false;
	}

	@Override
	public SyncResult<Frequencia> getEntidadesBaseRemota(Date dataAtualizacao) {
		try {
			FrequenciaPluginControle frequenciaPluginControle = getPluginControle();
			Date diaFrequencia = new SimpleDateFormat("yyyy-MM-dd").parse(getParametro("diaFrequencia"));
			OfertaDisciplinaProfessorControle ofertaDisciplinaProfessorControle = new OfertaDisciplinaProfessorControle();
			Retorno<OfertaDisciplinaProfessor> ret = ofertaDisciplinaProfessorControle
					.getModeloByID(Long.parseLong(getParametro("oferta_disciplina_prof_id")));
			return getSyncResult(frequenciaPluginControle.getFrequencia(diaFrequencia, ret.getValor().getOfertaDisciplina()));
		} catch (ParseException e) {
			e.printStackTrace();
			return new SyncResult<>(false);
		}
	}

	@Override
	protected PluginControle getNewPluginControle(Professor p) throws PluginInitException {
		return new FrequenciaPluginControle(p);
	}

	@Override
	protected Controle getControleBaseLocal() {
		return new FrequenciaControle();
	}

}
