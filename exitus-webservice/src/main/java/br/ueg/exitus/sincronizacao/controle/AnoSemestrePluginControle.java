package br.ueg.exitus.sincronizacao.controle;

import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginFactory;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.utils.Retorno;

public class AnoSemestrePluginControle extends PluginControle{
	
	public AnoSemestrePluginControle(Professor p) throws PluginInitException {
		super(p);
	}

	public Retorno<AnoSemestre> getAnosSemestres(){
		Retorno<AnoSemestre> ret =  plugin.getAnosSemestres();
		PluginFactory.getInstancia().returnPlugin(plugin);
		return ret;
	}
	
}
