package br.ueg.exitus.sincronizacao.tarefas;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import br.ueg.exitus.controle.CampusControle;
import br.ueg.exitus.controle.Controle;
import br.ueg.exitus.modelo.Campus;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.sincronizacao.controle.CampusPluginControle;
import br.ueg.exitus.sincronizacao.controle.PluginControle;
import br.ueg.exitus.sincronizador.utils.SyncResult;

@Entity
@DiscriminatorValue("campus")
public class TarefaCampus extends Tarefa<Campus> {
	
	public TarefaCampus(String params) {
		super(params);
	}
	
	public TarefaCampus() {
		super("{}");
	}

	@Override
	protected PluginControle getNewPluginControle(Professor p) throws PluginInitException {
		return new CampusPluginControle(p);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Controle getControleBaseLocal() {
		return new CampusControle();
	}

	@Override
	public boolean possuiControleDeAlteracaoNaBaseRemota() {
		return false;
	}

	@Override
	public SyncResult<Campus> getEntidadesBaseRemota(Date dataAtualizacao) {
		return getSyncResult(((CampusPluginControle) pluginControle).buscarCampus());
	}

}
