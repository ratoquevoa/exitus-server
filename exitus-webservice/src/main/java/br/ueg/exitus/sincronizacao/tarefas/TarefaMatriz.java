package br.ueg.exitus.sincronizacao.tarefas;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import br.ueg.exitus.controle.AnoSemestreControle;
import br.ueg.exitus.controle.Controle;
import br.ueg.exitus.controle.MatrizControle;
import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.modelo.Matriz;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.sincronizacao.controle.MatrizPluginControle;
import br.ueg.exitus.sincronizacao.controle.PluginControle;
import br.ueg.exitus.sincronizador.utils.SyncResult;

@Entity
@DiscriminatorValue("matriz")
public class TarefaMatriz extends Tarefa<Matriz> {
	
	public TarefaMatriz(String params) {
		super(params);
	}
	
	public TarefaMatriz() {
		super("{}");
	}

	@Override
	protected PluginControle getNewPluginControle(Professor p) throws PluginInitException {
		AnoSemestreControle anoSemestreControle = new AnoSemestreControle();
		AnoSemestre anoSemestre = anoSemestreControle.getModeloByChaveSecundaria(getParametro("ano_semestre")).getValor();
		return new MatrizPluginControle(p, anoSemestre);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Controle getControleBaseLocal() {
		return new MatrizControle();
	}

	@Override
	public boolean possuiControleDeAlteracaoNaBaseRemota() {
		return false;
	}

	@Override
	public SyncResult<Matriz> getEntidadesBaseRemota(Date dataAtualizacao) {
		return getSyncResult(((MatrizPluginControle) pluginControle).buscarMatrizes());
	}

}
