package br.ueg.exitus.sincronizacao.tarefas;

import java.util.List;

import org.hibernate.FlushMode;
import org.hibernate.Session;

import br.ueg.exitus.sincronizador.tarefa.ITarefaAgendada;
import br.ueg.exitus.sincronizador.tarefa.ITarefaDAO;
import br.ueg.exitus.sincronizador.utils.SyncResult;
import br.ueg.exitus.utils.HibernateUtils;

public class TarefaDAO implements ITarefaDAO {
	private Session sessao;

	@Override
	public SyncResult getTarefasPendentes() {
		try{
			iniciarTransacao();
			List lista = sessao.createQuery("from tarefa_agendada where status in('P', 'E')").list();
			finalizarTransacao();
			return new SyncResult<>(lista, true);
		}catch(Exception e){
			cancelarTransacao();
			e.printStackTrace();
			return new SyncResult<>(false);
		}
	}

	@Override
	public SyncResult persistirTarefaAgendada(ITarefaAgendada tarefa) {
		try{
			iniciarTransacao();
			sessao.save(tarefa);
			finalizarTransacao();
			return new SyncResult<>(true);
		}catch(Exception e){
			cancelarTransacao();
			e.printStackTrace();
			return new SyncResult<>(false);
		}
	}
	
	@Override
	public SyncResult atualizarTarefaAgendada(ITarefaAgendada tarefa) {
		try{
			iniciarTransacao();
			sessao.update(tarefa);
			finalizarTransacao();
			return new SyncResult<>(true);
		}catch(Exception e){
			cancelarTransacao();
			e.printStackTrace();
			return new SyncResult<>(false);
		}
	}

	public void abrirSessao(){
		if(!sessaoAberta()){
			sessao = HibernateUtils.getInstancia().getSessao();
			sessao.setHibernateFlushMode(FlushMode.MANUAL);
		}
	}

	public void fecharSessao(){
		if(sessaoAberta()){
			sessao.close();
			sessao = null;
		}
	}

	public boolean sessaoAberta(){
		return sessao != null && sessao.isOpen();
	}

	public void iniciarTransacao(){
		abrirSessao();
		sessao.beginTransaction();
	}

	public void cancelarTransacao(){
		if(sessaoAberta()){
			sessao.getTransaction().rollback();
			fecharSessao();
		}
	}

	public void finalizarTransacao(){
		if(sessaoAberta()){
			sessao.flush();
			sessao.getTransaction().commit();
			fecharSessao();
		}
	}
}
