package br.ueg.exitus.sincronizacao.controle;

import br.ueg.exitus.modelo.OfertaDisciplina;
import br.ueg.exitus.modelo.OfertaDisciplinaAluno;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginFactory;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.utils.Retorno;

public class OfertaDisciplinaAlunoPluginControle extends PluginControle {

	public OfertaDisciplinaAlunoPluginControle(Professor p) throws PluginInitException {
		super(p);
	}
	
	public Retorno<OfertaDisciplinaAluno> getOfertasDisciplinaAluno(OfertaDisciplina ofertaDisciplina){
		Retorno<OfertaDisciplinaAluno> ret = plugin.getOfertasDisciplinaAluno(ofertaDisciplina);
		PluginFactory.getInstancia().returnPlugin(plugin);
		return ret;
	}

}
