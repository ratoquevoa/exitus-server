package br.ueg.exitus.sincronizacao;

import br.ueg.exitus.sincronizacao.tarefas.TarefaDAO;
import br.ueg.exitus.sincronizador.GestorDeTarefas;
import br.ueg.exitus.sincronizador.MonitoradorAbstract;
import br.ueg.exitus.sincronizador.tarefa.ITarefaDAO;

public class GestorDeTarefasServidor extends GestorDeTarefas{
	private static GestorDeTarefasServidor instancia;

	protected GestorDeTarefasServidor() {
		super();
	}
	
	public static GestorDeTarefasServidor getInstancia(){
		if(instancia == null){
			instancia = new GestorDeTarefasServidor();
		}
		return instancia;
	}

	@Override
	protected ITarefaDAO getTarefaDAO() {
		return new TarefaDAO();
	}

	@Override
	protected MonitoradorAbstract getMonitorador() {
		return new Monitorador();
	}

}
