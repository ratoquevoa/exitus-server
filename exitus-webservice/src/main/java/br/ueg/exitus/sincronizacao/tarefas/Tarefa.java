package br.ueg.exitus.sincronizacao.tarefas;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.DiscriminatorOptions;
import org.json.JSONArray;
import org.json.JSONObject;

import br.ueg.exitus.controle.Controle;
import br.ueg.exitus.controle.ProfessorControle;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.sincronizacao.controle.PluginControle;
import br.ueg.exitus.sincronizador.tarefa.ITarefaAgendada;
import br.ueg.exitus.sincronizador.tarefa.ITarefaDAO;
import br.ueg.exitus.sincronizador.tarefa.Sincronizavel;
import br.ueg.exitus.sincronizador.tarefa.TarefaAbstract;
import br.ueg.exitus.sincronizador.utils.SyncResult;
import br.ueg.exitus.utils.ObjectUtils;
import br.ueg.exitus.utils.Retorno;

@Entity(name="tarefa_agendada")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorOptions(insert=false)
@DiscriminatorColumn(name="tabela")
public abstract class Tarefa<E extends Sincronizavel> extends TarefaAbstract<E> implements Serializable {

	@Id
	@GeneratedValue
	@Column(nullable = false)
	private Integer id;

	@Column(nullable=false, length=3)
	private String status;


	@Column(nullable=false, length=45)
	private String tabela;

	@Column(nullable=false)
	private String parametros;

	@Column(nullable=true, name="data_finalizacao")
	private Date dataFinalizacao;

	@ManyToMany(cascade={CascadeType.ALL}, fetch = FetchType.EAGER)
	@JoinTable(name="tarefa_agendada_depende",
	joinColumns={@JoinColumn(name="tarefa_pai")},
	inverseJoinColumns={@JoinColumn(name="tarefa_filha")})
	private List<Tarefa<?>> tarefasPai;

	@ManyToMany(mappedBy="tarefasPai", fetch = FetchType.EAGER)
	private List<Tarefa<?>> tarefasDependentes;

	@Transient
	private List<E> result;

	@Override
	public boolean ehDependente(){
		return !ObjectUtils.isNullOrEmpty(tarefasPai);
	}

	@Transient
	protected PluginControle pluginControle;

	@SuppressWarnings("rawtypes")
	@Transient
	protected ITarefaDAO tarefaDAO;

	@Transient
	protected JSONObject parametrosJSON;

	public Tarefa(String params){
		super(params);
		setTabela(getClass().getSimpleName().toLowerCase().replace("tarefa", ""));
		if(parametros != null){
			parametrosJSON = new JSONObject(parametros);
		}
	}

	protected abstract PluginControle getNewPluginControle(Professor p) throws PluginInitException;
	
	protected <T extends PluginControle> T getPluginControle(){
		return (T) pluginControle;
	}


	protected abstract Controle getControleBaseLocal();
	
	@Override
	public boolean podeIniciar() {
		try {
			Professor  p = new ProfessorControle().buscarPorUsuario(getParametro("usuario")).getValor();
			pluginControle = getNewPluginControle(p);
			System.out.println("Plugin travado para a tarefa: " + getClass().getSimpleName() + new Date());
			return true;
		} catch (PluginInitException e) {
			e.printStackTrace();
			System.out.println("Tarefa: " + getClass().getSimpleName() + " n�o pode iniciar. Causa: " + e.getMessage());
			return false;
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void setTarefaDAO(ITarefaDAO tarefaDAO) {
		this.tarefaDAO = tarefaDAO;
	}

	@Override
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Override
	public String getIdentificador() {
		if(parametrosJSON == null || parametrosJSON.isEmpty()){
			parametrosJSON = new JSONObject(parametros);
		}
		StringBuilder identificador = new StringBuilder();
		JSONArray keys = parametrosJSON.names();
		identificador.append(tabela);
		identificador.append("_" + parametrosJSON.optString("usuario"));
		for(int i = 0 ; i < keys.length()  ; i++){
			String key = keys.getString(i);
			if(key.equals("data_atualizacao") || key.equals("usuario")) continue;
			identificador.append("_"+parametrosJSON.optString(keys.getString(i)));
		}
		return identificador.toString();
	}

	@Override
	public String getStatus() {
		return status;
	}

	@Override
	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String getTabela() {
		return tabela;
	}

	public void setTabela(String tabela) {
		this.tabela = tabela;
	}

	@Override
	public Date getDataFinalizacao() {
		return dataFinalizacao;
	}

	@Override
	public void setDataFinalizacao(Date dataFinalizacao) {
		this.dataFinalizacao = dataFinalizacao;
	}

	@Override
	public List<ITarefaAgendada<?>> getTarefasPai() {
		List<ITarefaAgendada<?>> list = new ArrayList<>();
		if(!ObjectUtils.isNullOrEmpty(tarefasPai)){
			list.addAll(tarefasPai);
		}
		return list;
	}

	public void setTarefaPai(List<Tarefa<?>> tarefasPai) {
		this.tarefasPai = tarefasPai;
	}

//	@Override
//	public List<ITarefaAgendada<?>> getTarefasDependentes() {
//		List<ITarefaAgendada<?>> list = new ArrayList<>();
//		if(!ObjectUtils.isNullOrEmpty(tarefasDependentes)){
//			list.addAll(tarefasDependentes);
//		}
//		return list;
//	}

	public void setTarefasDependentes(List<Tarefa<?>> tarefasFilhas) {
		this.tarefasDependentes = tarefasFilhas;
	}

	public void addTarefaPai(Tarefa<?> tarefa){
		if(tarefasPai == null){
			tarefasPai = new ArrayList<>();
		}
		tarefasPai.add(tarefa);
	}

	public void addTarefaDependente(Tarefa<?> tarefa){
		if(tarefasDependentes == null){
			tarefasDependentes = new ArrayList<>();
		}
		tarefasDependentes.add(tarefa);
	}

	public String getParametros() {
		return parametros;
	}

	@Override
	public String getParametro(String key){
		if(parametrosJSON == null || parametrosJSON.isEmpty()){
			parametrosJSON = new JSONObject(parametros);
		}
		return parametrosJSON.optString(key);
	}

	public void setParametros(String parametros) {
		parametrosJSON = new JSONObject(parametros);
		this.parametros = parametros;
	}

	public SyncResult<E> getSyncResult(Retorno<E> ret){
		return new SyncResult<>(ret.getLista(), ret.isOk());
	}

	@SuppressWarnings("unchecked")
	@Override
	public SyncResult<E> getEntidadesBaseLocal() {
		return getSyncResult(getControleBaseLocal().listar(null));
	}

	@SuppressWarnings("unchecked")
	@Override
	public SyncResult<E> atualizarBaseLocalComChaveSecundaria(List<E> entidades) {
		for(E entidade : entidades){
			Retorno<E> ret = getControleBaseLocal().alterarPelaChaveSecundaria(entidade);
			if(!ret.isOk()) return getSyncResult(ret);
		}
		return new SyncResult<>(true);
	}

	@SuppressWarnings("unchecked")
	@Override
	public SyncResult<E> salvarOuatualizarBaseLocal(E entidade) {
		return getSyncResult(getControleBaseLocal().inserirOuAtualizar(entidade));
	}

	@SuppressWarnings("unchecked")
	@Override
	public SyncResult<E> salvarBaseLocal(E entidade) {
		return getSyncResult(getControleBaseLocal().inserir(entidade));
	}

	@SuppressWarnings("unchecked")
	@Override
	public SyncResult<E> salvarBaseLocal(List<E> entidades) {
		for(E entidade : entidades){
			Retorno<E> ret = getControleBaseLocal().inserir(entidade);
			if(!ret.isOk()) return getSyncResult(ret);
		}
		return new SyncResult<>(true);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void atualizarTarefaNaBaseLocal() {
		tarefaDAO.atualizarTarefaAgendada(this);
	}

	@Override
	public void iniciarExecucao() {
		new Thread(() -> executar()).start();
	}

	@Override
	public List<E> getResult() {
		return result;
	}

	@Override
	public void setResult(List<E> result) {
		this.result = result;
	}


}
