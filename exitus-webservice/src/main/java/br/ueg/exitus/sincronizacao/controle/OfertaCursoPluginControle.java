package br.ueg.exitus.sincronizacao.controle;

import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.modelo.OfertaCurso;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginFactory;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.utils.Retorno;

public class OfertaCursoPluginControle extends PluginControle {
	

	public OfertaCursoPluginControle(Professor p) throws PluginInitException {
		super(p);
	}
	
	public Retorno<OfertaCurso> getOfertaCursos(AnoSemestre anoSemestre){
		Retorno<OfertaCurso> ret = plugin.getOfertasCurso(anoSemestre);
		PluginFactory.getInstancia().returnPlugin(plugin);
		return ret;
	}

}
