package br.ueg.exitus.sincronizacao.tarefas;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import br.ueg.exitus.controle.AnoSemestreControle;
import br.ueg.exitus.controle.Controle;
import br.ueg.exitus.controle.DisciplinaControle;
import br.ueg.exitus.controle.OfertaCursoControle;
import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.modelo.Disciplina;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.sincronizacao.controle.DisciplinaPluginControle;
import br.ueg.exitus.sincronizacao.controle.PluginControle;
import br.ueg.exitus.sincronizador.utils.SyncResult;

@Entity
@DiscriminatorValue("disciplina")
public class TarefaDisciplina extends Tarefa<Disciplina> {

	public TarefaDisciplina(String params) {
		super(params);
	}
	
	public TarefaDisciplina() {
		super("{}");
	}

	@Override
	public void init(String params) {
		super.init(params);
		TarefaOfertaCurso tarefaOfertaCurso = new TarefaOfertaCurso(params);
		tarefaOfertaCurso.addTarefaDependente(this);
		addTarefaPai(tarefaOfertaCurso);
	}
	
	@Override
	protected PluginControle getNewPluginControle(Professor p) throws PluginInitException {
		return new DisciplinaPluginControle(p);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Controle getControleBaseLocal() {
		return new DisciplinaControle();
	}

	@Override
	public boolean possuiControleDeAlteracaoNaBaseRemota() {
		return true;
	}

	@Override
	public SyncResult<Disciplina> getEntidadesBaseRemota(Date dataAtualizacao) {
		OfertaCursoControle ofertaCursoControle = new OfertaCursoControle();
		AnoSemestreControle anoSemestreControle = new AnoSemestreControle();
		AnoSemestre anoSemestre = anoSemestreControle.getModeloByChaveSecundaria(getParametro("ano_semestre")).getValor();
		return getSyncResult(((DisciplinaPluginControle) pluginControle).buscarDisciplinas(ofertaCursoControle.getByAnoSemestre(anoSemestre).getLista()));
	}

}
