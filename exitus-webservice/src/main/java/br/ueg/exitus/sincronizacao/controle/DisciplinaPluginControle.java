package br.ueg.exitus.sincronizacao.controle;

import java.util.List;

import br.ueg.exitus.modelo.Disciplina;
import br.ueg.exitus.modelo.OfertaCurso;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginFactory;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.utils.Retorno;

public class DisciplinaPluginControle extends PluginControle {
	

	public DisciplinaPluginControle(Professor p) throws PluginInitException {
		super(p);
	}
	
	public Retorno<Disciplina> buscarDisciplinas(List<OfertaCurso> ofertasCurso){
		Retorno<Disciplina> ret = plugin.getDisciplinas(ofertasCurso);
		PluginFactory.getInstancia().returnPlugin(plugin);
		return ret;
	}

}
