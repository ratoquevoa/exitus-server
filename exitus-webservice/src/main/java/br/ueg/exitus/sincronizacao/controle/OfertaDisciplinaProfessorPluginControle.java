package br.ueg.exitus.sincronizacao.controle;

import java.util.List;

import br.ueg.exitus.modelo.OfertaDisciplina;
import br.ueg.exitus.modelo.OfertaDisciplinaProfessor;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginFactory;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.utils.Retorno;

public class OfertaDisciplinaProfessorPluginControle extends PluginControle {

	public OfertaDisciplinaProfessorPluginControle(Professor p) throws PluginInitException {
		super(p);
	}
	
	public Retorno<OfertaDisciplinaProfessor> getOfertasDisciplinaProfessor(List<OfertaDisciplina> ofertasDisciplina){
		Retorno<OfertaDisciplinaProfessor> ret = plugin.getOfertasDisciplinaProfessor(ofertasDisciplina);
		PluginFactory.getInstancia().returnPlugin(plugin);
		return ret;
	}

}
