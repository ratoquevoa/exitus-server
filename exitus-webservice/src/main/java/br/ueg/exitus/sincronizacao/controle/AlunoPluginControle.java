package br.ueg.exitus.sincronizacao.controle;

import br.ueg.exitus.modelo.Aluno;
import br.ueg.exitus.modelo.OfertaDisciplina;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginFactory;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.utils.Retorno;

public class AlunoPluginControle extends PluginControle {

	public AlunoPluginControle(Professor p) throws PluginInitException {
		super(p);
	}
	
	public Retorno<Aluno> buscarAlunos(OfertaDisciplina ofertaDisciplina){
		Retorno<Aluno> ret = plugin.getAlunos(ofertaDisciplina);
		PluginFactory.getInstancia().returnPlugin(plugin);
		return ret;
	}

}
