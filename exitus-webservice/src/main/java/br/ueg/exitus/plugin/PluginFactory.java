package br.ueg.exitus.plugin;

public class PluginFactory {
	
	private static PluginFactory instancia;
	
	private PluginFactory(){}
	
	public static PluginFactory getInstancia(){
		if(instancia == null){
			instancia = new PluginFactory();
		}
		return instancia;
	}
	
	public IPlugin getPlugin(PluginCapability capabilities) throws PluginInitException{
		try {
			Class<?> classe = Class.forName((String)capabilities.getCapability(PluginCapability.PLUGIN_PATH));
			IPlugin plugin = (IPlugin) classe.newInstance();
			plugin.init(capabilities);
			return plugin;
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
			throw new PluginInitException(e.getCause());
		}
	}
	
	public void returnPlugin(IPlugin plugin){
		plugin.expire();
		plugin = null;
	}

}
