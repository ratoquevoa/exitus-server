package br.ueg.exitus.plugin;

import java.util.HashMap;
import java.util.Map;


public class PluginCapability {
	public static final String PLUGIN_PATH = "plugin_path";
	
	public static final String AUTHORIZATION_REQUIRED = "auth";
	
	public static final String USER = "user";
	
	public static final String PASSWORD = "senha";
	
	public static final String ALLOCATE_NEW_DRIVER = "new_driver";
	
	private final Map<String, Object> capabilities = new HashMap<>();
	
	public PluginCapability(){
		capabilities.put(AUTHORIZATION_REQUIRED, true);
	}
	
	public PluginCapability(Map<String, ?> map){
		capabilities.putAll(map);
	}
	
	public Object getCapability(String key){
		return capabilities.get(key);
	}
	
	public void setCapability(String key, Object value){
		capabilities.put(key, value);
	}
	
}
