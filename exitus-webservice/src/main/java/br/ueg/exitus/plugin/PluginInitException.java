package br.ueg.exitus.plugin;

import br.ueg.exitus.utils.Severidade;

public class PluginInitException extends Exception {
	private Severidade severidade;
	
	public PluginInitException(){}
	
	public PluginInitException(String message){
		super(message);
	}

	public PluginInitException(String message, Severidade severidade){
		super(message);
		this.severidade = severidade;
	}
	
	public PluginInitException(Throwable throwable){
		super(throwable);
	}
	
	public PluginInitException(String message, Throwable throwable){
		super(message, throwable);
	}

	public Severidade getSeveridade() {
		return severidade;
	}

	public void setSeveridade(Severidade severidade) {
		this.severidade = severidade;
	}
	
}
