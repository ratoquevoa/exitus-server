package br.ueg.exitus.plugin;

import java.util.Date;
import java.util.List;

import br.ueg.exitus.modelo.Aluno;
import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.modelo.Campus;
import br.ueg.exitus.modelo.Curso;
import br.ueg.exitus.modelo.Disciplina;
import br.ueg.exitus.modelo.Frequencia;
import br.ueg.exitus.modelo.Matriz;
import br.ueg.exitus.modelo.OfertaCurso;
import br.ueg.exitus.modelo.OfertaDisciplina;
import br.ueg.exitus.modelo.OfertaDisciplinaAluno;
import br.ueg.exitus.modelo.OfertaDisciplinaProfessor;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.utils.Retorno;

public interface IPlugin {
	
	void init(PluginCapability capabilities) throws PluginInitException;
	
	void expire();
	
	Retorno<Professor> getProfessor(String usuario, String senha);
	
	/**
	 * Retorna o identificador de usu�rio do sistema acad�mico
	 * 
	 * @return Identificador de usu�rio
	 */
	String getIdentificadorUsuario();
	
	Retorno<AnoSemestre> getAnosSemestres();
	
	Retorno<Campus> getCampus();
	
	Retorno<Curso> getCursos(AnoSemestre anoSemestre);
	
	Retorno<Matriz> getMatrizes(AnoSemestre anoSemestre);
	
	Retorno<OfertaCurso> getOfertasCurso(AnoSemestre semestre);
	
	Retorno<Disciplina> getDisciplinas(List<OfertaCurso> ofertaCurso);
	
	Retorno<OfertaDisciplina> getOfertasDisciplina(List<OfertaCurso> ofertaCurso);
	
	Retorno<OfertaDisciplinaProfessor> getOfertasDisciplinaProfessor(List<OfertaDisciplina> ofertasDisciplina);
	
	Retorno<Aluno> getAlunos(OfertaDisciplina ofertaDisciplina);
	
	Retorno<OfertaDisciplinaAluno> getOfertasDisciplinaAluno(OfertaDisciplina ofertaDisciplina);
	
	Retorno<Frequencia> getFrequencia(Date diaFrequencia, OfertaDisciplina ofertaDisciplina);
	
	Retorno<Frequencia> salvarFrequencia(List<Frequencia> frequencia, Integer quantidadeAulas);
	
	Retorno<Integer> getquantidadeAulaLancada(Date diaFrequencia, OfertaDisciplina ofertaDisciplina);
}
