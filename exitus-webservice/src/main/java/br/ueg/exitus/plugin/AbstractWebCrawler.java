package br.ueg.exitus.plugin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.service.DriverService;

import br.ueg.exitus.utils.DriverEmUsoException;
import br.ueg.exitus.utils.DriverPool;
import br.ueg.exitus.utils.ObjectUtils;
import br.ueg.exitus.utils.PooledDriver;
import br.ueg.exitus.utils.Retorno;
import br.ueg.exitus.utils.Severidade;

public abstract class AbstractWebCrawler<D extends WebDriver, S extends DriverService> implements IPlugin{
	private PooledDriver<D, S> pooledDriver;
	
	private void initWebDriver(String usuario, String senha, PluginCapability capabilities) throws PluginInitException, DriverEmUsoException{
		if(pooledDriver == null){
			Retorno<PooledDriver<D, S>> ret = null;
			if(capabilities.getCapability(PluginCapability.ALLOCATE_NEW_DRIVER) != null && 
					(boolean) capabilities.getCapability(PluginCapability.ALLOCATE_NEW_DRIVER)){
				ret = getDriverPool().getDriverWithoutPoolControl(usuario, senha, this);
			}else{
				ret = getPooledDriver(usuario, senha);
			}
			if(!ret.isOk()){
				throw new PluginInitException(ret.getMensagem(), ret.getSeveridade());
			}
			pooledDriver = ret.getValor();
		}
	}
	
	protected D getWebDriver(){
		return pooledDriver.getDriver();
	}
	
	protected PooledDriver<D, S> getPooledDriver(){
		return pooledDriver;
	}
	
	protected abstract DriverPool<D, S> getDriverPool();
	
	
	private Retorno<PooledDriver<D, S>> getPooledDriver(String usuario, String senha) throws DriverEmUsoException {
		return getDriverPool().getDriver(usuario, senha, this);
	}
	
	@Override
	public void init(PluginCapability capabilities) throws PluginInitException{
		if(!(boolean)capabilities.getCapability(PluginCapability.AUTHORIZATION_REQUIRED)){
			return;
		}
		String usuario, senha;
		usuario = (String) capabilities.getCapability(PluginCapability.USER);
		senha = (String) capabilities.getCapability(PluginCapability.PASSWORD);
		if(ObjectUtils.isNullOrEmpty(usuario) || ObjectUtils.isNullOrEmpty(senha)){
			throw new PluginInitException("Para iniciar o plugin tipo webcrawler � necess�rio um usu�rio e senha", Severidade.ERRO);
		}
		try {
			initWebDriver(usuario, senha, capabilities);
		} catch (DriverEmUsoException e) {
			throw new PluginInitException(e);
		}
	}
	
	@Override
	public void expire(){
		getDriverPool().liberarDriver(pooledDriver);
	}
	
	public abstract void sair();
	
	public abstract Retorno<Object> autenticar(D driver, String usuario, String senha);
	
}
