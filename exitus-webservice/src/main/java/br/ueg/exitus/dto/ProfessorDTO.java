package br.ueg.exitus.dto;

import br.ueg.exitus.modelo.Professor;

public class ProfessorDTO {
	private String nome; 
	private String usuario;
	private String token;
	
	public ProfessorDTO(){}
	
	public ProfessorDTO(Professor p, String token){
		nome = p.getNome();
		usuario = p.getUsuario();
		this.token = token;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
}
