package br.ueg.exitus.controle;

import br.ueg.exitus.dao.FrequenciaAlunoDAO;
import br.ueg.exitus.modelo.Frequencia;
import br.ueg.exitus.modelo.FrequenciaAluno;
import br.ueg.exitus.utils.Retorno;

public class FrequenciaAlunoControle extends Controle<FrequenciaAluno, FrequenciaAlunoDAO> {

	@Override
	protected Retorno<FrequenciaAluno> verificarIntegridadeRelacional(FrequenciaAluno modelo) {
		return new Retorno<>(true);
	}
	
	public Retorno<FrequenciaAluno> deletarFrequenciaAluno(Long... frequenciaIds){
		return dao.deletarFrequenciaAluno(frequenciaIds);
	}

//	@Override
//	public Retorno<FrequenciaAluno> inserirOuAtualizar(FrequenciaAluno modelo) {
//		FrequenciaControle frequenciaControle = new FrequenciaControle();
//		Retorno<Frequencia> retFreq = frequenciaControle.inserirOuAtualizar(modelo.getFrequencia());
//		if(!retFreq.isOk()) return new Retorno<>(false, retFreq.getMensagem(), retFreq.getSeveridade());
//		return super.inserirOuAtualizar(modelo);
//	}
	
	

}
