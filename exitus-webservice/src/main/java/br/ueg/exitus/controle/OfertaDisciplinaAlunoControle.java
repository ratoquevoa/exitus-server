package br.ueg.exitus.controle;

import br.ueg.exitus.dao.OfertaDisciplinaAlunoDAO;
import br.ueg.exitus.modelo.OfertaDisciplinaAluno;
import br.ueg.exitus.utils.Retorno;

public class OfertaDisciplinaAlunoControle extends Controle<OfertaDisciplinaAluno, OfertaDisciplinaAlunoDAO> {

	@Override
	protected Retorno<OfertaDisciplinaAluno> verificarIntegridadeRelacional(OfertaDisciplinaAluno modelo) {
		return new Retorno<>(true);
	}

}
