package br.ueg.exitus.controle;

import java.util.List;

import br.ueg.exitus.dao.OfertaDisciplinaDAO;
import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.modelo.OfertaCurso;
import br.ueg.exitus.modelo.OfertaDisciplina;
import br.ueg.exitus.utils.Retorno;
import br.ueg.exitus.utils.Severidade;

public class OfertaDisciplinaControle extends Controle<OfertaDisciplina, OfertaDisciplinaDAO> {

	@Override
	protected Retorno<OfertaDisciplina> verificarIntegridadeRelacional(OfertaDisciplina modelo) {
		return new Retorno<>(true);
	}
	
	public Retorno<OfertaDisciplina> getByAnoSemestre(AnoSemestre anoSemestre){
		return dao.getByAnoSemestre(anoSemestre);
	}

}
