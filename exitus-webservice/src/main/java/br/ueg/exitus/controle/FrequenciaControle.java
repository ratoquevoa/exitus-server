package br.ueg.exitus.controle;

import java.util.ArrayList;
import java.util.List;

import br.ueg.exitus.dao.FrequenciaDAO;
import br.ueg.exitus.modelo.Frequencia;
import br.ueg.exitus.modelo.FrequenciaAluno;
import br.ueg.exitus.sincronizador.tarefa.Status;
import br.ueg.exitus.utils.DefaultPostgresKeyServer;
import br.ueg.exitus.utils.Retorno;
import br.ueg.exitus.utils.Severidade;

public class FrequenciaControle extends Controle<Frequencia, FrequenciaDAO> {

	@Override
	protected Retorno<Frequencia> verificarIntegridadeRelacional(Frequencia modelo) {
		return new Retorno<>(true);
	}

	public Retorno<Frequencia> substituirFrequencia(List<Frequencia> substituir, List<Frequencia> nova){
		FrequenciaAlunoControle frequenciaAlunoControle = new FrequenciaAlunoControle();
		List<Long> frequenciaIds = new ArrayList<>();
		for(Frequencia frequencia : substituir){
			if(frequencia.getId() == null) continue;
			frequenciaIds.add(frequencia.getId());
		}
		if(!frequenciaIds.isEmpty()){
			Retorno<FrequenciaAluno> retDeleteFreqAluno = frequenciaAlunoControle.deletarFrequenciaAluno(frequenciaIds.toArray(new Long[0]));
			if(!retDeleteFreqAluno.isOk()){
				return new Retorno<>(false);
			}
			Retorno<Frequencia> retDeletarFrequencia = dao.deletarFrequencia(frequenciaIds.toArray(new Long[0]));
			if(!retDeletarFrequencia.isOk()){
				return new Retorno<>(false, retDeletarFrequencia.getMensagem(), retDeletarFrequencia.getSeveridade());
			}
		}
		for(Frequencia frequencia : nova){
			Retorno<Frequencia> retInserirFreq = inserir(frequencia);
			if(!retInserirFreq.isOk()){
				return retInserirFreq;
			}
		}
		return new Retorno<Frequencia>(true, "", Severidade.SUCESSO, null, nova);
	}

	@Override
	public Retorno<Frequencia> inserir(Frequencia modelo) {
		if(modelo.getId() == null) modelo.setId(DefaultPostgresKeyServer.getInstancia().getNextKey());
		modelo.setStatus(Status.SINCRONIZADA);
		Retorno<Frequencia> retFreq = super.inserir(modelo);
		if(!retFreq.isOk()) return retFreq;
		FrequenciaAlunoControle frequenciaAlunoControle = new FrequenciaAlunoControle();
		for(FrequenciaAluno frequenciaAluno : modelo.getFrequenciaAlunos()){
			if(frequenciaAluno.getFrequencia() == null){
				frequenciaAluno.setFrequencia(modelo);
			}
			if(frequenciaAluno.getId() == null) frequenciaAluno.setId(DefaultPostgresKeyServer.getInstancia().getNextKey());
			Retorno<FrequenciaAluno> retFreqAluno = frequenciaAlunoControle.inserir(frequenciaAluno);
			if(!retFreqAluno.isOk()) return new Retorno<>(false, retFreqAluno.getMensagem(), retFreqAluno.getSeveridade());
		}
		return new Retorno<>(true); 
	}


	@Override
	public Retorno<Frequencia> inserirOuAtualizar(Frequencia modelo) {
		Retorno<Frequencia> retFreq = super.inserirOuAtualizar(modelo);
		if(!retFreq.isOk()) return retFreq;
		FrequenciaAlunoControle frequenciaAlunoControle = new FrequenciaAlunoControle();
		for(FrequenciaAluno frequenciaAluno : modelo.getFrequenciaAlunos()){
			if(frequenciaAluno.getFrequencia() == null){
				frequenciaAluno.setFrequencia(modelo);
			}
			Retorno<FrequenciaAluno> retFreqAluno = frequenciaAlunoControle.inserirOuAtualizar(frequenciaAluno);
			if(!retFreqAluno.isOk()) return new Retorno<>(false, retFreqAluno.getMensagem(), retFreqAluno.getSeveridade());
		}
		return new Retorno<>(true);
	}

}
