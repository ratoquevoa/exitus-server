package br.ueg.exitus.controle;

import br.ueg.exitus.dao.CampusDAO;
import br.ueg.exitus.modelo.Campus;
import br.ueg.exitus.utils.Retorno;

public class CampusControle extends Controle<Campus, CampusDAO> {

	@Override
	protected Retorno<Campus> verificarIntegridadeRelacional(Campus modelo) {
		return new Retorno<>(true);
	}

}
