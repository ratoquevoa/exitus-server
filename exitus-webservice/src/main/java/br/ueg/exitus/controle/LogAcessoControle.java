package br.ueg.exitus.controle;

import br.ueg.exitus.dao.LogAcessoDAO;
import br.ueg.exitus.modelo.LogAcesso;
import br.ueg.exitus.utils.Retorno;

public class LogAcessoControle extends Controle<LogAcesso, LogAcessoDAO> {

	@Override
	protected Retorno<LogAcesso> verificarIntegridadeRelacional(LogAcesso modelo) {
		return new Retorno<>(true);
	}

}
