package br.ueg.exitus.controle;

import br.ueg.exitus.dao.OfertaCursoDAO;
import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.modelo.Campus;
import br.ueg.exitus.modelo.Curso;
import br.ueg.exitus.modelo.Matriz;
import br.ueg.exitus.modelo.OfertaCurso;
import br.ueg.exitus.utils.Retorno;

public class OfertaCursoControle extends Controle<OfertaCurso, OfertaCursoDAO> {

	@Override
	protected Retorno<OfertaCurso> verificarIntegridadeRelacional(OfertaCurso modelo) {
		return new Retorno<>(true);
	}
	
	public Retorno<OfertaCurso> getByAnoSemestre(AnoSemestre anoSemestre){
		return dao.getByAnoSemestre(anoSemestre);
	}
	
	public Retorno<OfertaCurso> getByAnoSemestreCampusCursoMatriz(OfertaCurso ofertaCurso){
		
		AnoSemestreControle anoSemestreControle = new AnoSemestreControle();
		AnoSemestre anoSemestre2 = anoSemestreControle.getModeloByChaveSecundaria(ofertaCurso.getAnoSemestre().getDescricao()).getValor();
		
		CampusControle campusControle = new CampusControle();
		Campus campus2 = campusControle.getModeloByChaveSecundaria(ofertaCurso.getCampus().getNome()).getValor();
		
		CursoControle cursoControle = new CursoControle();
		Curso curso2 = cursoControle.getModeloByChaveSecundaria(ofertaCurso.getCurso().getNome()).getValor();
		
		MatrizControle matrizControle = new MatrizControle();
		Matriz matriz2 = matrizControle.getModeloByChaveSecundaria(ofertaCurso.getMatriz().getDescricao()).getValor();
		
		return dao.getByAnoSemestreCampusCursoMatriz(anoSemestre2, campus2, curso2, matriz2);
	}

}
