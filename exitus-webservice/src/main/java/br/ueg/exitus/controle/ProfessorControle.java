package br.ueg.exitus.controle;

import br.ueg.exitus.dao.ProfessorDAO;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.utils.Retorno;


public class ProfessorControle extends Controle<Professor, ProfessorDAO> {

	@Override
	protected Retorno<Professor> verificarIntegridadeRelacional(Professor modelo) {
		return null;
	}

	@Override
	public Retorno<Professor> inserirOuAtualizar(Professor modelo) {
		Retorno<Professor> retAux = buscarPorUsuario(modelo.getUsuario());
		if(retAux.isOk()){
			if(retAux.getLista().isEmpty()){
				Retorno<Professor> retInserir = inserir(modelo);
				if(!retInserir.isOk()){
					return retInserir;
				}
				retAux.setValor(modelo);
			}else{
				Professor modeloPersistido = retAux.getLista().get(0);
				modeloPersistido.setNome(modelo.getNome());
				modeloPersistido.setSenha(modelo.getSenha());
				Retorno<Professor> retAlterar = alterar(modeloPersistido);
				if(!retAlterar.isOk()){
					return retAlterar;
				}
				retAux.setValor(modeloPersistido);
			}
		}
		return retAux;
	}
	
	public Retorno<Professor> buscarPorUsuario(String usuario){
		return dao.buscarPorUsuario(usuario);
	}
	

}
