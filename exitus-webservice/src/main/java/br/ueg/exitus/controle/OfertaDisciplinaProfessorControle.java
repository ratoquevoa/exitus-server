package br.ueg.exitus.controle;

import br.ueg.exitus.modelo.OfertaDisciplinaProfessor;
import br.ueg.exitus.utils.Retorno;

public class OfertaDisciplinaProfessorControle
		extends Controle<OfertaDisciplinaProfessor, OfertaDisciplinaProfessorDAO> {

	@Override
	protected Retorno<OfertaDisciplinaProfessor> verificarIntegridadeRelacional(OfertaDisciplinaProfessor modelo) {
		return new Retorno<>(true);
	}

}
