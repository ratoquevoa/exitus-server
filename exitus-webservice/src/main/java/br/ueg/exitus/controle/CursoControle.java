package br.ueg.exitus.controle;

import br.ueg.exitus.dao.CursoDAO;
import br.ueg.exitus.modelo.Curso;
import br.ueg.exitus.utils.Retorno;

public class CursoControle extends Controle<Curso, CursoDAO> {

	@Override
	protected Retorno<Curso> verificarIntegridadeRelacional(Curso modelo) {
		return new Retorno<>(true);
	}

}
