package br.ueg.exitus.controle;

import br.ueg.exitus.dao.DisciplinaDAO;
import br.ueg.exitus.modelo.Disciplina;
import br.ueg.exitus.utils.Retorno;

public class DisciplinaControle extends Controle<Disciplina, DisciplinaDAO> {

	@Override
	protected Retorno<Disciplina> verificarIntegridadeRelacional(Disciplina modelo) {
		return new Retorno<>(true);
	}

}
