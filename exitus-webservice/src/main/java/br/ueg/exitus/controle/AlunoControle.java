package br.ueg.exitus.controle;

import br.ueg.exitus.dao.AlunoDAO;
import br.ueg.exitus.modelo.Aluno;
import br.ueg.exitus.utils.Retorno;

public class AlunoControle extends Controle<Aluno, AlunoDAO> {

	@Override
	protected Retorno<Aluno> verificarIntegridadeRelacional(Aluno modelo) {
		return new Retorno<>(true);
	}

}
