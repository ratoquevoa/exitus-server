package br.ueg.exitus.controle;

import br.ueg.exitus.dao.MatrizDAO;
import br.ueg.exitus.modelo.Matriz;
import br.ueg.exitus.utils.Retorno;

public class MatrizControle extends Controle<Matriz, MatrizDAO> {

	@Override
	protected Retorno<Matriz> verificarIntegridadeRelacional(Matriz modelo) {
		return new Retorno<>(true);
	}

}
