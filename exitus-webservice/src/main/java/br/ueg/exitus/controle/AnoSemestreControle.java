package br.ueg.exitus.controle;

import br.ueg.exitus.dao.AnoSemestreDAO;
import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.utils.Retorno;

public class AnoSemestreControle extends Controle<AnoSemestre, AnoSemestreDAO> {

	@Override
	protected Retorno<AnoSemestre> verificarIntegridadeRelacional(AnoSemestre modelo) {
		return new Retorno<>(true);
	}

}
