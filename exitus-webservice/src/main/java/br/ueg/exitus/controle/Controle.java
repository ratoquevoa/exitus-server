package br.ueg.exitus.controle;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import br.ueg.exitus.dao.DAOGenerico;
import br.ueg.exitus.utils.ObjectUtils;
import br.ueg.exitus.utils.Retorno;

public abstract class Controle<M,D extends DAOGenerico<M>> {
	protected D dao;

	@SuppressWarnings("unchecked")
	public Controle(){
		try {
			dao = ((Class<D>) ((ParameterizedType) 
					(getClass().getGenericSuperclass())).getActualTypeArguments()[1]).newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	protected abstract Retorno<M> verificarIntegridadeRelacional(M modelo);

	public Retorno<M> validar(M modelo){
		Retorno<M> ret = new Retorno<M>(true);
		try{
			Class<?> superClasse = modelo.getClass();
			while(superClasse != null && !superClasse.getSimpleName().equals("Object")){
				Field[] atributos = superClasse.getDeclaredFields();
				for(Field atributo : atributos){
					Column column = atributo.getAnnotation(Column.class);
					boolean obrigatorio = false;
					if(column != null){
						obrigatorio = !column.nullable();
					}else{
						ManyToOne manyToOne = atributo.getAnnotation(ManyToOne.class);
						if(manyToOne != null){
							obrigatorio = !manyToOne.optional();
						}
					}
					Object valor = null;
					String nomeAtributo = null;
					if(obrigatorio && atributo.getAnnotation(Id.class) == null){
						nomeAtributo = atributo.getName().substring(0,1).toUpperCase() + atributo.getName().substring(1);
						String nomeMetodo = "get" + nomeAtributo;
						Method getAtributo = superClasse.getDeclaredMethod(nomeMetodo, new Class<?>[0]);
						valor = getAtributo.invoke(modelo, new Object[0]);
						if(ObjectUtils.isNullOrEmpty(valor)){
							ret.setOk(false);
							ret.setMensagem("Campo " + nomeAtributo + " vazio");
							return ret;
						}
					}
					if(valor instanceof String && column != null){
						if(((String)valor).length() > column.length()){
							ret.setOk(false);
							ret.setMensagem("O campo " + nomeAtributo + " dever� ter no m�ximo " + column.length() +" caracteres");
							return ret;
						}
					}
				}
				superClasse = superClasse.getSuperclass();
			}
			
		}catch(NoSuchMethodException | InvocationTargetException | IllegalAccessException e){
			e.printStackTrace();
			ret.setOk(false);
			ret.setMensagem(e.getLocalizedMessage());
		}
		return ret;
	}
	
	public Retorno<M> getModelosAntigos(Date dataAtualizacao){
		return dao.getModelosAntigos(dataAtualizacao);
	}
	
	public Retorno<M> getModelosByFieldValues(Map<String, Object> fieldValues){
		return dao.getModelosByFieldValues(fieldValues);
	}
	
	public Retorno<M> getModeloByID(Long id){
		return dao.getModeloByID(id);
	}
	
	public Retorno<M> getModeloByChaveSecundaria(String value){
		return dao.getByChaveSecundaria(value);
	}

	public D getDao() {
		return dao;
	}

	public void setDao(D dao) {
		this.dao = dao;
	}

	public Retorno<M> inserir(M modelo){
		Retorno<M> ret = validar(modelo);
		if(ret.isOk()){
			return dao.inserir(modelo);
		}
		return ret;
	}
	
	public Retorno<M> inserirOuAtualizar(M modelo){
		Retorno<M> ret = validar(modelo);
		if(ret.isOk()){
			return dao.inserirOuAtualizar(modelo);
		}
		return ret;
	}

	public Retorno<M> excluir(M modelo){
		Retorno<M> ret = verificarIntegridadeRelacional(modelo);
		if(ret.isOk()){
			return dao.excluir(modelo);
		}
		return dao.alterar(modelo);
	}

	public Retorno<M> alterar(M modelo){
		Retorno<M> ret = validar(modelo);
		if(ret.isOk()){
			return dao.alterar(modelo);
		}
		return ret;
	}
	
	public Retorno<M> alterarPelaChaveSecundaria(M modelo){
		Retorno<M> ret = validar(modelo);
		if(ret.isOk()){
			return dao.alterarPelaChaveSecundaria(modelo);
		}
		return ret;
	}

	public Retorno<M> listar(M modelo){
		return dao.listar(modelo);
	}
	
//	public void limparSessao(){
//		dao.limparSessao();
//	}
}
