package br.ueg.exitus.teste;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import br.ueg.exitus.controle.OfertaDisciplinaProfessorControle;
import br.ueg.exitus.sincronizacao.GestorDeTarefasServidor;
import br.ueg.exitus.sincronizacao.tarefas.TarefaOfertaDisciplinaProfessor;

public class Teste {

	public static void main(String[] args) {
		long inicio = System.currentTimeMillis();
		
		
//		OfertaDisciplinaControle ofertaDisciplinaControle = new OfertaDisciplinaControle();
//		AnoSemestreControle anoSemestreControle = new AnoSemestreControle();
//		AnoSemestre anoSemestre = anoSemestreControle.getModeloByChaveSecundaria("2018/2").getValor();
//		Professor p = new Professor();
//		p.setUsuario("98030175191");
//		p.setSenha("AlunoSI20182");
		
//		System.out.println(ofertaDisciplinaControle.getByAnoSemestre(anoSemestre).getLista());
//		Map<String, Object> map = new HashMap<>();
//		map.put("data_atualizacao", new Date());
//		map.put("oferta_disciplina_id", 1057);
//		Retorno<OfertaDisciplinaProfessor> ret = 
//				new OfertaDisciplinaProfessorControle().getModelosByFieldValues(map);
//		System.out.println(ret.getLista());
//		Aluno aluno1 = new Aluno();
//		aluno1.setId(1L);
//		aluno1.setNome("Jonas");
//		Aluno aluno2 = new Aluno();
//		aluno2.setId(1L);
//		aluno2.setNome("Jonas");
//		OfertaDisciplina ofertaDisciplina1 = new OfertaDisciplina();
//		ofertaDisciplina1.setId(1L);
//		OfertaDisciplina ofertaDisciplina2 = new OfertaDisciplina();
//		ofertaDisciplina2.setId(1L);
//		OfertaDisciplinaAluno ofertaDisciplinaAluno1 = new OfertaDisciplinaAluno();
//		ofertaDisciplinaAluno1.setAluno(aluno1);
//		ofertaDisciplinaAluno1.setOfertaDisciplina(ofertaDisciplina1);
//		OfertaDisciplinaAluno ofertaDisciplinaAluno2 = new OfertaDisciplinaAluno();
//		ofertaDisciplinaAluno2.setAluno(aluno2);
//		ofertaDisciplinaAluno2.setOfertaDisciplina(ofertaDisciplina2);
//		Campus campus1 = new Campus();
//		campus1.setNome("Campus");
//		Campus campus2 = new Campus();
//		campus2.setNome("Campuss");
//		AnoSemestre anoSemestre1 = new AnoSemestre();
//		anoSemestre1.setDescricao("2018/2");
//		AnoSemestre anoSemestre2 = new AnoSemestre();
//		anoSemestre2.setDescricao("2018/2");
//		Curso curso1 = new Curso();
//		curso1.setNome("Sistemas");
//		Curso curso2 = new Curso();
//		curso2.setNome("Sistemas");
//		Matriz matriz1 = new Matriz();
//		matriz1.setDescricao("2015-1");
//		Matriz matriz2 = new Matriz();
//		matriz2.setDescricao("2015-1");
//		OfertaCurso ofertaCurso1 = new OfertaCurso(curso1, matriz1, anoSemestre1, campus1);
//		OfertaCurso ofertaCurso2 = new OfertaCurso(curso2, matriz2, anoSemestre2, campus2);
//		System.out.println(ReflectionUtils.getInstancia().compararEntidades(ofertaCurso1, ofertaCurso2));
//		System.out.println(new TarefaAluno("{\"usuario\":98030175191,\"data_frequencia\":\"2018-11-17\"}").getIdentificador());
//		testarGestorTarefas();
//		PluginCapability capabilities = new PluginCapability();
//		capabilities.setCabapility(PluginCapability.USER, "98030175191");
//		capabilities.setCabapility(PluginCapability.PASSWORD, "AlunoSI20182");
//		capabilities.setCabapility(PluginCapability.PLUGIN_PATH, "br.ueg.plugin.FenixWebCrawlerPlugin");
//		capabilities.setCabapility(PluginCapability.ALLOCATE_NEW_DRIVER, true);
//		try {
//			IPlugin plugin = PluginFactory.getInstancia().getPlugin(capabilities);
//			PluginFactory.getInstancia().returnPlugin(plugin);
//		} catch (PluginInitException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		try {
//	        int timeoutMs = 5000;
//	        Socket sock = new Socket();
//	        SocketAddress sockaddr = new InetSocketAddress("8.8.8.8", 53);
//
//	        sock.connect(sockaddr, timeoutMs);
//	        sock.close();
//
//	        System.out.println(true);
//	    } catch (IOException e) { System.out.println(false); }
//		List lista = new ArrayList<>();
//		List<Object> objLista = new ArrayList<>();
//		List<Frequencia> freqLista = new ArrayList<>();
//		lista = freqLista;
//		objLista = lista;
//		AnoSemestre anoSemestre = new AnoSemestre();
//		System.out.println(anoSemestre instanceof Sincronizavel);
//		Professor professor = new Professor();
//		professor.setUsuario("98030175191");
//		professor.setSenha("AlunoSI20182");
//		professor.setNome("GUILIANO RANGEL ALVES");
//		ProfessorControle professorControle = new ProfessorControle();
//		professorControle.alterarPelaChaveSecundaria(professor);
		
		OfertaDisciplinaProfessorControle ofertaDisciplinaProfessorControle = new OfertaDisciplinaProfessorControle();
		Map<String, Object> map = new HashMap<>();
		map.put("ofertaDisciplina.ofertaCurso.anoSemestre.id", 1583);
		try {
			map.put("ofertaDisciplinaProfessor.dataAtualizacao", new SimpleDateFormat("yyyy-MM-dd").parse("2018-12-09"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ofertaDisciplinaProfessorControle.getModelosByFieldValues(map);
		
//		Map<String, Object> map = new HashMap<>();
//		try {
//			map.put("data_atualizacao", new SimpleDateFormat("yyyy-MM-dd").parse("2018-12-09"));
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		map.put("oferta_disciplina_id", 1645);
//		new OfertaDisciplinaAlunoControle().getModelosByFieldValues(map);
		
//		try {
//			System.out.println(DateUtils.comparar(new Date(), new SimpleDateFormat("yyyy-MM-dd").parse("2018-12-09")));
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		System.out.println(DefaultPostgresKeyServer.getInstancia().getNextKey());
		long fim = System.currentTimeMillis();
		System.out.println("\nTempo de execu��o: " + ((fim-inicio)/1000D) + " segundos");
	}

	private static void testarGestorTarefas() {
//		PluginCapability capabilities = new PluginCapability();
//		capabilities.setCabapility(PluginCapability.USER, "98030175191");
//		capabilities.setCabapility(PluginCapability.PASSWORD, "AlunoSI20182");
//		capabilities.setCabapility(PluginCapability.PLUGIN_PATH, "br.ueg.plugin.FenixWebCrawlerPlugin");
		
//		TarefaAnoSemestre tarefaAnoSemestre = new TarefaAnoSemestre(capabilities);
//		tarefaAnoSemestre.init("{\"usuario\":98030175191,\"data_atualizacao\":\"2018-11-17\"}");
//		
//		TarefaCampus tarefaCampus = new TarefaCampus(capabilities);
//		tarefaCampus.init("{\"usuario\":98030175191,\"data_atualizacao\":\"2018-11-17\"}");
//		
//		
//		TarefaCurso tarefaCurso = new TarefaCurso(capabilities);
//		tarefaCurso.init("{\"usuario\":98030175191,\"ano_semestre\":\"2018/2\",\"data_atualizacao\":\"2018-11-17\"}");
//		tarefaCurso.addTarefaPai(tarefaAnoSemestre);
//		
//		TarefaMatriz tarefaMatriz = new TarefaMatriz(capabilities);
//		tarefaMatriz.init("{\"usuario\":98030175191,\"ano_semestre\":\"2018/2\",\"data_atualizacao\":\"2018-11-17\"}");
//		tarefaMatriz.addTarefaPai(tarefaAnoSemestre);
//		
//		ArrayList<Tarefa<?>> tarefasDependentes = new ArrayList<>();
//		tarefasDependentes.add(tarefaCurso);
//		tarefasDependentes.add(tarefaMatriz);
//		
//		tarefaAnoSemestre.setTarefasDependentes(tarefasDependentes);
//		
////		GestorDeTarefasServidor.getInstancia();
//		GestorDeTarefasServidor.getInstancia().agendarTarefa(tarefaAnoSemestre);
//		GestorDeTarefasServidor.getInstancia().agendarTarefa(tarefaCampus);
//		GestorDeTarefasServidor.getInstancia().agendarTarefa(tarefaCurso);
//		GestorDeTarefasServidor.getInstancia().agendarTarefa(tarefaMatriz);
//		TarefaOfertaCurso tarefaOfertaCurso = new TarefaOfertaCurso();
//		tarefaOfertaCurso.init("{\"usuario\":98030175191,\"ano_semestre\":\"2018/2\",\"data_atualizacao\":\"2018-11-17\"}");
//		GestorDeTarefasServidor.getInstancia().agendarTarefa(tarefaOfertaCurso);
		TarefaOfertaDisciplinaProfessor tarefaOfertaDisciplinaProfessor = new TarefaOfertaDisciplinaProfessor("{\"usuario\":98030175191,\"ano_semestre\":\"2018/2\",\"data_atualizacao\":\"2018-11-17\"}");
		GestorDeTarefasServidor.getInstancia().agendarTarefa(tarefaOfertaDisciplinaProfessor);
	}

}
