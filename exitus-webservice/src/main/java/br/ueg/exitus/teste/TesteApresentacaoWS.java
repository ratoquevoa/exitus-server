package br.ueg.exitus.teste;

import br.ueg.exitus.sincronizacao.GestorDeTarefasServidor;
import br.ueg.exitus.sincronizacao.tarefas.TarefaOfertaDisciplinaProfessor;

public class TesteApresentacaoWS {

	public static void main(String[] args) {
		TarefaOfertaDisciplinaProfessor tarefaOfertaDisciplinaProfessor = new TarefaOfertaDisciplinaProfessor("{\"usuario\":98030175191,\"ano_semestre\":\"2018/2\",\"data_atualizacao\":\"2018-11-17\"}");
		GestorDeTarefasServidor.getInstancia().agendarTarefa(tarefaOfertaDisciplinaProfessor);
	}

}
