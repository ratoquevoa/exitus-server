package br.ueg.exitus.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import br.ueg.exitus.sincronizador.utils.ChaveSecundaria;
import br.ueg.exitus.sincronizador.utils.Comparable;

@Entity
public class Professor extends Entidade {
	
	@Id
	@GeneratedValue
	@Column(nullable = false)
	private Long id;
	
	@Comparable
	@ChaveSecundaria
	@Column(nullable = false, length = 100)
	private String usuario;
	
	@Comparable
	@Column(nullable = false, length = 100)
	private String nome;
	
	@Comparable
	@Column(nullable = false, length = 100)
	private String senha;
	
	public Professor(){}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
