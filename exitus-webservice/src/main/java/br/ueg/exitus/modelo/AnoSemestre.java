package br.ueg.exitus.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import br.ueg.exitus.sincronizador.utils.ChaveSecundaria;
import br.ueg.exitus.sincronizador.utils.Comparable;
import br.ueg.exitus.utils.OrderBy;

@Entity(name="ano_semestre")
public class AnoSemestre extends Entidade {
	
	public AnoSemestre() {}
	
	@Id
	@GeneratedValue
	@Column(nullable = false)
	private Long id;

	@Comparable
	@ChaveSecundaria
	@Column(nullable=false, length=10)
	@OrderBy
	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public Object getChaveSecundaria() {
		return descricao;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
