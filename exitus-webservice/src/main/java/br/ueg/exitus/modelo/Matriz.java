package br.ueg.exitus.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import br.ueg.exitus.sincronizador.utils.ChaveSecundaria;
import br.ueg.exitus.sincronizador.utils.Comparable;

@Entity
public class Matriz extends Entidade {
	
	@Id
	@GeneratedValue
	@Column(nullable = false)
	private Long id;

	
	@Comparable
	@ChaveSecundaria
	@Column(nullable=false, length=10)
	private String descricao;
	
	public Matriz(){}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
