package br.ueg.exitus.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.ueg.exitus.sincronizador.utils.ChaveSecundaria;

@Entity(name="log_acesso")
public class LogAcesso implements Serializable{
	
	@Id
	@GeneratedValue
	@Column(nullable = false)
	private Integer id;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "professor_id")
	private Professor professor;
	
	@ChaveSecundaria
	@Column(length = 255, nullable = false)
	private String token;
	
	private Boolean valido;
	
	@Column(nullable = false)
	private Date data;
	
	public LogAcesso(Professor professor, String token) {
		this.professor = professor;
		this.token = token;
		data = new Date();
		valido = true;
	}

	public LogAcesso(){
		data = new Date();
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public Boolean getValido() {
		return valido;
	}

	public void setValido(Boolean valido) {
		this.valido = valido;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
}
