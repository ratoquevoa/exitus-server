package br.ueg.exitus.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.ueg.exitus.sincronizador.utils.Comparable;

@Entity(name="oferta_disciplina_professor")
public class OfertaDisciplinaProfessor extends Entidade {
	
	@Id
	@GeneratedValue
	@Column(nullable = false)
	private Long id;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "professor_id")
	@Comparable
	private Professor professor;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "oferta_disciplina_id")
	@Comparable
	private OfertaDisciplina ofertaDisciplina;
	
	public OfertaDisciplinaProfessor(){}

	public OfertaDisciplinaProfessor(Professor professor, OfertaDisciplina ofertaDisciplina) {
		super();
		this.professor = professor;
		this.ofertaDisciplina = ofertaDisciplina;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public OfertaDisciplina getOfertaDisciplina() {
		return ofertaDisciplina;
	}

	public void setOfertaDisciplina(OfertaDisciplina ofertaDisciplina) {
		this.ofertaDisciplina = ofertaDisciplina;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
