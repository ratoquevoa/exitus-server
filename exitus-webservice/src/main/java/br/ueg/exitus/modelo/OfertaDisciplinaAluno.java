package br.ueg.exitus.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.ueg.exitus.sincronizador.utils.Comparable;

@Entity(name="oferta_disciplina_aluno")
public class OfertaDisciplinaAluno extends Entidade {
	
	@Id
	@GeneratedValue
	@Column(nullable = false)
	private Long id;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "aluno_id")
	@Comparable
	private Aluno aluno;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "oferta_disciplina_id")
	@Comparable
	private OfertaDisciplina ofertaDisciplina;
	
	public OfertaDisciplinaAluno(){}
	
	public OfertaDisciplinaAluno(Aluno aluno, OfertaDisciplina ofertaDisciplina) {
		super();
		this.aluno = aluno;
		this.ofertaDisciplina = ofertaDisciplina;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public OfertaDisciplina getOfertaDisciplina() {
		return ofertaDisciplina;
	}

	public void setOfertaDisciplina(OfertaDisciplina ofertaDisciplina) {
		this.ofertaDisciplina = ofertaDisciplina;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}
