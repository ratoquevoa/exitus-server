package br.ueg.exitus.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import br.ueg.exitus.sincronizador.utils.HasID;
import br.ueg.exitus.sincronizador.utils.Comparable;

@Entity
@HasID
public class Disciplina extends Entidade {
	
	public Disciplina(){}
	
	@Id
	@Column(nullable = false)
	private Long id;

	
	@Comparable
	@Column(nullable=false, length=100)
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
