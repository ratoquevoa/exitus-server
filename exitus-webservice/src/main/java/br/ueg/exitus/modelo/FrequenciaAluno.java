package br.ueg.exitus.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import br.ueg.exitus.sincronizador.utils.Comparable;

@Entity(name="frequencia_aluno")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FrequenciaAluno extends Entidade {
	
	@Id
	@Column(nullable = false)
	private Long id;
	
	@Column(nullable=false)
	@Comparable
	private Boolean situacao;
	
	@ManyToOne(optional=false)
	@JoinColumn(name="oferta_disciplina_aluno_id")
	private OfertaDisciplinaAluno ofertaDisciplinaAluno;
	
	@ManyToOne(optional=false)
	@JoinColumn(name="frequencia_id")
	@XmlTransient
	private Frequencia frequencia;
	
	public FrequenciaAluno(){}
	
	public FrequenciaAluno(Boolean situacao, OfertaDisciplinaAluno ofertaDisciplinaAluno, Frequencia frequencia) {
		super();
		this.situacao = situacao;
		this.ofertaDisciplinaAluno = ofertaDisciplinaAluno;
		this.frequencia = frequencia;
	}

	public Boolean getSituacao() {
		return situacao;
	}

	public void setSituacao(Boolean situacao) {
		this.situacao = situacao;
	}

	public OfertaDisciplinaAluno getOfertaDisciplinaAluno() {
		return ofertaDisciplinaAluno;
	}

	public void setOfertaDisciplinaAluno(OfertaDisciplinaAluno ofertaDisciplinaAluno) {
		this.ofertaDisciplinaAluno = ofertaDisciplinaAluno;
	}

	public Frequencia getFrequencia() {
		return frequencia;
	}

	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
