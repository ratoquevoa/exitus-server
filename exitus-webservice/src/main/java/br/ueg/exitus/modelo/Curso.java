package br.ueg.exitus.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import br.ueg.exitus.sincronizador.utils.ChaveSecundaria;
import br.ueg.exitus.sincronizador.utils.Comparable;

@Entity
public class Curso extends Entidade {
	
	public Curso(){}
	
	@Id
	@GeneratedValue
	@Column(nullable = false)
	private Long id;

	
	@Comparable
	@ChaveSecundaria
	@Column(nullable=false, length=45)
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
