package br.ueg.exitus.modelo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import br.ueg.exitus.sincronizador.utils.Comparable;

@Entity(name="oferta_disciplina")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class OfertaDisciplina extends Entidade{
	
	@Id
	@GeneratedValue
	@Column(nullable = false)
	private Long id;

	
	@ManyToOne(optional = false)
	@JoinColumn(name = "disciplina_id")
	@Comparable
	private Disciplina disciplina;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "oferta_curso_id")
	@Comparable
	private OfertaCurso ofertaCurso;
	
	@Column(name = "aulas_previstas")
	private Integer aulasPrevistas;
	
	@Column(name = "aulas_lancadas")
	private Integer aulasLancadas;
	
	@Transient
	@XmlTransient
	private List<OfertaDisciplinaAluno> ofertaDisciplinaAlunos;
	
	public OfertaDisciplina(){}
	
	public OfertaDisciplina(Disciplina disciplina, OfertaCurso ofertaCurso) {
		super();
		this.disciplina = disciplina;
		this.ofertaCurso = ofertaCurso;
	}

	public Disciplina getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}

	public OfertaCurso getOfertaCurso() {
		return ofertaCurso;
	}

	public void setOfertaCurso(OfertaCurso ofertaCurso) {
		this.ofertaCurso = ofertaCurso;
	}

	public List<OfertaDisciplinaAluno> getOfertaDisciplinaAlunos() {
		return ofertaDisciplinaAlunos;
	}

	public void setOfertaDisciplinaAlunos(List<OfertaDisciplinaAluno> ofertaDisciplinaAlunos) {
		this.ofertaDisciplinaAlunos = ofertaDisciplinaAlunos;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getAulasPrevistas() {
		return aulasPrevistas;
	}

	public void setAulasPrevistas(Integer aulasPrevistas) {
		this.aulasPrevistas = aulasPrevistas;
	}

	public Integer getAulasLancadas() {
		return aulasLancadas;
	}

	public void setAulasLancadas(Integer aulasLancadas) {
		this.aulasLancadas = aulasLancadas;
	}

}
