package br.ueg.exitus.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.ueg.exitus.sincronizador.utils.Comparable;

@Entity(name="oferta_curso")
public class OfertaCurso extends Entidade {
	
	public OfertaCurso(){}
	
	public OfertaCurso(Curso curso, Matriz matriz, AnoSemestre anoSemestre, Campus campus) {
		super();
		this.curso = curso;
		this.matriz = matriz;
		this.anoSemestre = anoSemestre;
		this.campus = campus;
	}
	
	@Id
	@GeneratedValue
	@Column(nullable = false)
	private Long id;

	
	@ManyToOne(optional = false)
	@JoinColumn(name = "curso_id")
	@Comparable
	private Curso curso;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "matriz_id")
	@Comparable
	private Matriz matriz;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "ano_semestre_id")
	@Comparable
	private AnoSemestre anoSemestre;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "campus_id")
	@Comparable
	private Campus campus;

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public Matriz getMatriz() {
		return matriz;
	}

	public void setMatriz(Matriz matriz) {
		this.matriz = matriz;
	}

	public AnoSemestre getAnoSemestre() {
		return anoSemestre;
	}

	public void setAnoSemestre(AnoSemestre anoSemestre) {
		this.anoSemestre = anoSemestre;
	}

	public Campus getCampus() {
		return campus;
	}

	public void setCampus(Campus campus) {
		this.campus = campus;
	}
	
	public String getNomeCursoMatriz(){
		return curso.getNome() + " - " + matriz.getDescricao();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
