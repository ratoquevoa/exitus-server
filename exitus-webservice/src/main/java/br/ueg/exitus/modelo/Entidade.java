package br.ueg.exitus.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.ueg.exitus.sincronizador.tarefa.Sincronizavel;
import br.ueg.exitus.sincronizador.utils.DateUtils;

@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Entidade implements Serializable, Sincronizavel{
	
	@Column(name="data_atualizacao", nullable=false)
	@Temporal(TemporalType.DATE)
	private Date dataAtualizacao;
	
	public Entidade(){
		dataAtualizacao = DateUtils.formatar(new Date());
	}

	@Override
	public Date getDataAtualizacao() {
		return dataAtualizacao;
	}

	@Override
	public void setDataAtualizacao(Date dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	@Override
	public Object getChaveSecundaria() {
		return null;
	}
	
	
}
