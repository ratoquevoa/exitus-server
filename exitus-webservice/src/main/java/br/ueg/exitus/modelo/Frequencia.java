package br.ueg.exitus.modelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import br.ueg.exitus.sincronizador.utils.ChaveSecundaria;
import br.ueg.exitus.sincronizador.utils.Comparable;

@Entity
public class Frequencia extends Entidade {
	
	@Id
	@Column(nullable = false)
	private Long id;

	@Comparable
	@ChaveSecundaria
	@Column(nullable=false, name="data_frequencia")
	@Temporal(TemporalType.DATE)
	private Date dataFrequencia;
	
	@Comparable
	@Column(nullable=false)
	private Integer aula;
	
	@Column(nullable=false, length=3)
	private String status;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "oferta_disciplina_id")
	private OfertaDisciplina ofertaDisciplina;
	
	@Transient
	private List<FrequenciaAluno> frequenciaAlunos;
	
	public Frequencia(){}
	
	public Frequencia(Date dataFrequencia, Integer aula, OfertaDisciplina ofertaDisciplina) {
		super();
		this.dataFrequencia = dataFrequencia;
		this.aula = aula;
		this.ofertaDisciplina = ofertaDisciplina;
		this.frequenciaAlunos = new ArrayList<>();
	}

	public Frequencia(Date dataFrequencia, Integer aula, OfertaDisciplina ofertaDisciplina,
			List<FrequenciaAluno> frequenciaAlunos) {
		super();
		this.dataFrequencia = dataFrequencia;
		this.aula = aula;
		this.ofertaDisciplina = ofertaDisciplina;
		this.frequenciaAlunos = frequenciaAlunos;
	}

	public Date getDataFrequencia() {
		return dataFrequencia;
	}

	public void setDataFrequencia(Date dataFrequencia) {
		this.dataFrequencia = dataFrequencia;
	}

	public Integer getAula() {
		return aula;
	}

	public void setAula(Integer aula) {
		this.aula = aula;
	}

	public OfertaDisciplina getOfertaDisciplina() {
		return ofertaDisciplina;
	}

	public void setOfertaDisciplina(OfertaDisciplina ofertaDisciplina) {
		this.ofertaDisciplina = ofertaDisciplina;
	}

	public List<FrequenciaAluno> getFrequenciaAlunos() {
		return frequenciaAlunos;
	}

	public void setFrequenciaAlunos(List<FrequenciaAluno> frequenciaAlunos) {
		this.frequenciaAlunos = frequenciaAlunos;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
