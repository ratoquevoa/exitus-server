package br.ueg.exitus.servico;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import br.ueg.exitus.controle.LogAcessoControle;
import br.ueg.exitus.controle.OfertaDisciplinaAlunoControle;
import br.ueg.exitus.modelo.LogAcesso;
import br.ueg.exitus.modelo.OfertaDisciplina;
import br.ueg.exitus.modelo.OfertaDisciplinaAluno;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.sincronizacao.GestorDeTarefasServidor;
import br.ueg.exitus.sincronizacao.tarefas.TarefaOfertaDisciplinaAluno;
import br.ueg.exitus.utils.ObjectUtils;
import br.ueg.exitus.utils.Retorno;

@WebService(targetNamespace = "http://servico.exitus.ueg.br/", portName = "OfertaDisciplinaAlunoServicePort", serviceName = "OfertaDisciplinaAlunoServiceService")
public class OfertaDisciplinaAlunoService {

	@WebMethod(operationName = "getOfertaDisciplinaAluno", action = "urn:GetOfertaDisciplinaAluno")
	@WebResult(name = "return")
	public Retorno<OfertaDisciplinaAluno> getOfertaDisciplinaAluno(@WebParam(name = "arg0") String token, @WebParam(name = "arg1") OfertaDisciplina ofertaDisciplina, @WebParam(name = "arg2") Date dataAtualizacao){
		Map<String, Object> map = new HashMap<>();
		map.put("data_atualizacao", dataAtualizacao);
		map.put("oferta_disciplina_id", ofertaDisciplina.getId());
		Retorno<OfertaDisciplinaAluno> ret = 
				new OfertaDisciplinaAlunoControle().getModelosByFieldValues(map);
		Professor p = getProfessor(token);
		if(GestorDeTarefasServidor.getInstancia().existeTarefaExecutando("ofertadisciplinaaluno_" + p.getUsuario()
				 + "_" + ofertaDisciplina.getId())){
			return new Retorno<>(false);
		}
		if(ObjectUtils.isNullOrEmpty(ret.getLista())){
			ret.setOk(false);
		}
		String data = new SimpleDateFormat("yyyy-MM-dd").format(dataAtualizacao);
		TarefaOfertaDisciplinaAluno tarefaOfertaDisciplinaAluno = 
				new TarefaOfertaDisciplinaAluno("{\"usuario\":" +p.getUsuario()+",\"oferta_disciplina\":\"" 
						+ ofertaDisciplina.getId() + "\",\"data_atualizacao\":\"" + data +"\"}");
		GestorDeTarefasServidor.getInstancia().agendarTarefa(tarefaOfertaDisciplinaAluno);
		return ret;
	}
	
	private Professor getProfessor(String token){
		LogAcessoControle logAcessoControle = new LogAcessoControle();
		Map<String, Object> map = new HashMap<>();
		map.put("token", token);
		Retorno<LogAcesso> retLogAcesso = logAcessoControle.getModelosByFieldValues(map);
		return retLogAcesso.getLista().get(0).getProfessor();
	}

}
