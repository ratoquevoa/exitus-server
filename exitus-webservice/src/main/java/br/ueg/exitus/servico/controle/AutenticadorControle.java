package br.ueg.exitus.servico.controle;

import java.util.Date;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import br.ueg.exitus.controle.LogAcessoControle;
import br.ueg.exitus.controle.ProfessorControle;
import br.ueg.exitus.modelo.LogAcesso;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.IPlugin;
import br.ueg.exitus.plugin.PluginCapability;
import br.ueg.exitus.plugin.PluginFactory;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.utils.ObjectUtils;
import br.ueg.exitus.utils.Retorno;
import br.ueg.exitus.utils.Severidade;

public class AutenticadorControle {

	public AutenticadorControle(){

	}

	public Retorno<LogAcesso> autenticar(String usuario, String senha) throws PluginInitException{
		PluginCapability capabilities = new PluginCapability();
		capabilities.setCapability(PluginCapability.USER, usuario);
		capabilities.setCapability(PluginCapability.PASSWORD, senha);
		capabilities.setCapability(PluginCapability.PLUGIN_PATH, "br.ueg.plugin.FenixWebCrawlerPlugin");
		IPlugin plugin = PluginFactory.getInstancia().getPlugin(capabilities);
		Retorno<Professor> retProf = plugin.getProfessor(usuario, senha);
		PluginFactory.getInstancia().returnPlugin(plugin);
		Retorno<LogAcesso> retLog = new Retorno<LogAcesso>();
		retLog.copiar(retProf);
		String token;
		if(retProf.isOk()){
			ProfessorControle professorControle = new ProfessorControle();
			retProf = professorControle.inserirOuAtualizar(retProf.getValor());
			Professor p = retProf.getValor();
			String concat = usuario + new Date().toString();
			byte[] bytes = DigestUtils.sha256(concat.getBytes());
			token = Hex.encodeHexString(bytes);
			LogAcessoControle acessoControle = new LogAcessoControle();
			LogAcesso log = new LogAcesso(p, token);
			acessoControle.inserir(log);
			retLog.setValor(log);
		}
		return retLog;
	}

	public String getIdentificadorUsuario() throws PluginInitException{
		PluginCapability capabilities = new PluginCapability();
		capabilities.setCapability(PluginCapability.AUTHORIZATION_REQUIRED, false);
		capabilities.setCapability(PluginCapability.PLUGIN_PATH, "br.ueg.plugin.FenixWebCrawlerPlugin");
		IPlugin plugin = PluginFactory.getInstancia().getPlugin(capabilities);
		String ret = plugin.getIdentificadorUsuario();
		return ret;
	}

	public Retorno<Object> validarToken(String token){
		LogAcessoControle logAcessoControle = new LogAcessoControle();
		LogAcesso logAcesso = new LogAcesso(null, token);
		logAcesso.setData(null);
		logAcesso.setValido(null);
		Retorno<LogAcesso> ret = logAcessoControle.listar(logAcesso);
		if(ret.isOk() && !ObjectUtils.isNullOrEmpty(ret.getLista())){
			if(ret.getLista().get(0).getValido()){
				Professor p = ret.getLista().get(0).getProfessor();
				PluginCapability capabilities = new PluginCapability();
				capabilities.setCapability(PluginCapability.USER, p.getUsuario());
				capabilities.setCapability(PluginCapability.PASSWORD, p.getSenha());
				capabilities.setCapability(PluginCapability.ALLOCATE_NEW_DRIVER, true);
				capabilities.setCapability(PluginCapability.PLUGIN_PATH, "br.ueg.plugin.FenixWebCrawlerPlugin");
				IPlugin plugin = null;
				try {
					plugin = PluginFactory.getInstancia().getPlugin(capabilities);
					PluginFactory.getInstancia().returnPlugin(plugin);
					return new Retorno<>(true);
				} catch (PluginInitException e) {
					e.printStackTrace();
					if(plugin != null) PluginFactory.getInstancia().returnPlugin(plugin);
					Retorno<Object> retorno = new Retorno<>();
					retorno.setMensagem("Falha ao autenticar. Por favor realize login novamente");
					retorno.setOk(false);
					retorno.setSeveridade(Severidade.ERRO);
					return retorno;
				}
				
			}
		}
		Retorno<Object> retObj = new Retorno<>();
		retObj.copiar(ret);
		return retObj;

	}

}
