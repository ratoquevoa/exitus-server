package br.ueg.exitus.servico;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import br.ueg.exitus.controle.AnoSemestreControle;
import br.ueg.exitus.controle.LogAcessoControle;
import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.sincronizacao.tarefas.TarefaAnoSemestre;
import br.ueg.exitus.utils.Retorno;
import br.ueg.exitus.utils.Severidade;

@WebService(targetNamespace = "http://servico.exitus.ueg.br/", portName = "AnoSemestreServicePort", serviceName = "AnoSemestreServiceService")
public class AnoSemestreService {
	
	@WebMethod(operationName = "getAnosSemestres", action = "urn:GetAnosSemestres")
	@WebResult(name = "return")
	public Retorno<AnoSemestre> getAnosSemestres( @WebParam(name = "arg0") String token){
		try {
			TarefaAnoSemestre tarefaAnoSemestre = new TarefaAnoSemestre("{usuario:\"" + new LogAcessoControle()
					.getModeloByChaveSecundaria(token).getValor().getProfessor().getUsuario() + "\", data_atualizacao: \""
					+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "\"}");
			tarefaAnoSemestre.executar();
			return new AnoSemestreControle().listar(null);
		} catch (Exception e) {
			e.printStackTrace();
			return new Retorno<>(false, e.getMessage(), Severidade.ERRO);
		}
	}

}
