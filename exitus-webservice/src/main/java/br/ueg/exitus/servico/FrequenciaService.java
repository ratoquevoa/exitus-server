package br.ueg.exitus.servico;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import br.ueg.exitus.controle.FrequenciaAlunoControle;
import br.ueg.exitus.controle.FrequenciaControle;
import br.ueg.exitus.controle.LogAcessoControle;
import br.ueg.exitus.controle.OfertaDisciplinaControle;
import br.ueg.exitus.modelo.Frequencia;
import br.ueg.exitus.modelo.FrequenciaAluno;
import br.ueg.exitus.modelo.LogAcesso;
import br.ueg.exitus.modelo.OfertaDisciplina;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.plugin.PluginCapability;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.sincronizacao.GestorDeTarefasServidor;
import br.ueg.exitus.sincronizacao.controle.FrequenciaPluginControle;
import br.ueg.exitus.sincronizacao.tarefas.TarefaFrequencia;
import br.ueg.exitus.sincronizador.tarefa.Status;
import br.ueg.exitus.utils.ObjectUtils;
import br.ueg.exitus.utils.Retorno;
import br.ueg.exitus.utils.Severidade;

@WebService(targetNamespace = "http://servico.exitus.ueg.br/", portName = "FrequenciaServicePort", serviceName = "FrequenciaServiceService")
public class FrequenciaService {

	@WebMethod(operationName = "agendarSincronizacaoFrequencia", action = "urn:AgendarSincronizacaoFrequencia")
	@WebResult(name = "return")
	public Retorno<Frequencia> agendarSincronizacaoFrequencia(@WebParam(name = "arg0") String token, @WebParam(name = "arg1") List<Frequencia> frequencias){
		try{
			agendar(token, frequencias);
			return new Retorno<>(true);
		}catch(Exception e){
			e.printStackTrace();
			return new Retorno<>(false);
		}
	}


	@WebMethod(operationName = "verificarStatusSincronizacao", action = "urn:VerificarStatusSincronizacao")
	@WebResult(name = "return")
	public Retorno<Frequencia> verificarStatusSincronizacao(@WebParam(name = "arg0") String token, @WebParam(name = "arg1") Date diaFrequencia){
		Professor p = getProfessor(token);
		String dataFormatada = new SimpleDateFormat("yyyy-MM-dd").format(diaFrequencia);
		if(GestorDeTarefasServidor.getInstancia().existeTarefaExecutando("frequencia_" + p.getUsuario()
		+ "_" + dataFormatada)){
			return new Retorno<>(false, Status.PENDENTE);
		}else{
			FrequenciaControle frequenciaControle = new FrequenciaControle();
			Retorno<Frequencia> ret = frequenciaControle.getModeloByChaveSecundaria(dataFormatada);
			if(ObjectUtils.isNullOrEmpty(ret.getLista())) return new Retorno<>(false, Status.PENDENTE);
			if(ret.getLista().get(0).getStatus().equals(Status.CONFLITO)){
				for(Frequencia frequencia : ret.getLista()){
					FrequenciaAlunoControle frequenciaAlunoControle = new FrequenciaAlunoControle();
					Map<String, Object> map = new HashMap<>();
					map.put("frequencia_id", frequencia.getId());
					Retorno<FrequenciaAluno> retFreqAluno = frequenciaAlunoControle.getModelosByFieldValues(map);
					frequencia.setFrequenciaAlunos(retFreqAluno.getLista());
				}
				ret.setMensagem(Status.CONFLITO);
				return ret;
			}
			return new Retorno<>(true, Status.SINCRONIZADA);
		}
	}


	@WebMethod(operationName = "atualizarFrequencia", action = "urn:AtualizarFrequencia")
	@WebResult(name = "return")
	public Retorno<Frequencia> atualizarFrequencia(@WebParam(name = "arg0") String token, @WebParam(name = "arg1") List<Frequencia> frequencias){
		Professor p = getProfessor(token);
		String dataFormatada = new SimpleDateFormat("yyyy-MM-dd").format(frequencias.get(0).getDataFrequencia());
		if(GestorDeTarefasServidor.getInstancia().existeTarefaExecutando("frequencia_" + p.getUsuario()
		+ "_" + dataFormatada)){
			return new Retorno<>(false, "Existe uma sincroniza��o pendente para essa frequ�ncia. N�o � poss�vel"
					+ " sincronizar no momento.", Severidade.ERRO);
		}
		try {
			PluginCapability capabilities = new PluginCapability();
			capabilities.setCapability(PluginCapability.USER, p.getUsuario());
			capabilities.setCapability(PluginCapability.PASSWORD, p.getSenha());
			capabilities.setCapability(PluginCapability.PLUGIN_PATH, "br.ueg.plugin.FenixWebCrawlerPlugin");
			capabilities.setCapability(PluginCapability.ALLOCATE_NEW_DRIVER, true);
			FrequenciaPluginControle frequenciaPluginControle = new FrequenciaPluginControle(capabilities);
			Retorno<Frequencia> retPluginFreq = new Retorno<>(false);
			int tentativas = 0;
			int numMax = 3;
			OfertaDisciplinaControle ofertaDisciplinaControle = new OfertaDisciplinaControle();
			Retorno<OfertaDisciplina> retOfertaDisc = 
					ofertaDisciplinaControle.getModeloByID(frequencias.get(0).getOfertaDisciplina().getId());
			if(!retOfertaDisc.isOk()) return new Retorno<>(false, retOfertaDisc.getMensagem(), retOfertaDisc.getSeveridade());
			while(tentativas < numMax){
				retPluginFreq = 
						frequenciaPluginControle.getFrequencia(frequencias.get(0).getDataFrequencia(), 
								retOfertaDisc.getValor());
				if(retPluginFreq.isOk()){
					break;
				}else{
					tentativas++;
				}
			}
			if(!retPluginFreq.isOk()){
				frequenciaPluginControle.devolverPlugin();
				return retPluginFreq;
			}else if(ObjectUtils.isNullOrEmpty(retPluginFreq.getLista())){
				frequenciaPluginControle.devolverPlugin();
				return new Retorno<>(false, "N�o foi encontrada nenhuma frequ�ncia para este dia.", Severidade.SUCESSO);
			}
			FrequenciaControle frequenciaControle = new FrequenciaControle();
			frequenciaPluginControle.devolverPlugin();
			return frequenciaControle.substituirFrequencia(frequencias, retPluginFreq.getLista());
		} catch (PluginInitException e) {
			e.printStackTrace();
			return new Retorno<>(false, e.getMessage(), Severidade.ERRO);
		}

	}

	private void agendar(String token, List<Frequencia> frequencias){
		FrequenciaControle frequenciaControle = new FrequenciaControle();
		for(Frequencia frequencia : frequencias){
			frequenciaControle.inserirOuAtualizar(frequencia);
		}
		Professor p = getProfessor(token);
		String dataFrequencia = new SimpleDateFormat("yyyy-MM-dd").format(frequencias.get(0).getDataFrequencia());
		TarefaFrequencia tarefaFrequencia = new TarefaFrequencia("{\"usuario\":" +p.getUsuario()+",\"data_frequencia\":\"" 
				+ dataFrequencia + "\"}");
		GestorDeTarefasServidor.getInstancia().agendarTarefa(tarefaFrequencia);
	}

	private Professor getProfessor(String token){
		LogAcessoControle logAcessoControle = new LogAcessoControle();
		Map<String, Object> map = new HashMap<>();
		map.put("token", token);
		Retorno<LogAcesso> retLogAcesso = logAcessoControle.getModelosByFieldValues(map);
		return retLogAcesso.getLista().get(0).getProfessor();
	}

}
