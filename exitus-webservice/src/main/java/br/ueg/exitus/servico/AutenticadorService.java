package br.ueg.exitus.servico;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import br.ueg.exitus.dto.ProfessorDTO;
import br.ueg.exitus.modelo.LogAcesso;
import br.ueg.exitus.plugin.PluginInitException;
import br.ueg.exitus.servico.controle.AutenticadorControle;
import br.ueg.exitus.utils.Retorno;

@WebService(targetNamespace = "http://servico.exitus.ueg.br/", portName = "AutenticadorServicePort", serviceName = "AutenticadorServiceService")
public class AutenticadorService {

	@WebMethod(operationName = "autenticar", action = "urn:Autenticar")
	@WebResult(name = "return")
	public Retorno<ProfessorDTO> autenticar(@WebParam(name = "arg0") String usuario, @WebParam(name = "arg1") String senha){
		try {
			Retorno<LogAcesso> retLog = new AutenticadorControle().autenticar(usuario, senha);
			Retorno<ProfessorDTO> retProfDTO = new Retorno<>();
			retProfDTO.copiar(retLog);
			if(retLog.isOk()){
				retProfDTO.setValor(new ProfessorDTO(retLog.getValor().getProfessor(), retLog.getValor().getToken()));
			}
			return retProfDTO;
		} catch (PluginInitException e) {
			e.printStackTrace();
			return new Retorno<>(false, e.getMessage(), e.getSeveridade());
		}
	}

	@WebMethod(operationName = "getIdentificadorUsuario", action = "urn:GetIdentificadorUsuario")
	@WebResult(name = "return")
	public String getIdentificadorUsuario(){
		try {
			return new AutenticadorControle().getIdentificadorUsuario();
		} catch (PluginInitException e) {
			e.printStackTrace();
			return "";
		}
	}

	@WebMethod(operationName = "validarToken", action = "urn:ValidarToken")
	@WebResult(name = "return")
	public Retorno<Object> validarToken(@WebParam(name = "arg0") String token){
		return new AutenticadorControle().validarToken(token);
	}

}
