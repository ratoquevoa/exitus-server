package br.ueg.exitus.servico;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import br.ueg.exitus.controle.LogAcessoControle;
import br.ueg.exitus.controle.OfertaDisciplinaProfessorControle;
import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.modelo.LogAcesso;
import br.ueg.exitus.modelo.OfertaDisciplinaProfessor;
import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.sincronizacao.GestorDeTarefasServidor;
import br.ueg.exitus.sincronizacao.tarefas.TarefaOfertaDisciplinaProfessor;
import br.ueg.exitus.utils.ObjectUtils;
import br.ueg.exitus.utils.Retorno;

@WebService(targetNamespace = "http://servico.exitus.ueg.br/", portName = "OfertaDisciplinaProfessorServicePort", serviceName = "OfertaDisciplinaProfessorServiceService")
public class OfertaDisciplinaProfessorService {

	@WebMethod(operationName = "getOfertaDisciplinaProfessor", action = "urn:GetOfertaDisciplinaProfessor")
	@WebResult(name = "return")
	public Retorno<OfertaDisciplinaProfessor> getOfertaDisciplinaProfessor(@WebParam(name = "arg0") String token, @WebParam(name = "arg1") AnoSemestre anoSemestre, @WebParam(name = "arg2") Date dataAtualizacao){
		Map<String, Object> map = new HashMap<>();
		map.put("ofertaDisciplina.ofertaCurso.anoSemestre.id", anoSemestre.getId());
		map.put("ofertaDisciplinaProfessor.dataAtualizacao", dataAtualizacao);
		Retorno<OfertaDisciplinaProfessor> ret = 
				new OfertaDisciplinaProfessorControle().getModelosByFieldValues(map);
		Professor p = getProfessor(token);
		if(GestorDeTarefasServidor.getInstancia().existeTarefaExecutando("ofertadisciplinaprofessor_" + p.getUsuario() 
				+ "_" + anoSemestre.getDescricao())){
			return new Retorno<>(false);
		}
		if(ObjectUtils.isNullOrEmpty(ret.getLista())){
			ret.setOk(false);
		}
		String data = new SimpleDateFormat("yyyy-MM-dd").format(dataAtualizacao);
		TarefaOfertaDisciplinaProfessor tarefaOfertaDisciplinaProfessor = new TarefaOfertaDisciplinaProfessor("{\"usuario\":" +p.getUsuario()+",\"ano_semestre\":\"" 
				+ anoSemestre.getDescricao() + "\",\"data_atualizacao\":\"" + data +"\"}");
		GestorDeTarefasServidor.getInstancia().agendarTarefa(tarefaOfertaDisciplinaProfessor);
		return ret;

	}
	
	private Professor getProfessor(String token){
		LogAcessoControle logAcessoControle = new LogAcessoControle();
		Map<String, Object> map = new HashMap<>();
		map.put("token", token);
		Retorno<LogAcesso> retLogAcesso = logAcessoControle.getModelosByFieldValues(map);
		return retLogAcesso.getLista().get(0).getProfessor();
	}

}
