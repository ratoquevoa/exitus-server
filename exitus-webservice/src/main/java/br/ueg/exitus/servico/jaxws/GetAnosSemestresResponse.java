
package br.ueg.exitus.servico.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class was generated by Apache CXF 3.2.0
 * Mon Nov 26 22:39:43 BRST 2018
 * Generated source version: 3.2.0
 */

@XmlRootElement(name = "getAnosSemestresResponse", namespace = "http://servico.exitus.ueg.br/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAnosSemestresResponse", namespace = "http://servico.exitus.ueg.br/")

public class GetAnosSemestresResponse {

    @XmlElement(name = "return")
    private br.ueg.exitus.utils.Retorno<br.ueg.exitus.modelo.AnoSemestre> _return;

    public br.ueg.exitus.utils.Retorno<br.ueg.exitus.modelo.AnoSemestre> getReturn() {
        return this._return;
    }

    public void setReturn(br.ueg.exitus.utils.Retorno<br.ueg.exitus.modelo.AnoSemestre> new_return)  {
        this._return = new_return;
    }

}

