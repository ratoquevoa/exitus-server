package br.ueg.exitus.utils;

public enum Severidade {
	ALERTA("alerta"), ERRO("erro"), SUCESSO("sucesso"), INFORMATIVO("info");
	
	private final String valor;
	
	Severidade(String valor){
		this.valor = valor;
	}
	
	public String getValor(){
		return valor;
	}

}
