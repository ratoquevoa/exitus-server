package br.ueg.exitus.utils;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.service.DriverService;

import br.ueg.exitus.plugin.AbstractWebCrawler;

public abstract class DriverPool<D extends WebDriver, S extends DriverService> {
	protected long tempoDeExpiracao;
	protected int numeroMax;
	private List<PooledDriver<D,S>> alocado, livre;

	protected DriverPool() {
		tempoDeExpiracao = 1200000; //1200000 - 20 minutos
		numeroMax = 5;
		alocado = new ArrayList<>();
		livre = new ArrayList<>();
	}

	public void liberarDriver(PooledDriver<D, S> driver){
		if(driver == null) return;
		if(driver.isLiberarRecursos()){
			driver.liberarRecursos();
			driver = null;
		}else{
			driver.setEmUso(false);
		}
	}
	
	public Retorno<PooledDriver<D, S>> getDriverWithoutPoolControl(String usuario, String senha, AbstractWebCrawler<D, S> plugin) throws DriverEmUsoException {
		PooledDriver<D, S> pooledDriver = new PooledDriver<>();
		pooledDriver.setDriverService(criarDriverService());
		pooledDriver.setDriver(criarDriver(pooledDriver.getDriverService()));
		pooledDriver.setLiberarRecursos(true);
		return reservar(pooledDriver, usuario, senha, System.currentTimeMillis(), plugin);
	}

	public synchronized Retorno<PooledDriver<D, S>> getDriver(String usuario, String senha, AbstractWebCrawler<D, S> plugin) throws DriverEmUsoException {
		long agora = System.currentTimeMillis();
		System.out.println();
		System.out.println("Tamanho do pool: "+alocado.size());
		System.out.println();
		ArrayList<PooledDriver<D, S>> copia = new ArrayList<>(alocado);
		for(PooledDriver<D, S> pooledDriver : copia){
			//Expirado
			if ((agora - pooledDriver.getMomento()) > tempoDeExpiracao){
				alocado.remove(pooledDriver);
				expirar(pooledDriver);
				livre.add(pooledDriver);
				System.out.println("Driver expirado e desalocado do usuario : " + usuario);
			}else if(validar(pooledDriver, usuario, senha)){
				if(pooledDriver.isEmUso()){
					throw new DriverEmUsoException("Driver em uso");
				}
				System.out.println("Driver em cache encontrado para : " + usuario + ". Tempo renovado");
				Retorno<PooledDriver<D, S>> ret = new Retorno<>(true);
				pooledDriver.setMomento(agora);
				pooledDriver.setEmUso(true);
				ret.setValor(pooledDriver);
				return ret;
			}
		}

		//Caso o tamanho de objetos alocados seja menor que o m�ximo permitido, um novo objeto � criado e retornado
		if (alocado.size() < numeroMax) {
			PooledDriver<D, S> pooledDriver = new PooledDriver<D, S>();
			pooledDriver.setDriverService(criarDriverService());
			pooledDriver.setDriver(criarDriver(pooledDriver.getDriverService()));
			Retorno<PooledDriver<D, S>> ret = reservar(pooledDriver, usuario, senha, agora, plugin);
			if(ret.isOk()){
				ret.setValor(pooledDriver);
				alocado.add(pooledDriver);
				System.out.println("Espa�o ainda livre. Driver alocado e retornado para : " + usuario);
			}else{
				livre.add(pooledDriver);
			}
			return ret;
		}


		//Caso existam objetos livres, um objeto � reservado, alocado e retornado.
		if(livre.size() > 0){
			PooledDriver<D, S> pooledDriver = livre.get(0);
			Retorno<PooledDriver<D, S>> ret = reservar(pooledDriver, usuario, senha, agora, plugin);
			if(ret.isOk()){
				livre.remove(0);
				alocado.add(pooledDriver);
				System.out.println("Driver livre alocado para : " + usuario);
			}
			return ret;
		}else{ //Caso contr�rio, o objeto menos utilizado � removido e alocado um novo.
			PooledDriver<D, S> driverMaisAntigo = alocado.get(0);
			for(int i = 1; i < alocado.size(); i++){
				PooledDriver<D, S> pooledDriver = alocado.get(i);
				if(pooledDriver.getMomento() < driverMaisAntigo.getMomento()){
					driverMaisAntigo = pooledDriver;
				}
			}
			Retorno<PooledDriver<D, S>> ret = reservar(driverMaisAntigo, usuario, senha, agora, plugin);
			if(ret.isOk()){
				System.out.println("Sem espa�o livre. Driver do usuario : "+ driverMaisAntigo.getUsuario() +" desalocado e alocado para : " + usuario);
			}
			return ret;
		}
	}

	public abstract D criarDriver(S driverService);
	
	protected abstract S criarDriverService();

	public boolean validar(PooledDriver<D, S> pooledDriver, String usuario, String senha) {
		return pooledDriver.getUsuario().equals(usuario) && pooledDriver.getSenha().equals(senha);
	}

	public void setInfo(PooledDriver<D, S> driver, String usuario, String senha, long momento, boolean emUso){
		driver.setUsuario(usuario);
		driver.setSenha(senha);
		driver.setMomento(momento);
		driver.setEmUso(emUso);
	}

	public void expirar(PooledDriver<D, S> driver) {
		driver.sair();
		setInfo(driver, "", "", 0L, false);
	}

	public Retorno<PooledDriver<D, S>> reservar(PooledDriver<D, S> pooledDriver, String usuario, String senha, long momento, AbstractWebCrawler<D, S> plugin) throws DriverEmUsoException{
		if(pooledDriver.getUsuario() != null && !pooledDriver.getUsuario().equals(usuario) && pooledDriver.isAutenticado()){
			pooledDriver.sair();
		}
		setInfo(pooledDriver, usuario, senha, momento, true);
		pooledDriver.setPlugin(plugin);
		Retorno<PooledDriver<D, S>> ret = new Retorno<>(true);
		ret.copiar(pooledDriver.autenticar());
		ret.setValor(pooledDriver);
		return ret;
	}

}
