package br.ueg.exitus.utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {
	
	public static int comparar(Date date1, Date date2){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date1);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.HOUR, 0);
		date1 = calendar.getTime();
		calendar.setTime(date2);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.HOUR, 0);
		date2 = calendar.getTime();
		System.out.println("DateUtils: " + date1 + "  " + date2);
		return date1.compareTo(date2);
	}

}
