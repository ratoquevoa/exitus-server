package br.ueg.exitus.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class HibernateUtils {
	private static HibernateUtils instancia;
	private SessionFactory fabricaSessao;

	private HibernateUtils(){
		fabricaSessao = new Configuration().configure().buildSessionFactory();
	}

	public static HibernateUtils getInstancia(){
		if(instancia == null){
			instancia = new HibernateUtils();
		}
		return instancia;
	}
	
	public Session getSessao() {
		return fabricaSessao.openSession();
	}

}
