package br.ueg.exitus.utils;

import java.math.BigInteger;

import org.hibernate.FlushMode;
import org.hibernate.Session;

public class DefaultPostgresKeyServer {
	private static DefaultPostgresKeyServer instancia;

	private DefaultPostgresKeyServer(){}

	public static DefaultPostgresKeyServer getInstancia(){
		if(instancia == null){
			instancia = new DefaultPostgresKeyServer();
		}
		return instancia;
	}

	public Long getNextKey(){
		Session sessao = null;
		try{
			sessao = HibernateUtils.getInstancia().getSessao();
			sessao.setHibernateFlushMode(FlushMode.MANUAL);
			BigInteger value = (BigInteger) sessao.createNativeQuery("select nextval('public.\"hibernate_sequence\"')").list().get(0);
			sessao.close();
			return value.longValue();
		}catch(Exception e){
			if(sessao != null) sessao.close();
			e.printStackTrace();
			return -1L;
		}
	}


}
