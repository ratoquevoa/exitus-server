package br.ueg.exitus.utils;

import java.util.List;

public class Retorno<T> {
	private String mensagem;
	private String severidadeMensagem;
	private Severidade severidade;
	private T valor;
	private List<T> lista;
	private boolean ok;
	
	public Retorno(){}
	
	public Retorno(boolean ok) {
		super();
		this.ok = ok;
	}
	
	public Retorno(boolean ok, String mensagem) {
		super();
		this.mensagem = mensagem;
		this.ok = ok;
	}
	
	public Retorno(boolean ok, String mensagem, Severidade severidade) {
		super();
		this.mensagem = mensagem;
		this.severidadeMensagem = severidade.getValor();
		this.severidade = severidade;
		this.ok = ok;
	}
	
	public Retorno(boolean ok, String mensagem, Severidade severidade, T valor) {
		super();
		this.mensagem = mensagem;
		this.severidadeMensagem = severidade.getValor();
		this.severidade = severidade;
		this.valor = valor;
		this.ok = ok;
	}

	public Retorno(boolean ok, String mensagem, Severidade severidade, T valor, List<T> lista) {
		super();
		this.mensagem = mensagem;
		this.severidadeMensagem = severidade.getValor();
		this.severidade = severidade;
		this.valor = valor;
		this.lista = lista;
		this.ok = ok;
	}
	
	public void copiar(Retorno<?> ret){
		this.ok = ret.isOk();
		this.mensagem = ret.getMensagem();
		this.severidade = ret.getSeveridade();
		this.severidadeMensagem  = ret.getSeveridadeMensagem();
	}
	
	public void concat(Retorno<T> ret){
		this.ok = ret.isOk();
		this.mensagem = ret.getMensagem();
		this.severidade = ret.getSeveridade();
		this.severidadeMensagem  = ret.getSeveridadeMensagem();
		this.valor = ret.getValor();
		this.lista.addAll(ret.getLista());
	}

	public String getMensagem() {
		return mensagem;
	}
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	public String getSeveridadeMensagem() {
		return severidadeMensagem;
	}
	
	public void setSeveridadeMensagem(String severidadeMensagem) {
		this.severidadeMensagem = severidadeMensagem;
	}
	
	public T getValor() {
		return valor;
	}
	
	public void setValor(T valor) {
		this.valor = valor;
	}
	
	public List<T> getLista() {
		return lista;
	}
	
	public void setLista(List<T> lista) {
		this.lista = lista;
	}
	
	public boolean isOk() {
		return ok;
	}
	
	public void setOk(boolean ok) {
		this.ok = ok;
	}

	public Severidade getSeveridade() {
		return severidade;
	}

	public void setSeveridade(Severidade severidade) {
		this.severidade = severidade;
	}
}
