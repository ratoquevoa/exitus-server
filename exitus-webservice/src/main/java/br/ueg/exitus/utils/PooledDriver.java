package br.ueg.exitus.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.service.DriverService;

import br.ueg.exitus.plugin.AbstractWebCrawler;

public class PooledDriver<D extends WebDriver, S extends DriverService> {
	
	protected D driver;
	protected S driverService;
	protected long momento;
	protected String usuario;
	protected String senha;
	protected boolean emUso;
	protected AbstractWebCrawler<D, S> plugin;
	protected boolean autenticado;
	protected boolean liberarRecursos;
	
	public PooledDriver(){}
	
	public PooledDriver(D driver, S driverService, long momento, String usuario, String senha, 
			AbstractWebCrawler<D, S> plugin) {
		super();
		this.driver = driver;
		this.driverService = driverService;
		this.momento = momento;
		this.usuario = usuario;
		this.senha = senha;
		this.plugin = plugin;
		this.liberarRecursos = false;
		this.emUso = false;
	}
	public D getDriver() {
		return driver;
	}
	public void setDriver(D driver) {
		this.driver = driver;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}

	public long getMomento() {
		return momento;
	}

	public void setMomento(long momento) {
		this.momento = momento;
	}
	
	public boolean isEmUso() {
		return emUso;
	}

	public void setEmUso(boolean emUso) {
		this.emUso = emUso;
	}
	
	public Retorno<Object> autenticar(){
		Retorno<Object> ret = plugin.autenticar(driver, usuario, senha);
		autenticado = ret.isOk();
		return ret;
	}
	
	public void sair(){
		autenticado = false;
		plugin.sair();
	}
	
	public void liberarRecursos(){
		driver.manage().deleteAllCookies();
		driver.quit();
		driverService.stop();
	}

	public AbstractWebCrawler<D, S> getPlugin() {
		return plugin;
	}

	public void setPlugin(AbstractWebCrawler<D, S> plugin) {
		this.plugin = plugin;
	}

	public boolean isAutenticado() {
		return autenticado;
	}

	public void setAutenticado(boolean autenticado) {
		this.autenticado = autenticado;
	}

	public boolean isLiberarRecursos() {
		return liberarRecursos;
	}

	public void setLiberarRecursos(boolean liberarRecursos) {
		this.liberarRecursos = liberarRecursos;
	}

	public S getDriverService() {
		return driverService;
	}

	public void setDriverService(S driverService) {
		this.driverService = driverService;
	}
}
