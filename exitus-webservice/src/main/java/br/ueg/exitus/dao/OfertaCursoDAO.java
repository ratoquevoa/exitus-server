package br.ueg.exitus.dao;

import java.util.List;

import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.modelo.Campus;
import br.ueg.exitus.modelo.Curso;
import br.ueg.exitus.modelo.Matriz;
import br.ueg.exitus.modelo.OfertaCurso;
import br.ueg.exitus.utils.Retorno;
import br.ueg.exitus.utils.Severidade;

public class OfertaCursoDAO extends DAOGenerico<OfertaCurso> {

	public Retorno<OfertaCurso> getByAnoSemestre(AnoSemestre anoSemestre){
		try{
			iniciarTransacao();
			List<OfertaCurso> lista = sessao.createQuery("from oferta_curso where ano_semestre_id = " + anoSemestre.getId()).list();
			finalizarTransacao();
			return new Retorno<OfertaCurso>(true, "", Severidade.SUCESSO, null , lista);
		}catch(Exception e){
			cancelarTransacao();
			return new Retorno<OfertaCurso>(false, e.getMessage(), Severidade.ERRO);
		}
	}
	
	public Retorno<OfertaCurso> getByAnoSemestreCampusCursoMatriz(AnoSemestre anoSemestre,
			Campus campus, Curso curso, Matriz matriz){
		try{
			iniciarTransacao();
			List<OfertaCurso> lista = sessao.createQuery("from oferta_curso where ano_semestre_id = " + anoSemestre.getId()
					+ " and campus_id = " + campus.getId() + " and curso_id = " + curso.getId() + " matriz_id = " + matriz.getId()).list();
			finalizarTransacao();
			return new Retorno<OfertaCurso>(true, "", Severidade.SUCESSO, null , lista);
		}catch(Exception e){
			cancelarTransacao();
			return new Retorno<OfertaCurso>(false, e.getMessage(), Severidade.ERRO);
		}
	}
}
