package br.ueg.exitus.dao;

import java.util.List;

import br.ueg.exitus.modelo.Professor;
import br.ueg.exitus.utils.Retorno;
import br.ueg.exitus.utils.Severidade;

public class ProfessorDAO extends DAOGenerico<Professor>{

	public Retorno<Professor> buscarPorUsuario(String usuario){
		try{
			iniciarTransacao();
			List<Professor> lista = sessao.createQuery("from Professor where usuario = '" + usuario + "'").list();
			finalizarTransacao();
			Professor p = null;
			if(!lista.isEmpty()){
				p = lista.get(0);
			}
			return new Retorno<Professor>(true, "", Severidade.SUCESSO, p, lista);
		}catch(Exception e){
			e.printStackTrace();
			cancelarTransacao();
			return new Retorno<Professor>(false,"Falha ao buscar por usuario", Severidade.ALERTA);
		}
	}

}
