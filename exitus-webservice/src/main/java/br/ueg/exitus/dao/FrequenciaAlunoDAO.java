package br.ueg.exitus.dao;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;

import br.ueg.exitus.modelo.FrequenciaAluno;
import br.ueg.exitus.utils.Retorno;
import br.ueg.exitus.utils.Severidade;

public class FrequenciaAlunoDAO extends DAOGenerico<FrequenciaAluno> {

	public Retorno<FrequenciaAluno> deletarFrequenciaAluno(Long... frequenciaIds){
		EntityManager em = null;
		try{
			iniciarTransacao();
			em = sessao.getEntityManagerFactory().createEntityManager();
			em.getTransaction().begin();
			CriteriaBuilder criteriaBuilder  = em.getCriteriaBuilder();
			CriteriaDelete<FrequenciaAluno> query = criteriaBuilder.createCriteriaDelete(FrequenciaAluno.class);
			Root<FrequenciaAluno> root = query.from(FrequenciaAluno.class);
			query.where(root.get("frequencia").in((Object[]) frequenciaIds));
			em.createQuery(query).executeUpdate();
			em.getTransaction().commit();
			finalizarTransacao();
			return new Retorno<>(true);
		}catch(Exception e){
			if(em != null) em.getTransaction().rollback();
			cancelarTransacao();
			e.printStackTrace();
			return new Retorno<>(false, e.getMessage(), Severidade.ERRO);
		}
	}
	
//	@Override
//	public Retorno<FrequenciaAluno> inserirOuAtualizar(FrequenciaAluno modelo) {
//		Retorno<FrequenciaAluno> modeloByID = getModeloByID(modelo.getId());
//		boolean atualizar = modeloByID.getValor() != null;
//		try{
//			iniciarTransacao();
//			if(atualizar){
//				sessao.update(modelo);
//			}else{
//				sessao.save(modelo);
//			}
//			finalizarTransacao();
//			return new Retorno<>(true, "", Severidade.SUCESSO);
//			//			}
//			//			return ret;
//		}catch(Exception e){
//			cancelarTransacao();
//			return new Retorno<>(false,"Falha ao cadastrar", Severidade.ALERTA);
//		}
//	}

}
