package br.ueg.exitus.dao;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.hibernate.FlushMode;
import org.hibernate.Session;

import br.ueg.exitus.sincronizador.utils.ChaveSecundaria;
import br.ueg.exitus.sincronizador.utils.Comparable;
import br.ueg.exitus.sincronizador.utils.ReflectionUtils;
import br.ueg.exitus.utils.HibernateUtils;
import br.ueg.exitus.utils.ObjectUtils;
import br.ueg.exitus.utils.OrderBy;
import br.ueg.exitus.utils.Retorno;
import br.ueg.exitus.utils.Severidade;

public abstract class DAOGenerico<M> {
	private Class<M> classeModelo;
	protected Session sessao;

	@SuppressWarnings("unchecked")
	public DAOGenerico(){
		classeModelo = ((Class<M>) ((ParameterizedType) 
				(getClass().getGenericSuperclass())).getActualTypeArguments()[0]);
	}

	/**
	 * Persiste o modelo M no banco de dados
	 * @param modelo Objeto a ser persistido
	 * @return Retorna 
	 */
	public Retorno<M> inserir(M modelo){
		try{
			//			Retorno<M> ret = persistirObjetos();
			//			if(ret.isOk()){
			iniciarTransacao();
			sessao.save(modelo);
			finalizarTransacao();
			return new Retorno<M>(true, classeModelo.getSimpleName() + " cadastrado com sucesso", Severidade.SUCESSO);
			//			}
			//			return ret;
		}catch(Exception e){
			cancelarTransacao();
			return new Retorno<M>(false,"Falha ao cadastrar: " + e.getMessage(), Severidade.ALERTA);
		}
	}

	public Retorno<M> inserirOuAtualizar(M modelo){
		try{
			//			Retorno<M> ret = persistirObjetos();
			//			if(ret.isOk()){
			iniciarTransacao();
			sessao.saveOrUpdate(modelo);
			finalizarTransacao();
			return new Retorno<M>(true, classeModelo.getSimpleName() + " cadastrado com sucesso", Severidade.SUCESSO);
			//			}
			//			return ret;
		}catch(Exception e){
			cancelarTransacao();
			return new Retorno<M>(false,"Falha ao cadastrar", Severidade.ALERTA);
		}
	}

	public Retorno<M> excluir(M modelo){
		try{
			iniciarTransacao();
			sessao.delete(modelo);
			finalizarTransacao();
			return new Retorno<M>(true, classeModelo.getSimpleName() + " exclu�do com sucesso", Severidade.SUCESSO);
		}catch(Exception e){
			e.printStackTrace();
			cancelarTransacao();
			return new Retorno<M>(false,"Falha ao excluir", Severidade.ALERTA);
		}
	}

	public Retorno<M> alterar(M modelo){
		try{
			iniciarTransacao();
			sessao.update(modelo);
			finalizarTransacao();
			return new Retorno<M>(true, classeModelo.getSimpleName() + " alterado com sucesso", Severidade.SUCESSO);
		}catch(Exception e){
			e.printStackTrace();
			cancelarTransacao();
			return new Retorno<M>(false,"Falha ao alterar", Severidade.ALERTA);
		}
	}

	public Retorno<M> alterarPelaChaveSecundaria(M modelo){
		EntityManager em = null;
		try{
			iniciarTransacao();
			em = sessao.getEntityManagerFactory().createEntityManager();
			em.getTransaction().begin();
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaUpdate<M> criteriaUpdate = criteriaBuilder.createCriteriaUpdate(classeModelo);
			Root<M> root = criteriaUpdate.from(classeModelo);
			Map<String, Object> map = ReflectionUtils.getInstancia().getFieldsWithValueWithAnnotation(modelo, Comparable.class);
			Iterator<String> iterator = map.keySet().iterator();
			while(iterator.hasNext()){
				String name = iterator.next();
				criteriaUpdate.set(root.get(name), map.get(name));
			}
			Map<String, Object> mapChaveSecundaria = ReflectionUtils.getInstancia()
					.getFieldsWithValueWithAnnotation(modelo, ChaveSecundaria.class);
			String nameChaveSecundaria = mapChaveSecundaria.keySet().iterator().next();
			Expression<Boolean> filterPredicate = criteriaBuilder
					.equal(criteriaUpdate.getRoot().get(nameChaveSecundaria), mapChaveSecundaria.get(nameChaveSecundaria));
			criteriaUpdate.where(filterPredicate);
			em.createQuery(criteriaUpdate).executeUpdate();
			em.getTransaction().commit();
			finalizarTransacao();
			return new Retorno<M>(true, classeModelo.getSimpleName() + " alterado com sucesso", Severidade.SUCESSO);
		}catch(Exception e){
			e.printStackTrace();
			if(em != null) em.getTransaction().rollback();
			cancelarTransacao();
			return new Retorno<M>(false,"Falha ao alterar", Severidade.ALERTA);
		}
	}

	@SuppressWarnings("unchecked")
	public Retorno<M> listar(M modelo){
		Retorno<M> ret = new Retorno<M>(true);
		try{
			if(modelo == null){
				iniciarTransacao();
				String consulta = "from " + getNomeTabela();
				String orderBy = ReflectionUtils.getInstancia().getFieldNameWithAnnotation(classeModelo.newInstance(), OrderBy.class);
				if(orderBy != null){
					consulta += " order by " + orderBy + " desc";
				}
				System.out.println(consulta);
				ret.setLista(sessao.createQuery(consulta).list());
				finalizarTransacao();
				return ret;
			}else{
				Class<?> superClasse = classeModelo;
				StringBuilder condicoes = new StringBuilder();
				StringBuilder consulta = new StringBuilder();
				consulta.append("from " + getNomeTabela());
				while(superClasse != null && !superClasse.getSimpleName().equals("Object")){
					Field[] atributos = superClasse.getDeclaredFields();
					for(Field atributo : atributos){
						if(atributo.getAnnotation(ManyToMany.class) != null 
								|| atributo.getAnnotation(OneToOne.class) != null 
								|| atributo.getAnnotation(OneToMany.class) != null 
								|| atributo.getAnnotation(ManyToOne.class) != null) continue;
						String nomeAtributo = atributo.getName().substring(0,1).toUpperCase() + atributo.getName().substring(1);
						String nomeMetodo = "get" + nomeAtributo;
						Method getAtributo = superClasse.getDeclaredMethod(nomeMetodo, new Class<?>[0]);
						Object valor = getAtributo.invoke(modelo, new Object[0]);
						if(valor == null) continue;
						condicoes.append(" and " + atributo.getName() + " = '" + valor + "'");
					}
					superClasse = superClasse.getSuperclass();
				}
				if(condicoes.length() > 0){
					consulta.append(" where ");
					consulta.append(condicoes.substring(5));
				}
				String orderBy = ReflectionUtils.getInstancia().getFieldNameWithAnnotation(modelo, OrderBy.class);
				if(orderBy != null){
					consulta.append(" order by " + orderBy + " desc");
				}
				System.out.println(consulta);
				iniciarTransacao();
				ret.setLista(sessao.createQuery(consulta.toString()).list());
				finalizarTransacao();
			}
		}catch(IllegalAccessException | IllegalArgumentException | NoSuchMethodException | SecurityException | InvocationTargetException | InstantiationException e){
			cancelarTransacao();
			ret.setOk(false);
			ret.setMensagem(e.getMessage());
		}
		return ret;
	}

	protected String getNomeTabela(){
		Entity entityAnnotation = classeModelo.getAnnotation(Entity.class);
		if(entityAnnotation.name() != null && !entityAnnotation.name().isEmpty()){
			return entityAnnotation.name();
		}else{
			return classeModelo.getSimpleName();
		}
	}

	@SuppressWarnings("unchecked")
	public Retorno<M> getModelosAntigos(Date dataAtualizacao){
		try{
			iniciarTransacao();
			List<M> modelos = sessao.createQuery("from " + getNomeTabela()  + " where data_atualizacao < "
					+ "'" + new SimpleDateFormat("yyyy/MM/dd").format(dataAtualizacao) + "'").list();
			finalizarTransacao();
			return new Retorno<M>(true, "", Severidade.SUCESSO, null, modelos);
		}catch(Exception e){
			cancelarTransacao();
			return new Retorno<M>(false,e.getMessage(), Severidade.ALERTA);
		}

	}

	@SuppressWarnings("unchecked")
	public Retorno<M> getModelosByFieldValues(Map<String, Object> fieldValues){
		try{
			StringBuilder consulta = new StringBuilder();
			StringBuilder condicoes = new StringBuilder();
			consulta.append("from ");
			consulta.append(getNomeTabela());
			consulta.append(" as " + firstLetterLowercase(classeModelo.getSimpleName()));
			String orderBy = ReflectionUtils.getInstancia().getFieldNameWithAnnotation(classeModelo.newInstance(), OrderBy.class);
			Set<String> keySet = fieldValues.keySet();
			Iterator<String> it = keySet.iterator();
			while(it.hasNext()){
				String key = it.next();
				Object value = fieldValues.get(key);
				if(value instanceof Date){
					condicoes.append(" and " + key + " >= '" + new SimpleDateFormat("yyyy/MM/dd").format(value) + "'");
				}else{
					condicoes.append(" and " + key + " = '" + value + "'");
				}
			}
			if(condicoes.length() > 0){
				consulta.append(" where ");
				consulta.append(condicoes.substring(5));
			}
			if(orderBy != null){
				consulta.append(" order by " + orderBy + " desc");
			}
			iniciarTransacao();
			List<M> modelos = sessao.createQuery(consulta.toString()).list();
			finalizarTransacao();
			return new Retorno<M>(true, "", Severidade.SUCESSO, null, modelos);
		}catch(Exception e){
			cancelarTransacao();
			return new Retorno<M>(false,e.getMessage(), Severidade.ALERTA);
		}

	}
	
	public String firstLetterLowercase(String string){
		return string.substring(0,1).toLowerCase() + string.substring(1);
	}

	@SuppressWarnings("unchecked")
	public Retorno<M> getModeloByID(Long id){
		try{
			iniciarTransacao();
			List<M> modelos = sessao.createQuery("from " + getNomeTabela()  + " where id = '"+ id + "'").list();
			finalizarTransacao();
			M modelo = null;
			if(!ObjectUtils.isNullOrEmpty(modelos)){
				modelo = modelos.get(0);
			}
			return new Retorno<M>(true, "", Severidade.SUCESSO, modelo, modelos);
		}catch(Exception e){
			cancelarTransacao();
			return new Retorno<M>(false,e.getMessage(), Severidade.ALERTA);
		}

	}

	public Retorno<M> getByChaveSecundaria(String value){
		try{
			Field[] fields = classeModelo.getDeclaredFields();
			for(Field f : fields){
				if(f.isAnnotationPresent(ChaveSecundaria.class)){
					String coluna = f.getName();
					if(f.isAnnotationPresent(Column.class)){
						Column columnAnnotation = f.getAnnotation(Column.class);
						coluna = ObjectUtils.isNullOrEmpty(columnAnnotation.name()) ? coluna : columnAnnotation.name();
					}
					iniciarTransacao();
					System.out.println("from " + getNomeTabela()  + " where " + coluna + " = '"+ value + "'");
					List<M> modelos = sessao.createQuery("from " + getNomeTabela()  + " where " + coluna + " = '"+ value + "'").list();
					finalizarTransacao();
					M modelo = null;
					if(!ObjectUtils.isNullOrEmpty(modelos)){
						modelo = modelos.get(0);
					}
					return new Retorno<M>(true, "", Severidade.SUCESSO, modelo, modelos);
				}
			}
			return new Retorno<M>(false, classeModelo.getSimpleName()+" n�o possui chave secund�ria", Severidade.ERRO);
		}catch(Exception e){
			cancelarTransacao();
			return new Retorno<M>(false,e.getMessage(), Severidade.ALERTA);
		}
	}

	public void abrirSessao(){
		if(!sessaoAberta()){
			sessao = HibernateUtils.getInstancia().getSessao();
			sessao.setHibernateFlushMode(FlushMode.MANUAL);
		}
	}

	public void fecharSessao(){
		if(sessaoAberta()){
			sessao.close();
			sessao = null;
		}
	}

	public boolean sessaoAberta(){
		return sessao != null && sessao.isOpen();
	}

	public void iniciarTransacao(){
		abrirSessao();
		sessao.beginTransaction();
	}

	public void cancelarTransacao(){
		if(sessaoAberta()){
			sessao.getTransaction().rollback();
			fecharSessao();
		}
	}

	public void finalizarTransacao(){
		if(sessaoAberta()){
			sessao.flush();
			sessao.getTransaction().commit();
			fecharSessao();
		}
	}

	//	@SuppressWarnings("unchecked")
	//	public Retorno<M> buscarPorCPF(String cpf) {
	//		Retorno<M> ret = new Retorno<>(true);
	//		try{
	//			iniciarTransacao();
	//			ret.setLista(getSessao().createQuery("from Pessoa where usuario = '" + cpf + "'").list());
	//			finalizarTransacao();
	//		}catch(Exception e){
	//			e.printStackTrace();
	//			cancelarTransacao();
	//			ret.setOk(false);
	//			ret.setMensagem(e.getMessage());
	//		}
	//		return ret;	
	//	}
	//	public void limparSessao(){
	//		getSessao().clear();
	//	}

}
