package br.ueg.exitus.dao;

import br.ueg.exitus.modelo.LogAcesso;
import br.ueg.exitus.utils.Retorno;

public class LogAcessoDAO extends DAOGenerico<LogAcesso> {

	@Override
	public Retorno<LogAcesso> inserir(LogAcesso modelo) {
		iniciarTransacao();
		sessao.createQuery("update log_acesso set valido = 'false' where professor ='"+modelo.getProfessor().getId()+"'").executeUpdate();
		finalizarTransacao();
		return super.inserir(modelo);
	}

	
}
