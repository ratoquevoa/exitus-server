package br.ueg.exitus.dao;

import java.util.List;

import br.ueg.exitus.modelo.AnoSemestre;
import br.ueg.exitus.modelo.OfertaDisciplina;
import br.ueg.exitus.utils.Retorno;
import br.ueg.exitus.utils.Severidade;

public class OfertaDisciplinaDAO extends DAOGenerico<OfertaDisciplina> {

	public Retorno<OfertaDisciplina> getByAnoSemestre(AnoSemestre anoSemestre){
		try{
			iniciarTransacao();
			List<OfertaDisciplina> lista = sessao.createQuery("select od from oferta_disciplina as od"
															+ " inner join od.ofertaCurso as oc"
															+ " where oc.anoSemestre = " + anoSemestre.getId()).list();
			finalizarTransacao();
			return new Retorno<>(true, "", Severidade.SUCESSO, null , lista);
		}catch(Exception e){
			e.printStackTrace();
			cancelarTransacao();
			return new Retorno<>(false, e.getMessage(), Severidade.ERRO);
		}
	}
}
