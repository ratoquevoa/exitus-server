package br.ueg.exitus.sincronizador;

import java.util.ArrayList;
import java.util.List;

import br.ueg.exitus.sincronizador.tarefa.ITarefaAgendada;
import br.ueg.exitus.sincronizador.tarefa.ITarefaDAO;
import br.ueg.exitus.sincronizador.tarefa.TarefaImpl;

public abstract class GestorDeTarefas {

	private MonitoradorAbstract monitorador;
	private List<ITarefaAgendada<?>> listaExecucao;
	private ITarefaDAO<ITarefaAgendada<?>> tarefaDAO;

	protected GestorDeTarefas(){
		init();
	}

	public void init(){
		listaExecucao  = new ArrayList<>();
		tarefaDAO = getTarefaDAO();
		monitorador = getMonitorador();
		monitorador.setGestorDeTarefas(this);
		iniciarTarefasPendentesDaBaseLocal();
//		monitorador.iniciarMonitoramento(listaExecucao);
	}

	public synchronized void agendarTarefa(ITarefaAgendada<?> tarefa){
		if(existeNaListaDeExecucao(tarefa)) return;
		if(tarefa.getId() == null){
			tarefaDAO.persistirTarefaAgendada(tarefa);
		}
		tarefa.setTarefaDAO(tarefaDAO);
//		tarefa.atualizarTarefaNaBaseLocal();
		if(tarefa.ehDependente()){
			for(ITarefaAgendada<?> tarefasPai : tarefa.getTarefasPai()){
				if(!existeNaListaDeExecucao(tarefasPai)){
					agendarTarefa(tarefasPai);
				}
			}
			listaExecucao.add(tarefa);
		}else{
			listaExecucao.add(tarefa);
			iniciarTarefa(tarefa);
		}
		if(!monitorador.isAlive()){
			monitorador.iniciarMonitoramento(listaExecucao);
		}else if(monitorador.isWaiting()){
			monitorador.notificar();
		}
	}

	protected boolean existeNaListaDeExecucao(ITarefaAgendada<?> tarefa){
		for(ITarefaAgendada<?> tarefaExecucao : listaExecucao){
			if(tarefaExecucao.getIdentificador().equals(tarefa.getIdentificador())) return true;
		}
		return false;
	}
	
	public boolean existeTarefaExecutando(String identificador){
		System.out.println("Procurando tarefa com identificador: " + identificador);
		return existeNaListaDeExecucao(new TarefaImpl<>(identificador));
	}

	protected abstract ITarefaDAO getTarefaDAO();

	protected abstract MonitoradorAbstract getMonitorador();

	protected void iniciarTarefasPendentesDaBaseLocal(){
		List<ITarefaAgendada<?>> tarefasAgendadas = tarefaDAO.getTarefasPendentes().getLista();
		for(ITarefaAgendada<?> tarefa : tarefasAgendadas){
			agendarTarefa(tarefa);
		}
	}

	public void iniciarTarefa(ITarefaAgendada<?> tarefa){
		monitorador.iniciarTarefa(tarefa, tarefaDAO);
	}
}
