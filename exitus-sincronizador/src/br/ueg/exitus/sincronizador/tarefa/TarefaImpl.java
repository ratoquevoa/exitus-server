package br.ueg.exitus.sincronizador.tarefa;

import java.util.Date;
import java.util.List;

import br.ueg.exitus.sincronizador.utils.SyncResult;

public class TarefaImpl<E extends Sincronizavel> implements ITarefaAgendada<E> {
	private String identificador;
	
	public TarefaImpl(String identificador) {
		this.identificador = identificador;
	}

	@Override
	public void init(String params) {
		
	}

	@Override
	public void iniciarExecucao() {
		
	}

	@Override
	public void executar() {
		
	}

	@Override
	public boolean podeIniciar() {
		return false;
	}

	@Override
	public boolean ehDependente() {
		return false;
	}

	@Override
	public Integer getId() {
		return null;
	}

	@Override
	public String getStatus() {
		return null;
	}

	@Override
	public void setStatus(String status) {
		
	}

	public void setTabela(String tabela){
	}
	
	@Override
	public String getTabela() {
		return null;
	}

	@Override
	public Date getDataFinalizacao() {
		return null;
	}

	@Override
	public void setDataFinalizacao(Date dataFinalizacao) {
		
	}

	@Override
	public List<ITarefaAgendada<?>> getTarefasPai() {
		return null;
	}

	@Override
	public String getParametro(String key) {
		return null;
	}

	@Override
	public void setParametros(String parametros) {
		
	}

	@Override
	public void atualizarTarefaNaBaseLocal() {
		
	}

	@Override
	public List<E> getResult() {
		return null;
	}

	@Override
	public void setResult(List<E> result) {
		
	}

	@Override
	public void setTarefaDAO(ITarefaDAO baseLocal) {
		
	}

	@Override
	public boolean possuiControleDeAlteracaoNaBaseRemota() {
		return false;
	}

	@Override
	public SyncResult<E> getEntidadesBaseLocal() {
		return null;
	}

	@Override
	public SyncResult<E> getEntidadesBaseRemota(Date dataAtualizacao) {
		return null;
	}

	@Override
	public SyncResult<E> atualizarBaseLocalComChaveSecundaria(List<E> entidades) {
		return null;
	}

	@Override
	public SyncResult<E> salvarBaseLocal(List<E> entidades) {
		return null;
	}

	@Override
	public SyncResult<E> salvarOuatualizarBaseLocal(E entidadee) {
		return null;
	}

	@Override
	public SyncResult<E> salvarBaseLocal(E entidade) {
		return null;
	}

	@Override
	public String getParametros() {
		return null;
	}

	@Override
	public String getIdentificador() {
		return identificador;
	}

}
