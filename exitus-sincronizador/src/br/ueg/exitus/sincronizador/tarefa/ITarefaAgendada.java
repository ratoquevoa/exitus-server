package br.ueg.exitus.sincronizador.tarefa;

import java.util.Date;
import java.util.List;

import br.ueg.exitus.sincronizador.utils.SyncResult;

public interface ITarefaAgendada<E extends Sincronizavel> {
	
	void init(String params);

	void iniciarExecucao();
	
	void executar();

	boolean podeIniciar();

	boolean ehDependente();
	
	Integer getId();

	String getStatus();

	void setStatus(String status);
	
	String getTabela();

	Date getDataFinalizacao();

	void setDataFinalizacao(Date dataFinalizacao);

	List<ITarefaAgendada<?>> getTarefasPai();

	String getParametro(String key);

	void setParametros(String parametros);

	void atualizarTarefaNaBaseLocal();

	List<E> getResult();
	
	void setResult(List<E> result);

	void setTarefaDAO(ITarefaDAO baseLocal);

	boolean possuiControleDeAlteracaoNaBaseRemota();

	SyncResult<E> getEntidadesBaseLocal();

	SyncResult<E> getEntidadesBaseRemota(Date dataAtualizacao);

	SyncResult<E> atualizarBaseLocalComChaveSecundaria(List<E> entidades);

	SyncResult<E> salvarBaseLocal(List<E> entidades);

	SyncResult<E> salvarOuatualizarBaseLocal(E entidadee);

	SyncResult<E> salvarBaseLocal(E entidade);

	String getParametros();
	
	String getIdentificador();
}
