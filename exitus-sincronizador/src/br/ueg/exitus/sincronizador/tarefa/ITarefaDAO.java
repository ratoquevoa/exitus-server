package br.ueg.exitus.sincronizador.tarefa;

import br.ueg.exitus.sincronizador.utils.SyncResult;

public interface ITarefaDAO<T extends ITarefaAgendada<?>> {
	
	SyncResult<T> getTarefasPendentes();
	
	SyncResult<T> persistirTarefaAgendada(T tarefa);

	SyncResult<T> atualizarTarefaAgendada(T tarefa);

	
//	Boolean atualizarStatusTarefa()
}
