package br.ueg.exitus.sincronizador.tarefa;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import br.ueg.exitus.sincronizador.utils.ChaveSecundaria;
import br.ueg.exitus.sincronizador.utils.DateUtils;
import br.ueg.exitus.sincronizador.utils.ReflectionUtils;
import br.ueg.exitus.sincronizador.utils.SyncResult;

public abstract class TarefaAbstract<E extends Sincronizavel> implements ITarefaAgendada<E> {

	public TarefaAbstract(String params) {
		init(params);
	}

	@Override
	public void init(String params){
		setStatus(Status.PENDENTE);
		setParametros(params);
	}

	@Override
	public void executar() {
		if(!podeIniciar()){
			setStatus(Status.PENDENTE);
			return;
		}
		Date dataAtualizacao;
		try {
			dataAtualizacao = (new SimpleDateFormat("yyyy-MM-dd")
					.parse((String) getParametro("data_atualizacao")));
		} catch (ParseException e) {
			e.printStackTrace();
			return;
		}
		SyncResult<E> retRemoto = (SyncResult<E>) getEntidadesBaseRemota(dataAtualizacao);
		System.out.println("Buscando lista na base remota da entidade: " + getClass().getSimpleName());
		if(retRemoto.isOk()){
			if(retRemoto.getLista().isEmpty()){
				System.out.println("Sem dados remotos para a tarefa " + getClass().getSimpleName());
				setStatus(Status.FINALIZADA);
				return;
			}
			setResult(retRemoto.getLista());
			SyncResult<E> retLocal = (SyncResult<E>) getEntidadesBaseLocal();
			System.out.println("Buscando lista na base local da entidade: " + getClass().getSimpleName());
			List<E> result = getResult();
			if(retLocal.getLista() == null || retLocal.getLista().isEmpty()){
				System.out.println("Base local vazia. Gravando todas as entidades...");
				salvarBaseLocal(result);
				setStatus(Status.FINALIZADA);
				return;
			}else if(possuiControleDeAlteracaoNaBaseRemota()){
				System.out.println("Base local possui entidades: " + getClass().getSimpleName());
				System.out.println("Possui controle de altera��o. Atualiza��o dos dados com base na chave prim�ria");
				for(E entidade : result){
					try {
						if(DateUtils.comparar(entidade.getDataAtualizacao(), new SimpleDateFormat("yyyy-MM-dd")
								.parse(getParametro("data_atualizacao"))) >= 0){
							entidade.setDataAtualizacao(new Date());
							salvarOuatualizarBaseLocal(entidade);
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				setStatus(Status.FINALIZADA);
			}else{
				System.out.println("Base local possui entidades: " + getClass().getSimpleName());
				System.out.println("N�o possui controle de altera��o. Verificando se houve altera��o por meio da chave secund�ria");
				if(ReflectionUtils.getInstancia().identificavel(retLocal.getLista().get(0).getClass())){
					for(E entidade : result){
						entidade.setDataAtualizacao(new Date());
						salvarOuatualizarBaseLocal(entidade);
					}
				}else{
					for(E entidadeRemota : result){
						Object valueSecundariaRemota = ReflectionUtils.getInstancia().getFieldValueWithAnnotation(entidadeRemota, ChaveSecundaria.class);
						boolean encontrada = false;
						for(E entidadeLocal : retLocal.getLista()){
//							Object valueSecundariaLocal = ReflectionUtils.getInstancia().getFieldValueWithAnnotation(entidadeLocal, ChaveSecundaria.class);
							if(ReflectionUtils.getInstancia().compararEntidades(entidadeLocal, entidadeRemota)){
								ReflectionUtils.getInstancia().copiar(entidadeRemota, entidadeLocal);
								System.out.println("Entidade local " + this.getTabela() + " encontrada com chave secund�ria " 
										+ ". Atualizando entidade com dados remotos");
								entidadeLocal.setDataAtualizacao(new Date());
								salvarOuatualizarBaseLocal(entidadeLocal);
								encontrada = true;
								break;
							}
							setResult(retLocal.getLista());
						}
						if(!encontrada){
							System.out.println("N�o foi encontrada entidade local com chave secund�ria " + 
									valueSecundariaRemota + ". Inserindo novo registro");
							salvarBaseLocal(entidadeRemota);
						}
					}
				}
				setStatus(Status.FINALIZADA);
			}
		}else{
			setStatus(Status.PENDENTE);
		}
	}

}
