package br.ueg.exitus.sincronizador.tarefa;

import java.util.Date;

public interface Sincronizavel {
	
	Date getDataAtualizacao();
	
	void setDataAtualizacao(Date date);
	
	Object getChaveSecundaria();
	
}
