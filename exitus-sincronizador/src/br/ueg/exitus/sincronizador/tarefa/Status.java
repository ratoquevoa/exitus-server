package br.ueg.exitus.sincronizador.tarefa;

public class Status {
	public static final String FINALIZADA = "F";
	public static final String EXECUTANDO = "E";
	public static final String PENDENTE = "P";
	public static final String CONFLITO = "C";
	public static final String SINCRONIZADA = "S";
}
