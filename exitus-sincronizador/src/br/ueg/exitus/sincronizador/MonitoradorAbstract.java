package br.ueg.exitus.sincronizador;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.ueg.exitus.sincronizador.tarefa.ITarefaAgendada;
import br.ueg.exitus.sincronizador.tarefa.ITarefaDAO;
import br.ueg.exitus.sincronizador.tarefa.Status;

public abstract class MonitoradorAbstract {
	private GestorDeTarefas gestor;

	public abstract void iniciarMonitoramento(List<ITarefaAgendada<?>> listaExecucao);


	public void executar(List<ITarefaAgendada<?>> listaExecucao){
		//		while(true){
		if(listaExecucao.size() == 0){
			System.out.println("Sem mais tarefas a serem executadas. Parando monitoramento");
			esperar();
		}
		ArrayList<ITarefaAgendada<?>> copia = new ArrayList<>(listaExecucao);
		for(ITarefaAgendada<?> tarefa : copia){
			switch (tarefa.getStatus()) {
			case Status.EXECUTANDO:
				continue;

			case Status.FINALIZADA:
				tarefa.setDataFinalizacao(new Date());
				tarefa.atualizarTarefaNaBaseLocal();
				listaExecucao.remove(tarefa);
				break;

			default:
				if(tarefa.ehDependente()){
					boolean tarefasProntas = true;
					for(ITarefaAgendada<?> tarefaPai : tarefa.getTarefasPai()){
						tarefasProntas = tarefasProntas && (tarefaPai.getStatus().equals(Status.FINALIZADA));
					}
					if(tarefasProntas){
						gestor.iniciarTarefa(tarefa);
					}
					continue;
				}else{
					gestor.iniciarTarefa(tarefa);
				}
				break;
			}
		}
		//		}
	}

	protected boolean verificarSeFinalizada(ITarefaAgendada<?> tarefa){
		if(!tarefa.ehDependente()){
			return tarefa.getStatus().equals(Status.FINALIZADA);
		}
		return tarefa.getStatus().equals(Status.FINALIZADA) && verificarSeFinalizada(tarefa);
	}

	protected void finalizarTarefa(ITarefaAgendada<?> tarefa, List<ITarefaAgendada<?>> listaExecucao){
		tarefa.setDataFinalizacao(new Date());
		tarefa.atualizarTarefaNaBaseLocal();
		listaExecucao.remove(tarefa);
		System.out.println("Removendo a lista de execu��o a tarefa " + tarefa);
	}
	
	public void iniciarTarefa(ITarefaAgendada<?> tarefa, ITarefaDAO<ITarefaAgendada<?>> tarefaDAO){
		System.out.println("Colocando em execucao: " + tarefa);
		tarefa.setStatus(Status.EXECUTANDO);
		tarefa.setTarefaDAO(tarefaDAO);
		tarefa.iniciarExecucao();
	}

	protected void setGestorDeTarefas(GestorDeTarefas gestor){
		this.gestor = gestor;
	}

	protected abstract void esperar();

	public abstract void notificar();

	public abstract boolean isWaiting();

	public abstract boolean isAlive();

}
