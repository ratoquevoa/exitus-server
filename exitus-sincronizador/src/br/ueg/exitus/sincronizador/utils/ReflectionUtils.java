package br.ueg.exitus.sincronizador.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ReflectionUtils {

	private static ReflectionUtils instancia;

	static Set<Class<?>> primitiveTypes = new HashSet<Class<?>>();
	static{
		primitiveTypes.add(String.class);
		primitiveTypes.add(Boolean.class);
		primitiveTypes.add(Character.class);
		primitiveTypes.add(Byte.class);
		primitiveTypes.add(Short.class);
		primitiveTypes.add(Integer.class);
		primitiveTypes.add(Long.class);
		primitiveTypes.add(Float.class);
		primitiveTypes.add(Double.class);
		primitiveTypes.add(Void.class);
	}

	private ReflectionUtils(){}

	public static ReflectionUtils getInstancia(){
		if(instancia == null){
			instancia = new ReflectionUtils();
		}
		return instancia;
	}

	public boolean identificavel(Class<?> classe){
		return classe.isAnnotationPresent(HasID.class);
	}

	@SuppressWarnings("unchecked")
	public <T> T getFieldValueWithAnnotation(Object object, Class<? extends Annotation> annotation){
		try{
			Class<?> superClasse = object.getClass();
			while(superClasse != null && !superClasse.getSimpleName().equals("Object")){
				for(Field f : superClasse.getDeclaredFields()){
					if(f.isAnnotationPresent(annotation)){
						String nomeMetodo = "get" + firstLetterUppercase(f.getName());
						Method getAtributo = superClasse.getDeclaredMethod(nomeMetodo);
						return (T) getAtributo.invoke(object);
					}
				}
				superClasse = superClasse.getSuperclass();
			}
		}catch(NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e){
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public String getFieldNameWithAnnotation(Object object, Class<? extends Annotation> annotation){
		try{
			Class<?> superClasse = object.getClass();
			while(superClasse != null && !superClasse.getSimpleName().equals("Object")){
				for(Field f : superClasse.getDeclaredFields()){
					if(f.isAnnotationPresent(annotation)){
						return f.getName();
					}
				}
				superClasse = superClasse.getSuperclass();
			}
		}catch(IllegalArgumentException e){
		}
		return null;
	}

	public boolean compararEntidades(Object entidade1, Object entidade2){
		boolean iguais = true;
		try{
			Class<?> classe = entidade1.getClass();
			for(Field f : classe.getDeclaredFields()){
				if(f.isAnnotationPresent(Comparable.class)){
					String nomeMetodo = "get" + firstLetterUppercase(f.getName());
					Method getAtributo = classe.getDeclaredMethod(nomeMetodo);
					Object valueEntidade1 =  getAtributo.invoke(entidade1);
					Object valueEntidade2 =  getAtributo.invoke(entidade2);
					if(notPrimitiveType(valueEntidade1)){
						if(getFieldValueWithAnnotation(valueEntidade1, ChaveSecundaria.class) != null){
							valueEntidade1 = getFieldValueWithAnnotation(valueEntidade1, ChaveSecundaria.class);
							valueEntidade2 = getFieldValueWithAnnotation(valueEntidade2, ChaveSecundaria.class);
						}else{
							nomeMetodo = "getId";
							getAtributo = valueEntidade1.getClass().getDeclaredMethod(nomeMetodo);
							valueEntidade1 = getAtributo.invoke(valueEntidade1);
							valueEntidade2 = getAtributo.invoke(valueEntidade2);
						}
					}
					if(valueEntidade1 == null){
						iguais = iguais && valueEntidade2 == null;
					}else{
						iguais = iguais && (valueEntidade1.equals(valueEntidade2));
					}
				}
			}
		}catch(NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e){
			e.printStackTrace();
			iguais = false;
		}
		return iguais;
	}

	public Map<String, Object> getFieldsWithValueWithAnnotation(Object object, Class<? extends Annotation> annotation){
		Map<String, Object> map = new HashMap<>();
		try{
			Class<?> superClasse = object.getClass();
			while(superClasse != null && !superClasse.getSimpleName().equals("Object")){
				Field[] fields = superClasse.getDeclaredFields();
				for(Field f : fields){
					if(f.isAnnotationPresent(annotation)){
						String nomeMetodo = "get" + firstLetterUppercase(f.getName());
						Method getAtributo = superClasse.getDeclaredMethod(nomeMetodo);
						Object value =  getAtributo.invoke(object);
						map.put(f.getName(), value);
					}
				}
				superClasse = superClasse.getSuperclass();
			}
		}catch(IllegalArgumentException | NoSuchMethodException | SecurityException | IllegalAccessException | InvocationTargetException e){
			e.printStackTrace();
		}
		return map;
	}

	public boolean copiar(Object entidade1, Object entidade2){
		boolean iguais = true;
		try{
			Class<?> classe = entidade1.getClass();
			for(Field f : classe.getDeclaredFields()){
				if(f.isAnnotationPresent(Comparable.class)){
					String nomeGetMethod = "get" + firstLetterUppercase(f.getName());
					Method getAtributo = classe.getDeclaredMethod(nomeGetMethod);
					Object valueEntidade1 =  getAtributo.invoke(entidade1);

					String nomeSetMethod = "set" + firstLetterUppercase(f.getName());
					Method setAtributo = classe.getDeclaredMethod(nomeSetMethod, f.getType());
					setAtributo.invoke(entidade2, valueEntidade1);
				}
			}
		}catch(NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e){
			e.printStackTrace();
			iguais = false;
		}
		return iguais;
	}

	public String firstLetterUppercase(String string){
		return string.substring(0,1).toUpperCase() + string.substring(1);
	}

	public boolean notPrimitiveType(Object object){
		if(object == null) return false;
		return !primitiveTypes.contains(object.getClass());
	}

}
