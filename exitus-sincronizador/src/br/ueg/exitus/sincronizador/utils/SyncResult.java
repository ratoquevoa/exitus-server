package br.ueg.exitus.sincronizador.utils;

import java.util.List;

public class SyncResult<T> {
	private List<T> lista;
	private boolean ok;
	
	public SyncResult(){}
	
	public SyncResult(boolean ok) {
		super();
		this.ok = ok;
	}
	
	public SyncResult(List<T> lista, boolean ok) {
		super();
		this.lista = lista;
		this.ok = ok;
	}

	public List<T> getLista() {
		return lista;
	}

	public void setLista(List<T> lista) {
		this.lista = lista;
	}

	public boolean isOk() {
		return ok;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}
}
